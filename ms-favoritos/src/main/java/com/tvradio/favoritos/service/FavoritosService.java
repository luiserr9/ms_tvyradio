package com.tvradio.favoritos.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale.Category;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvradio.favoritos.repository.FavoritosRepository;
import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Favoritos;
import com.tvradio.general.entities.Programa;
import com.tvradio.general.entities.UsuarioOnDemand;
import com.tvradio.general.utilities.Response;

@Service
public class FavoritosService {
	@Autowired
	private FavoritosRepository favoritos;

	public Response getAllFavoritesByIdUsuario(UsuarioOnDemand user) {
		try {
			return Response.ok(this.favoritos.findByUsuario(user));
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response deleteFavoriteById(Long id) {
		try {
			this.favoritos.deleteById(id);
			return Response.success(true);
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response insertFavorite(Favoritos favo) {
		try {
			if (this.favoritos.findByProgramaAndUsuario(favo.getPrograma(), favo.getUsuario()) == null) {
				return Response.created(this.favoritos.save(favo));
			} else {
				return Response.badRequest("Favorito ya agregado");
			}

		} catch (Exception e) {
			return Response.error("Error inespeerado:" + e.getMessage());
		}
	}

}