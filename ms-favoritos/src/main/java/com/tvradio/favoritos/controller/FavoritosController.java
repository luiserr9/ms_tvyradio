package com.tvradio.favoritos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tvradio.favoritos.service.FavoritosService;
import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Favoritos;
import com.tvradio.general.entities.UsuarioOnDemand;
import com.tvradio.general.utilities.Response;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/favorites")
public class FavoritosController {

	@Autowired
	private FavoritosService service;

	@PostMapping("/getAllFavoritesByIdUsuario")
	public Response getAllFavoritesByIdUsuario(@RequestBody UsuarioOnDemand user) {
		return this.service.getAllFavoritesByIdUsuario(user);
	}
	
	@PostMapping("/deleteFavoriteById")
	public Response deleteFavoriteById(@RequestBody Favoritos favo) {
		return this.service.deleteFavoriteById(favo.getId());
	}
	
	@PostMapping("/insertFavorite")
	public Response insertFavorite(@RequestBody Favoritos favo ) {
		return this.service.insertFavorite(favo);
	}

}