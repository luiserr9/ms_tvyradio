package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.MetaValue;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;

@Entity(name = "notificaiones_push")
public class Notificaciones {
	
	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="notificacion_id")
	private Long notificacionid;
	
	
	@Column(name = "title")
	private String titulo;
	
	@Column(name = "message")
	private String mensaje;
	
	@Column(name = "url")
	private String url;
	
	@Column(name = "segmentation")
	private String segemnto;
	
	@JsonFormat(timezone = "UTC" ,pattern="yyyy-MM-dd")
	@Column(name="date_send")
	private Date  fech_envio;
	
	@ManyToOne
	@JoinColumn(name = "id_uaurio")
	private UsuarioPanel usuario;
	
	@Column(name = "activo")
	private Boolean activo;

	public Notificaciones() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public UsuarioPanel getUsuario() {
		return usuario;
	}



	public void setUsuario(UsuarioPanel usuario) {
		this.usuario = usuario;
	}



	public Long getNotificacionid() {
		return notificacionid;
	}

	public void setNotificacionid(Long notificacionid) {
		this.notificacionid = notificacionid;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSegemnto() {
		return segemnto;
	}

	public void setSegemnto(String segemnto) {
		this.segemnto = segemnto;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	
	

}
