package com.tvradio.general.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "home_app")
public class HomeApp {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_home_app", nullable = false, unique = true)
	private Long id_home;
	
	@Column(name = "img_logo")
	private String imglogo;
	
	@Column(name = "img_banner1")
	private String imgbanner1;
	
	@Column(name = "img_banner2")
	private String imgbanner2;
	
	@Column(name = "img_banner3")
	private String imgbanner3;

	public HomeApp() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId_home() {
		return id_home;
	}

	public void setId_home(Long id_home) {
		this.id_home = id_home;
	}

	public String getImglogo() {
		return imglogo;
	}

	public void setImglogo(String imglogo) {
		this.imglogo = imglogo;
	}

	public String getImgbanner1() {
		return imgbanner1;
	}

	public void setImgbanner1(String imgbanner1) {
		this.imgbanner1 = imgbanner1;
	}

	public String getImgbanner2() {
		return imgbanner2;
	}

	public void setImgbanner2(String imgbanner2) {
		this.imgbanner2 = imgbanner2;
	}

	public String getImgbanner3() {
		return imgbanner3;
	}

	public void setImgbanner3(String imgbanner3) {
		this.imgbanner3 = imgbanner3;
	}
	
	
	
	
}
