package com.tvradio.general.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "img_btnondemand")
public class Imgradioondemand {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_img_ondemand",unique = true, nullable = false)
	private Long idimgondemand;
	
	@Column(name = "title")
	private String titulo;
	
	@Column(name = "img_logo_btn")
	private String img;
	
	

	public Imgradioondemand() {
		super();
		
	}

	public Long getIdimgondemand() {
		return idimgondemand;
	}

	public void setIdimgondemand(Long idimgondemand) {
		this.idimgondemand = idimgondemand;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	
	
	
	

}
