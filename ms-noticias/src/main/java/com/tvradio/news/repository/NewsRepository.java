package com.tvradio.news.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tvradio.general.entities.Noticias;



public interface NewsRepository extends CrudRepository<Noticias, Long> {

	public List<Noticias> findAllByStatus(Boolean status);

	public Noticias findByIdAndStatus(Long id, Boolean status);
	
	public Noticias  findByName(String name);
	
	public Noticias  findByNameAndStatus(String name , Boolean status);
	
	
	
	

}
