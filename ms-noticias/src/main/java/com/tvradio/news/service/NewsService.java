package com.tvradio.news.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale.Category;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvradio.general.entities.Noticias;
import com.tvradio.general.utilities.Response;
import com.tvradio.news.repository.NewsRepository;

@Service
public class NewsService {
	@Autowired
	private NewsRepository repository;

	public Response getAllNews() {
		try {
			return Response.ok(this.repository.findAllByStatus(true));
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getNewsById(Long id) {
		try {
			return Response.ok(this.repository.findByIdAndStatus(id, true));
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response insertNews(Noticias news) {
		try {
			if (this.repository.findByNameAndStatus(news.getName(), true) == null) {
				if (this.repository.findByName(news.getName()) == null) {
					news.setStatus(true);
					news.setDateCreation(new Date());
					this.repository.save(news);
					return Response.created("Noticia Creada");
				} else {
					news.setId(this.repository.findByName(news.getName()).getId());
					news.setStatus(true);
					this.repository.save(news);
					return Response.accepted("Noticia Habilitada");
				}
			} else {
				return Response.badRequest("Noticia ya existente");
			}
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response updateNews(Noticias news) {
		try {
			
			
			if (this.repository.findByIdAndStatus(news.getId(), true) != null) {
				Noticias ss = this.repository.findById(news.getId()).get();
				if (news.getUrl() == null) {
					news.setUrl(ss.getUrl());
				}
				if (news.getPicture() == null) {
					news.setPicture(ss.getPicture());
				}
				if (news.getName() == null) {
					news.setName(ss.getName());
				}
				if (news.getDateCreation() == null) {
					news.setDateCreation(ss.getDateCreation());
				}
				if (news.getStatus() == null) {
					news.setStatus(ss.getStatus());
				}
				this.repository.save(news);
				return Response.accepted("OK");
			}else {
				return Response.notFound("No se encuentra");
			}
			
		} catch (Exception e) {
			return Response.error("Error insesperado: " + e.getMessage());
		}
	}

	
	public Response deleteNews(Long id) {
		try {
			if (this.repository.findByIdAndStatus(id, true) != null) {
				Noticias ss=this.repository.findByIdAndStatus(id, true);
				ss.setStatus(false);
				this.repository.save(ss);
				return Response.accepted("OK");
			}else {
				return Response.badRequest("La noticia no existe");
			}
		}catch(Exception e) {
			return Response.error("Error inesperado: "+e.getMessage());
		}
	}
}