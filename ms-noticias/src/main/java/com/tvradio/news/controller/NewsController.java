package com.tvradio.news.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tvradio.general.entities.Noticias;
import com.tvradio.general.utilities.Response;
import com.tvradio.news.service.NewsService;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("/news")
public class NewsController {
	@Autowired
	private NewsService service;

	@GetMapping("/getAllNews")
	public Response getAllNews() {
		return this.service.getAllNews();
	}

	@PostMapping("/getNewsById")
	public Response getNewsById(@RequestBody Noticias news) {
		return this.service.getNewsById(news.getId());
	}

	@PostMapping("/insertNews")
	public Response insertNews(@RequestBody Noticias news) {
		return this.service.insertNews(news);
	}
	
	
	@PostMapping("/updateNews")
	public Response updateNews(@RequestBody Noticias news) {
		return this.service.updateNews(news);
	}
	@PostMapping("/deleteNews")
	public Response deleteNews(@RequestBody Noticias news) {
		return this.service.deleteNews(news.getId());
	}
}