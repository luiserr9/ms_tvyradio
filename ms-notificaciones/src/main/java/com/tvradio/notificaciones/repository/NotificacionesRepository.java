package com.tvradio.notificaciones.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.Notificaciones;

public interface NotificacionesRepository extends CrudRepository<Notificaciones, Long> {
	
	public Notificaciones findByActivo(Boolean activo);
	
	@Query(value="SELECT n.* FROM notificaiones_push n ORDER BY n.notificacion_id DESC LIMIT 1 ", nativeQuery = true)
	public Notificaciones getfinNoti();
	
	public Notificaciones findByNotificacionid(Long notificacionid);
}
