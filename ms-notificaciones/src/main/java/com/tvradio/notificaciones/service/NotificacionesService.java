package com.tvradio.notificaciones.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvradio.general.entities.Notificaciones;
import com.tvradio.general.utilities.Response;
import com.tvradio.notificaciones.repository.NotificacionesRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


@Service
public class NotificacionesService {

	@Autowired
	private NotificacionesRepository notificacionesRepository;
	
	public Response getNotificApp() {
		
		return Response.success(notificacionesRepository.findByActivo(true)) ;
	}
	
	public Response getNotificDash() {
		return Response.success(notificacionesRepository.findAll());
	}
	
	
	
	public Response getNotificationById(Long id) {
		
		return Response.success(notificacionesRepository.findByNotificacionid(id));
	}
	
	public Response insertNotification( Notificaciones notificaciones) {
		try {
			ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();
			final ScheduledFuture<?> timerHandle = timer.scheduleAtFixedRate(thread, 10, 10, TimeUnit.HOURS);
			timer.schedule(new Runnable () {
				public void run() {
					timerHandle.cancel(true);
				}
			},1*15, TimeUnit.HOURS);
			
			
			notificacionesRepository.save(notificaciones);
			return Response.success("notificacion registrada");
		
			
		} catch (Exception e) {
			return Response.error("Error al insertar: "+e.getMessage());
		}
		
	}
	
	final Runnable  thread = new Runnable ()  {
		@Override
		public void run () {
			notifcationiDisabled();			
		}
	};
	
	
	public void notifcationiDisabled() {
		
		Notificaciones notificaciones = notificacionesRepository.getfinNoti();
		notificaciones.setActivo(false);
		notificacionesRepository.save(notificaciones); 
		System.out.println("noti push desabilitad");
	}
}
