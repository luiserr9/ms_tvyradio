package com.tvradio.notificaciones.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tvradio.general.entities.Notificaciones;
import com.tvradio.general.utilities.Response;
import com.tvradio.notificaciones.service.NotificacionesService;





@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/notification")
public class NotificacionesController {
	
	@Autowired
	private NotificacionesService notificacionesService;
	
	
	@GetMapping("/getNotificationApp")
	private Response getNotificationApp () {
		return notificacionesService.getNotificApp();
	}
	
	@GetMapping("/getNotificationDash")
	private Response getNotificationDash () {
		return notificacionesService.getNotificDash();
	}
	
	@GetMapping("/getNotificationById/{id}")
	private Response getNotificationById (@PathVariable("id") Long id) {
		return notificacionesService.getNotificationById(id);
	}
	
	
	
	@PostMapping("/insertNotification")
	private Response insertNotification (@RequestBody Notificaciones notificaciones) {
		return notificacionesService.insertNotification(notificaciones);
	}
	
	
	
	
		
}
