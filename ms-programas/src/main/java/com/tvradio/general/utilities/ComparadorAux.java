package com.tvradio.general.utilities;

import java.util.Comparator;

import com.tvradio.general.dto.LogViewsProgramAux;


public class ComparadorAux implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		LogViewsProgramAux log1 = (LogViewsProgramAux) o2;
		LogViewsProgramAux log2 = (LogViewsProgramAux) o1;
		return log1.getViews().compareTo(log2.getViews());

	}
}
