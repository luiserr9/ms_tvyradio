package com.tvradio.general.utilities;

import java.util.Comparator;

import com.tvradio.general.dto.LogViewsProgramResp;

public class Comparador implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		LogViewsProgramResp log1 = (LogViewsProgramResp) o2;
		LogViewsProgramResp log2 = (LogViewsProgramResp) o1;
		return log1.getViews().compareTo(log2.getViews());

	}
}
