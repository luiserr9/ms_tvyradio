package com.tvradio.general.dto;

public class LikeProgramResp {
	private Long programId;
	private String name;
	private String pictureMiniature;

	public LikeProgramResp() {
		super();
	}

	public LikeProgramResp(Long programId, String name, String pictureMiniature) {
		super();
		this.programId = programId;
		this.name = name;
		this.pictureMiniature = pictureMiniature;
	}

	public Long getProgramId() {
		return programId;
	}

	public void setProgramId(Long programId) {
		this.programId = programId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPictureMiniature() {
		return pictureMiniature;
	}

	public void setPictureMiniature(String pictureMiniature) {
		this.pictureMiniature = pictureMiniature;
	}

}
