package com.tvradio.general.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.tvradio.general.entities.Categoria;

public class ProgramasRecomendacion {

	private Long programId;

	private String name;

	private String description;

	private Integer numChapters;

	private Boolean isPrincipaldOfCategory;

	private String pictureMiniature;

	private String pictureCover;

	private String pictureLogo;

	private Boolean status;

	private Date dateCreation;

	private Integer seasons;
	
	private Boolean isFvoirite;
	
	
	

	public ProgramasRecomendacion(Long programId, String name, String description, Integer numChapters,
			Boolean isPrincipaldOfCategory, String pictureMiniature, String pictureCover, String pictureLogo,
			Boolean status, Date dateCreation, Integer seasons, Boolean isFvoirite) {
		super();
		this.programId = programId;
		this.name = name;
		this.description = description;
		this.numChapters = numChapters;
		this.isPrincipaldOfCategory = isPrincipaldOfCategory;
		this.pictureMiniature = pictureMiniature;
		this.pictureCover = pictureCover;
		this.pictureLogo = pictureLogo;
		this.status = status;
		this.dateCreation = dateCreation;
		this.seasons = seasons;
		this.isFvoirite = isFvoirite;
	}



	public ProgramasRecomendacion() {
		super();
	}

	
	
	public Boolean getIsFvoirite() {
		return isFvoirite;
	}



	public void setIsFvoirite(Boolean isFvoirite) {
		this.isFvoirite = isFvoirite;
	}



	public Long getProgramId() {
		return programId;
	}

	public void setProgramId(Long programId) {
		this.programId = programId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	public Integer getNumChapters() {
		return numChapters;
	}

	public void setNumChapters(Integer numChapters) {
		this.numChapters = numChapters;
	}

	public Boolean getIsPrincipaldOfCategory() {
		return isPrincipaldOfCategory;
	}

	public void setIsPrincipaldOfCategory(Boolean isPrincipaldOfCategory) {
		this.isPrincipaldOfCategory = isPrincipaldOfCategory;
	}

	public String getPictureMiniature() {
		return pictureMiniature;
	}

	public void setPictureMiniature(String pictureMiniature) {
		this.pictureMiniature = pictureMiniature;
	}

	public String getPictureCover() {
		return pictureCover;
	}

	public void setPictureCover(String pictureCover) {
		this.pictureCover = pictureCover;
	}

	public String getPictureLogo() {
		return pictureLogo;
	}

	public void setPictureLogo(String pictureLogo) {
		this.pictureLogo = pictureLogo;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Integer getSeasons() {
		return seasons;
	}

	public void setSeasons(Integer seasons) {
		this.seasons = seasons;
	}
	
	

}
