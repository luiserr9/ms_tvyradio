package com.tvradio.general.dto;

import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Programa;
import com.tvradio.general.entities.UsuarioOnDemand;

public class LogViewDto {

	private UsuarioOnDemand usuario;
	private Programa programa;
	
	public LogViewDto() {
		super();
	}
	public LogViewDto(UsuarioOnDemand usuario, Programa programa) {
		super();
		this.usuario = usuario;
		this.programa = programa;
	}
	public UsuarioOnDemand getUsuario() {
		return usuario;
	}
	public void setUsuario(UsuarioOnDemand usuario) {
		this.usuario = usuario;
	}
	public Programa getPrograma() {
		return programa;
	}
	public void setPrograma(Programa programa) {
		this.programa = programa;
	}
	
	

}
