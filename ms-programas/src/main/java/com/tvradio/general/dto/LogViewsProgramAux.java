package com.tvradio.general.dto;

public class LogViewsProgramAux {

	private String nombre;

	private Integer views;

	private Long id;

	public LogViewsProgramAux() {
		super();
	}

	public LogViewsProgramAux(String nombre, Integer views, Long id) {
		super();
		this.nombre = nombre;
		this.views = views;
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getViews() {
		return views;
	}

	public void setViews(Integer views) {
		this.views = views;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
