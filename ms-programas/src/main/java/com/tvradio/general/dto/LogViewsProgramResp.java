package com.tvradio.general.dto;

public class LogViewsProgramResp {

	private Long programId;

	private String nombre;

	private Integer views;

	private String pictureMiniature;
	
	
	public LogViewsProgramResp() {
		super();
	}

	public LogViewsProgramResp(Long programId, String nombre, Integer views, String pictureMiniature) {
		super();
		this.programId = programId;
		this.nombre = nombre;
		this.views = views;
		this.pictureMiniature = pictureMiniature;

		}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getViews() {
		return views;
	}

	public void setViews(Integer views) {
		this.views = views;
	}

	public Long getProgramId() {
		return programId;
	}

	public void setProgramId(Long programId) {
		this.programId = programId;
	}

	public String getPictureMiniature() {
		return pictureMiniature;
	}

	public void setPictureMiniature(String pictureMiniature) {
		this.pictureMiniature = pictureMiniature;
	}
	 
	
	
	
}
