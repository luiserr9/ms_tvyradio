package com.tvradio.general.dto;


import java.math.BigInteger;

public class ProgramaResp {

	private long programId=0; 	
	private String name;
    private BigInteger categoryId; 
    private String nameCategory; 
    private Integer numChapters; 
    private String fechCreation; 
    //private Date fechEnd;
    private String picturePerf; 
    private String picturePort;
    private Long idFavorito;
    
	public ProgramaResp() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProgramaResp(long programId, String name, BigInteger categoryId, String nameCategory, Integer numChapters,
			String fechCreation, String picturePerf, String picturePort, Long idFavorito) {
		super();
		this.programId = programId;
		this.name = name;
		this.categoryId = categoryId;
		this.nameCategory = nameCategory;
		this.numChapters = numChapters;
		this.fechCreation = fechCreation;
		this.picturePerf = picturePerf;
		this.picturePort = picturePort;
		this.idFavorito = idFavorito;
	}

	public long getProgramId() {
		return programId;
	}

	public void setProgramId(long programId) {
		this.programId = programId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigInteger getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(BigInteger categoryId) {
		this.categoryId = categoryId;
	}

	public String getNameCategory() {
		return nameCategory;
	}

	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}

	public Integer getNumChapters() {
		return numChapters;
	}

	public void setNumChapters(Integer numChapters) {
		this.numChapters = numChapters;
	}

	public String getFechCreation() {
		return fechCreation;
	}

	public void setFechCreation(String fechCreation) {
		this.fechCreation = fechCreation;
	}

	public String getPicturePerf() {
		return picturePerf;
	}

	public void setPicturePerf(String picturePerf) {
		this.picturePerf = picturePerf;
	}

	public String getPicturePort() {
		return picturePort;
	}

	public void setPicturePort(String picturePort) {
		this.picturePort = picturePort;
	}
	
          
}
