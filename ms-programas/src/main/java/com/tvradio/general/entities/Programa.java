package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.GenerationType;

@Entity(name="programa")

public class Programa {
	
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	@Column(name = "program_id",unique = true, nullable = false)
	private Long programId; 
	
	
	@Column(name = "name",unique = true,nullable = false)
    private String name;
	
	@Column(name="description",nullable = true)
	private String description;
	
	
	@ManyToOne
	//@JsonIgnore
	@JoinColumn(name="categoria")
	private Categoria category;
		
	@Column(name = "num_chapters" ,nullable = true)
    private Integer numChapters; 
	
	@Column(name="is_principal_of_category",nullable = false)
	private Boolean isPrincipaldOfCategory;
	
	
	@Column(name = "picture_miniature")
    private String pictureMiniature; 
	
	@Column(name = "picture_cover")
    private String pictureCover;
	
	@Column(name="picture_logo")
	private String pictureLogo;

	
	@Column(name = "status")
    private Boolean status;
	 
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_creation", nullable = false,updatable = false)
    private Date dateCreation;
	
	@Column(name = "seasons", nullable = false)
	public Integer seasons;
	
	
	public Integer getSeasons() {
		return seasons;
	}

	public void setSeasons(Integer seasons) {
		this.seasons = seasons;
	}

	public Long getProgramId() {
		return programId;
	}

	public void setProgramId(Long programId) {
		this.programId = programId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Categoria getCategory() {
		return category;
	}

	public void setCategory(Categoria category) {
		this.category = category;
	}

	public Integer getNumChapters() {
		return numChapters;
	}

	public void setNumChapters(Integer numChapters) {
		this.numChapters = numChapters;
	}

	public Boolean getIsPrincipaldOfCategory() {
		return isPrincipaldOfCategory;
	}

	public void setIsPrincipaldOfCategory(Boolean isPrincipaldOfCategory) {
		this.isPrincipaldOfCategory = isPrincipaldOfCategory;
	}

	public String getPictureMiniature() {
		return pictureMiniature;
	}

	public void setPictureMiniature(String pictureMiniature) {
		this.pictureMiniature = pictureMiniature;
	}

	public String getPictureCover() {
		return pictureCover;
	}

	public void setPictureCover(String pictureCover) {
		this.pictureCover = pictureCover;
	}

	public String getPictureLogo() {
		return pictureLogo;
	}

	public void setPictureLogo(String pictureLogo) {
		this.pictureLogo = pictureLogo;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	} 
	
	

	
	
		
}