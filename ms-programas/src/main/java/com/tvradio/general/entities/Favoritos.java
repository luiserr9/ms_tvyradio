package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity(name = "favoritos")
public class Favoritos {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id" ,nullable = false,unique = true)
	private Long id;
	//@JsonIgnore
	@ManyToOne()
	@JoinColumn(name = "usuario",nullable = false)
	private  UsuarioOnDemand usuario;
	
	@ManyToOne()
	@JoinColumn(name="programa",nullable = false)
	private Programa programa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UsuarioOnDemand getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioOnDemand usuario) {
		this.usuario = usuario;
	}

	public Programa getPrograma() {
		return programa;
	}

	public void setPrograma(Programa programa) {
		this.programa = programa;
	}
	
	
}
