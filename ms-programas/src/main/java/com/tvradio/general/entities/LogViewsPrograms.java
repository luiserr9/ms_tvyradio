package com.tvradio.general.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "log_views_program")
public class LogViewsPrograms {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "program", nullable = false)
	private Programa program;

	@ManyToOne
	@JoinColumn(name = "user", nullable = false)
	private UsuarioOnDemand user;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_creation")
	private Date dateOfCreation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Programa getProgram() {
		return program;
	}

	public void setProgram(Programa program) {
		this.program = program;
	}

	public UsuarioOnDemand getUser() {
		return user;
	}

	public void setUser(UsuarioOnDemand user) {
		this.user = user;
	}

	public Date getDateOfCreation() {
		return dateOfCreation;
	}

	public void setDateOfCreation(Date dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public LogViewsPrograms( Programa program, UsuarioOnDemand user, Date dateOfCreation) {
		super();
	
		this.program = program;
		this.user = user;
		this.dateOfCreation = dateOfCreation;
	}

	public LogViewsPrograms() {
		super();
	}
	
	

}
