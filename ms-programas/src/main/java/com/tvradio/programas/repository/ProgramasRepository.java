package com.tvradio.programas.repository;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.dto.UsuarioPanelResp;
import com.tvradio.general.entities.Capitulos;
import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Programa;


@Transactional
public interface ProgramasRepository extends CrudRepository<Programa, Long> {

	@Modifying
	@Query(value = "UPDATE programa p SET p.status=0 WHERE p.programId=:Id ")
	public void deleteProgramById(@Param("Id") Long programId);

	@Query(value = "SELECT p FROM programa p WHERE p.status!=0 ")
	public List<Programa> getAllPrograms();
	
	
	
	
	@Query(value = "SELECT * FROM programa WHERE LOWER(name) COLLATE latin1_swedish_ci LIKE LOWER('%:busqueda%') AND status!=0",nativeQuery = true)
	public List<Programa> getLikeProgram(@Param("busqueda") String busqueda);

	
	
	
	@Query(value = "SELECT p fROM programa p WHERE p.programId=:Id AND p.status!=0")
	public Programa getProgramById(@Param("Id") Long Id);

	public Programa findByProgramId(Long programId);
//#######
	@Modifying
	@Query(value = "UPDATE programa  p SET p.isPrincipaldOfCategory=0 WHERE p.programId = p.programId AND  p.category=:cat ")
	public void updateMainOfCategory(@Param("cat") Categoria cat);

	public Programa findByName(String name);

	public Programa findByNameAndStatus(String name, Boolean status);

	public List<Programa> findByNameIgnoreCaseContainingAndStatus(String name, Boolean status);

	public List<Programa> findByCategoryAndStatus(Categoria category, Boolean status);
}
