package com.tvradio.programas.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Favoritos;
import com.tvradio.general.entities.Programa;
import com.tvradio.general.entities.UsuarioOnDemand;


@Transactional()
public interface FavoritosRepository extends CrudRepository<Favoritos, Long>{
	public List<Favoritos>  findByUsuario(UsuarioOnDemand usuario);
	
	
	public Favoritos  findByProgramaAndUsuario(Programa programa,UsuarioOnDemand usuario);
}

