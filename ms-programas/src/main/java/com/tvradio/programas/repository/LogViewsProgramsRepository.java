package com.tvradio.programas.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tvradio.general.entities.LogViewsPrograms;
import com.tvradio.general.entities.Programa;

@Transactional
public interface LogViewsProgramsRepository extends CrudRepository<LogViewsPrograms, Long> {

	@Query(value = "SELECT  COUNT(l)  FROM log_views_program l WHERE l.program=:programa ")
	public Integer getViewsByProgram(@Param("programa") Programa programa);

	@Query(value = "SELECT  COUNT(l)  FROM log_views_program l WHERE l.program=:programa AND l.dateOfCreation BETWEEN :date1 AND :date2 ")
	public Integer getViewsByProgramByInterval(@Param("programa") Programa programa, @Param("date1") Date date1,
			@Param("date2") Date date2);

	@Query(value = "SELECT COUNT(l) FROM  log_views_program l WHERE l.dateOfCreation=:today")
	public Integer getDailyReproductions(@Param("today") Date today);

	@Query(value = "SELECT COUNT(l) FROM  log_views_program l WHERE l.dateOfCreation BETWEEN :date1 AND :date2")
	public Integer getReproductionsByIntervalDate(@Param("date1") Date date1, @Param("date2") Date date2);

}