package com.tvradio.programas.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.tvradio.general.utilities.Comparador;
import com.tvradio.general.utilities.ComparadorAux;
import com.tvradio.general.utilities.Response;
import com.tvradio.programas.repository.FavoritosRepository;
import com.tvradio.programas.repository.LogViewsProgramsRepository;
import com.tvradio.programas.repository.ProgramasRepository;
import com.tvradio.programas.repository.chapterRepository;
import com.tvradio.general.dto.LikeProgramResp;
import com.tvradio.general.dto.LogViewDto;
import com.tvradio.general.dto.LogViewsProgramAux;
import com.tvradio.general.dto.LogViewsProgramResp;
import com.tvradio.general.dto.ProgramaResp;
import com.tvradio.general.dto.ProgramasResp;
import com.tvradio.general.dto.SeassonResp;
import com.tvradio.general.dto.UsuarioPanelResp;
import com.tvradio.general.entities.Capitulos;
import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Favoritos;
import com.tvradio.general.entities.LogViewsPrograms;
import com.tvradio.general.entities.Programa;
import com.tvradio.general.entities.UsuarioOnDemand;

@Service
public class ProgramasService {

	@Autowired
	private ProgramasRepository programaRepository;

	@Autowired
	private chapterRepository chapter;

	@Autowired
	private FavoritosRepository favorito;

	@Autowired
	private LogViewsProgramsRepository log;

	public Response getDailyReproductions() {
		try {
			return Response.ok(this.log.getDailyReproductions(new Date()));
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getReproductionsByIntervalDate(Date date1, Date date2) {
		try {
			return Response.ok(this.log.getReproductionsByIntervalDate(date1, date2));
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getProgramMostWhatchedByIntervalDate(Date date1, Date date2) {
		try {
			List<LogViewsProgramResp> resp = new ArrayList<LogViewsProgramResp>();
			List<Programa> ss = this.programaRepository.getAllPrograms();
			for (int i = 0; i < ss.size(); i++) {
				LogViewsProgramResp obj = new LogViewsProgramResp(ss.get(i).getProgramId(), ss.get(i).getName(),
						this.log.getViewsByProgramByInterval(ss.get(i), date1, date2), ss.get(i).getPictureMiniature());
				if (obj.getViews() != 0) {
					resp.add(obj);
				}
			}
			resp.sort(new Comparador());

			if (resp.size() > 9) {
				for (int i = resp.size() - 1; i >= 9; i--) {
					resp.remove(i);
				}
			}

			return Response.ok(resp);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

//""""""""""""""""""

	public Boolean exists1(Long id, List<Favoritos> favo) {
		for (int i = 0; i < favo.size(); i++) {
			if (favo.get(i).getPrograma().getProgramId() == id) {
				return true;
			}
		}
		return false;
	}

//****************************
	public Response getProgramMostWhatchedWhithLabelFvorite(UsuarioOnDemand user) {
		try {
			Long idfavo = (long) 0;
			List<LogViewsProgramAux> resp = new ArrayList<LogViewsProgramAux>();
			resp.clear();
			List<Programa> sss = this.programaRepository.getAllPrograms();
			for (int i = 0; i < sss.size(); i++) {
				LogViewsProgramAux obj = new LogViewsProgramAux(sss.get(i).getName(),
						this.log.getViewsByProgram(sss.get(i)), sss.get(i).getProgramId());
				if (obj.getViews() != 0) {
					resp.add(obj);
				}
			}
			resp.sort(new ComparadorAux());

			if (resp.size() > 9) {
				for (int i = resp.size() - 1; i >= 9; i--) {
					// System.out.println("" + resp.get(i).getNombre());
					resp.remove(i);
				}
			}

			if (resp.size() == 0) {
				return Response.ok("Sin vistas en programas");
			}

			// return Response.ok(resp);
			/////

			List<Favoritos> favo = this.favorito.findByUsuario(user);
			List<ProgramasResp> retorno = new ArrayList<ProgramasResp>();

			for (int i = 0; i < resp.size(); i++) {
				if (this.exists1(resp.get(i).getId(), favo) == true) {
					

					Programa se = this.programaRepository.findById(resp.get(i).getId()).get();
					for (int j = 0; j < favo.size(); j++) {

						if (se.getProgramId() == favo.get(j).getPrograma().getProgramId()) {
							System.out.println(favo.get(j).getId());
							idfavo = favo.get(j).getId();
						}

					}

					ProgramasResp pro = new ProgramasResp(se.getProgramId(), se.getName(), se.getDescription(),
							se.getNumChapters(), se.getIsPrincipaldOfCategory(), se.getPictureMiniature(),
							se.getPictureCover(), se.getPictureLogo(), se.getStatus(), se.getDateCreation(),
							se.getSeasons(), true,idfavo);
					retorno.add(pro);

				} else {

					Programa se = this.programaRepository.findById(resp.get(i).getId()).get();

					ProgramasResp pro = new ProgramasResp(se.getProgramId(), se.getName(), se.getDescription(),
							se.getNumChapters(), se.getIsPrincipaldOfCategory(), se.getPictureMiniature(),
							se.getPictureCover(), se.getPictureLogo(), se.getStatus(), se.getDateCreation(),
							se.getSeasons(), false,idfavo);
					retorno.add(pro);

				}
			}
			return Response.ok(retorno);

			////
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getProgramMostWhatched() {
		try {
			List<LogViewsProgramResp> resp = new ArrayList<LogViewsProgramResp>();
			List<Programa> ss = this.programaRepository.getAllPrograms();
			for (int i = 0; i < ss.size(); i++) {
				LogViewsProgramResp obj = new LogViewsProgramResp(ss.get(i).getProgramId(), ss.get(i).getName(),
						this.log.getViewsByProgram(ss.get(i)), ss.get(i).getPictureMiniature());
				if (obj.getViews() != 0) {
					resp.add(obj);
				}
			}
			resp.sort(new Comparador());

			if (resp.size() > 9) {
				for (int i = resp.size() - 1; i >= 9; i--) {
					resp.remove(i);
				}
			}
			return Response.ok(resp);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response insertViewProgram(LogViewDto dto) {
		try {
			LogViewsPrograms obj = new LogViewsPrograms(dto.getPrograma(), dto.getUsuario(), new Date());
			this.log.save(obj);
			return Response.created("OK");
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}

	}

	public Response getSeassonByIdProgram(Long id) {
		try {
			SeassonResp resp = new SeassonResp();
			resp.setSeasson(this.programaRepository.findById(id).get().getSeasons());
			return Response.ok(resp);
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Boolean exists(Programa prog, List<Favoritos> favo) {
		for (int i = 0; i < favo.size(); i++) {
			if (favo.get(i).getPrograma().equals(prog)) {
				return true;
			}
		}
		return false;
	}

	public Response getProgramaByCategoriaAndIdUserWithLabelFavorite(Categoria category, UsuarioOnDemand user) {
		
		try {
			Long idfavo = (long) 0;
			List<Favoritos> favo = this.favorito.findByUsuario(user);
			List<Programa> ss = this.programaRepository.findByCategoryAndStatus(category, true);
			for (int i = 0; i < ss.size(); i++) {
				ss.get(i).setNumChapters((this.getNumberOfChaptersByIdProgram((ss.get(i).getProgramId()))));
			}
			List<ProgramasResp> retorno = new ArrayList<ProgramasResp>();
			for (int i = 0; i < ss.size(); i++) {
				if (this.exists1(ss.get(i).getProgramId(), favo) == true) {
					//Long idfavo;
					for (int j = 0; j < favo.size(); j++) {

						if (ss.get(i).getProgramId() == favo.get(j).getPrograma().getProgramId()) {
							System.out.println(favo.get(j).getId());
							idfavo = favo.get(j).getId();
						}

					}

					ProgramasResp pro = new ProgramasResp(ss.get(i).getProgramId(), ss.get(i).getName(),
							ss.get(i).getDescription(), ss.get(i).getNumChapters(),
							ss.get(i).getIsPrincipaldOfCategory(), ss.get(i).getPictureMiniature(),
							ss.get(i).getPictureCover(), ss.get(i).getPictureLogo(), ss.get(i).getStatus(),
							ss.get(i).getDateCreation(), ss.get(i).getSeasons(), true,idfavo);
					retorno.add(pro);

				} else {
					ProgramasResp pro = new ProgramasResp(ss.get(i).getProgramId(), ss.get(i).getName(),
							ss.get(i).getDescription(), ss.get(i).getNumChapters(),
							ss.get(i).getIsPrincipaldOfCategory(), ss.get(i).getPictureMiniature(),
							ss.get(i).getPictureCover(), ss.get(i).getPictureLogo(), ss.get(i).getStatus(),
							ss.get(i).getDateCreation(), ss.get(i).getSeasons(), false,idfavo);
					retorno.add(pro);

				}

			}

			return Response.ok(retorno);
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response getProgramaByCategoria(Categoria category) {
		try {
			List<Programa> ss = this.programaRepository.findByCategoryAndStatus(category, true);
			for (int i = 0; i < ss.size(); i++) {
				ss.get(i).setNumChapters((this.getNumberOfChaptersByIdProgram((ss.get(i).getProgramId()))));
			}
			return Response.ok(ss);
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public int getNumberOfChaptersByIdProgram(Long idCategory) {
		return chapter.findAllByProgram(programaRepository.findById(idCategory).get()).size();
	}

	public Response getProgramById(Long id) {
		try {
			if (this.programaRepository.findById(id).get().getStatus() == true) {
				Programa ss = this.programaRepository.findById(id).get();
				ss.setNumChapters(this.getNumberOfChaptersByIdProgram(id)); // (this.getNumberOfPorogramsByIdCategory(id));
				return Response.success(ss);
			} else {
				return Response.notFound(null);
			}
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response getAllPrograms() {
		try {
			List<Programa> ss = programaRepository.getAllPrograms();
			for (int i = 0; i < ss.size(); i++) {
				ss.get(i).setNumChapters((this.getNumberOfChaptersByIdProgram((ss.get(i).getProgramId()))));
			}
			return Response.success(ss);
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response insertPrograms(Programa programa) {
		try {
			if (this.programaRepository.findByNameAndStatus(programa.getName(), true) == null) {
				if (this.programaRepository.findByName(programa.getName()) == null) {
					programa.setStatus(true);
					programa.setDateCreation(new Date());
					if (programa.getIsPrincipaldOfCategory() == true) {
						programaRepository.updateMainOfCategory(programa.getCategory());
					}
					programaRepository.save(programa);
					return Response.success("OK");
				} else {
					programa.setProgramId(this.programaRepository.findByName(programa.getName()).getProgramId());
					programa.setStatus(true);
					this.programaRepository.save(programa);
					return Response.accepted("OK");
				}
			} else {
				return Response.badRequest("Programa ya existe");
			}
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response deleteProgramById(Long id) {
		try {
			if (programaRepository.getProgramById(id) != null) {
				programaRepository.deleteProgramById(id);
				chapter.deleteAllChapters(id);
				return Response.success("Programa Borrado");
			} else {
				return Response.error("El programa no existe");
			}

		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}

	}

	public Response updatePrograms(Programa programa) {
		try {
			if (programaRepository.findByProgramId(programa.getProgramId()) != null) {

				if (programa.getIsPrincipaldOfCategory() == null) {

					programa.setIsPrincipaldOfCategory(
							programaRepository.findByProgramId(programa.getProgramId()).getIsPrincipaldOfCategory());
				}

				if (programa.getPictureCover() == null) {

					programa.setPictureCover(
							programaRepository.findByProgramId(programa.getProgramId()).getPictureCover());
				}
				if (programa.getPictureMiniature() == null) {

					programa.setPictureMiniature(
							programaRepository.findByProgramId(programa.getProgramId()).getPictureMiniature());
				}
				if (programa.getPictureLogo() == null) {

					programa.setPictureLogo(
							programaRepository.findByProgramId(programa.getProgramId()).getPictureLogo());
				}
				if (programa.getNumChapters() == null) {

					programa.setNumChapters(
							programaRepository.findByProgramId(programa.getProgramId()).getNumChapters());
				}
				if (programa.getName() == null) {

					programa.setName(programaRepository.findByProgramId(programa.getProgramId()).getName());
				}
				if (programa.getDateCreation() == null) {

					programa.setDateCreation(
							programaRepository.findByProgramId(programa.getProgramId()).getDateCreation());
				}
				if (programa.getCategory() == null) {

					programa.setCategory(programaRepository.findByProgramId(programa.getProgramId()).getCategory());
				}
				if (programa.getStatus() == null) {

					programa.setStatus(programaRepository.findByProgramId(programa.getProgramId()).getStatus());
				}

				if (programa.getSeasons() == null) {

					programa.setSeasons(programaRepository.findByProgramId(programa.getProgramId()).getSeasons());
				}

				if (programa.getIsPrincipaldOfCategory() == true) {
					programaRepository.updateMainOfCategory(programa.getCategory());
				}

				programaRepository.save(programa);
				return Response.accepted("OK");
			} else {
				return Response.error("No existe usuario a actualizar");
			}

		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response getLikePrograms(String name) {
		try {
			List<Programa> ss = programaRepository.findByNameIgnoreCaseContainingAndStatus(name, true);
			// List<Programa> ss = programaRepository.getLikeProgram(name);
			List<LikeProgramResp> resp = new ArrayList<LikeProgramResp>();
			for (int i = 0; i < ss.size(); i++) {
				LikeProgramResp obj = new LikeProgramResp(ss.get(i).getProgramId(), ss.get(i).getName(),
						ss.get(i).getPictureMiniature());
				resp.add(obj);
			}
			return Response.ok(resp);
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

}
