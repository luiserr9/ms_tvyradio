package com.tvradio.programas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tvradio.general.dto.IntervalDate;
import com.tvradio.general.dto.LogViewDto;
import com.tvradio.general.dto.ProgramasDTO;
import com.tvradio.general.entities.Capitulos;
import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Programa;
import com.tvradio.general.entities.UsuarioOnDemand;
import com.tvradio.general.utilities.Response;
import com.tvradio.programas.service.ProgramasService;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("/program")
public class ProgramasController {

	@Autowired
	private ProgramasService programasService;

	@PostMapping("/getProgramMostWhatchedWhithLabelFvorite")
	public Response getProgramMostWhatchedWhithLabelFvorite(@RequestBody UsuarioOnDemand user) {
		return this.programasService.getProgramMostWhatchedWhithLabelFvorite(user);
	}
	
	
	
	@GetMapping("/getDailyReproductions")
	public Response getDailyReproductions() {
		return this.programasService.getDailyReproductions();
	}
	
	
	
	@PostMapping("/getReproductionsByIntervalDate")
	public Response getReproductionsByIntervalDate(@RequestBody IntervalDate dates) {
		return this.programasService.getReproductionsByIntervalDate(dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/getProgramMostWhatchedByIntervalDate")
	public Response getProgramMostWhatchedByIntervalDate(@RequestBody IntervalDate dates) {
		return this.programasService.getProgramMostWhatchedByIntervalDate(dates.getDate1(), dates.getDate2());
	}

	@GetMapping("/getProgramMostWhatched")
	public Response getProgramMostWhatched() {
		return this.programasService.getProgramMostWhatched();
	}

	@PostMapping("/insertViewProgram")
	public Response insertViewProgram(@RequestBody LogViewDto log) {
		return this.programasService.insertViewProgram(log);
	}

	@PostMapping("/getSeassonByIdProgram")
	public Response getSeassonByIdProgram(@RequestBody Programa programa) {
		return this.programasService.getSeassonByIdProgram(programa.getProgramId());
	}

	@GetMapping("/getAllPrograms")
	public Response getAllPrograms() {
		return programasService.getAllPrograms();

	}

	@PostMapping("/getProgramaByCategoria")
	public Response getProgramaByCategoria(@RequestBody Categoria category) {
		return this.programasService.getProgramaByCategoria(category);
	}

	@PostMapping("/insertProgram")
	public Response insertProgram(@RequestBody Programa programa) {
		return programasService.insertPrograms(programa);
	}

	@PostMapping("/updateProgram")
	public Response updateProgram(@RequestBody Programa programa) {
		return programasService.updatePrograms(programa);
	}

	@PostMapping("/getProgramById")
	public Response getProgramById(@RequestBody() Programa programa) {
		try {
			if (programa.getProgramId() == null || programa.getProgramId() == 0) {
				return Response.error("Indice Inválido");
			} else {
				return programasService.getProgramById(programa.getProgramId());
			}
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	@PostMapping("/deleteProgramById")
	public Response deleteProgramById(@RequestBody() Programa programa) {
		return programasService.deleteProgramById(programa.getProgramId());
	}

	@PostMapping("/getLikePrograms")
	public Response getLikePrograms(@RequestBody Programa programa) {
		return programasService.getLikePrograms(programa.getName());
	}

	@PostMapping("/getProgramaByCategoriaAndIdUserWithLabelFavorite")
	public Response getProgramaByCategoriaAndIdUserWithLabelFavorite(@RequestBody ProgramasDTO prog) {
		System.out.println(prog.getCategoria().getCategoryId());
		System.out.println(prog.getUsuario().getUserOnDemandId());
		return this.programasService.getProgramaByCategoriaAndIdUserWithLabelFavorite(prog.getCategoria(),
				prog.getUsuario());
	}
}
