package com.tvradio.general.utilities;

import java.util.Comparator;

import com.tvradio.categoria.dto.LogViewsCategoryResp;


public class Comparador implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		LogViewsCategoryResp log1 = (LogViewsCategoryResp) o2;
		LogViewsCategoryResp log2 = (LogViewsCategoryResp) o1;
		return log1.getViews().compareTo(log2.getViews());

	}
}
