package com.tvradio.categoria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tvradio.general.entities.Categoria;
import com.tvradio.general.utilities.Response;
import com.tvradio.categoria.dto.IntervalDate;
import com.tvradio.categoria.dto.LogViewDto;
import com.tvradio.categoria.service.CategoriaService;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/category")
public class CategoriaController{
	
	@Autowired 
	private CategoriaService categoryService;
	
	@GetMapping("/getCategoryMostWhatched")
	public Response getCategoryMostWhatched() {
		return this.categoryService.getCategoryMostWhatched();
		
	}
	
	
	@PostMapping("/insertViewCategry")
	public Response insertViewProgram(@RequestBody LogViewDto log) {
		return this.categoryService.insertViewCategory(log);
	}
	
	@PostMapping("/getCategoryMostWhatchedByIntervalDate")
	public Response getCategoryMostWhatchedByIntervalDate(@RequestBody IntervalDate dates) {
		return this.categoryService.getCategoryMostWhatchedByIntervalDate(dates.getDate1(), dates.getDate2());
		
	}
	
	@PostMapping("/insertCategory")
	public Response insertCategory(@RequestBody Categoria category) {
		return categoryService.insertCategoory(category);
	}
	
	@GetMapping("/getallCategories")
	public Response   getallCategories() {
		return categoryService.getallCategories();	
	}
	
	@PostMapping("/getCategoryById")
	public Response getCategoryById(@RequestBody Categoria categoria) {
		return categoryService.getCategoryById(categoria.getCategoryId());
	}
	
	@PostMapping("/updateCategory")
	public Response  updateCategory(@RequestBody Categoria  categoria) {
		return categoryService.updateCategory(categoria);
	}
	
	@PostMapping("/deleteCategoryById")
	public Response deleteCategoryById(@RequestBody Categoria categoria) {
		return  categoryService.deleteCategoryById(categoria.getCategoryId());
	}
	
	
	
}