package com.tvradio.categoria.dto;

public class LogViewsCategoryResp {

	private String nombre;
	
	private Double views;

	
	
	public LogViewsCategoryResp() {
		super();
	}

	public LogViewsCategoryResp(String nombre, Double views) {
		super();
		this.nombre = nombre;
		this.views = views;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getViews() {
		return views;
	}

	public void setViews(Double views) {
		this.views = views;
	}
	
	
	
	
}
