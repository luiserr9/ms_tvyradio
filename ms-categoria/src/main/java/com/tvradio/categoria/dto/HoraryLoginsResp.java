package com.tvradio.categoria.dto;

public class HoraryLoginsResp {

	private Double primerIntervalo;

	private Double segundoIntervalo;

	private Double tercerIntervalo;

	private Double cuartoIntervalo;

	public HoraryLoginsResp() {
		super();
	}

	public HoraryLoginsResp(Double primerIntervalo, Double segundoIntervalo, Double tercerIntervalo,
			Double cuartoIntervalo) {
		super();
		this.primerIntervalo = primerIntervalo;
		this.segundoIntervalo = segundoIntervalo;
		this.tercerIntervalo = tercerIntervalo;
		this.cuartoIntervalo = cuartoIntervalo;
	}

	public Double getPrimerIntervalo() {
		return primerIntervalo;
	}

	public void setPrimerIntervalo(Double primerIntervalo) {
		this.primerIntervalo = primerIntervalo;
	}

	public Double getSegundoIntervalo() {
		return segundoIntervalo;
	}

	public void setSegundoIntervalo(Double segundoIntervalo) {
		this.segundoIntervalo = segundoIntervalo;
	}

	public Double getTercerIntervalo() {
		return tercerIntervalo;
	}

	public void setTercerIntervalo(Double tercerIntervalo) {
		this.tercerIntervalo = tercerIntervalo;
	}

	public Double getCuartoIntervalo() {
		return cuartoIntervalo;
	}

	public void setCuartoIntervalo(Double cuartoIntervalo) {
		this.cuartoIntervalo = cuartoIntervalo;
	}

	

}
