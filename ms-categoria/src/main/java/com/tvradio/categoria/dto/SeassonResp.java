package com.tvradio.categoria.dto;

public class SeassonResp {
	private Integer seasson;

	public SeassonResp(Integer seasson) {
		super();
		this.seasson = seasson;
	}

	public SeassonResp() {
		super();
	}

	public Integer getSeasson() {
		return seasson;
	}

	public void setSeasson(Integer seasson) {
		this.seasson = seasson;
	}
	

}
