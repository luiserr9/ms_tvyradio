package com.tvradio.categoria.dto;

import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Programa;
import com.tvradio.general.entities.UsuarioOnDemand;

public class LogViewDto {

	private UsuarioOnDemand usuario;
	private Categoria category;

	public LogViewDto() {
		super();
	}

	public LogViewDto(UsuarioOnDemand usuario, Categoria category) {
		super();
		this.usuario = usuario;
		this.category = category;
	}

	public UsuarioOnDemand getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioOnDemand usuario) {
		this.usuario = usuario;
	}

	public Categoria getCategory() {
		return category;
	}

	public void setCategory(Categoria category) {
		this.category = category;
	}

	
}
