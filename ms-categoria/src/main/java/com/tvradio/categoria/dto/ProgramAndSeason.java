package com.tvradio.categoria.dto;

import java.util.Date;

import com.tvradio.general.entities.Programa;

public class ProgramAndSeason {

	private Programa programa;
	private Integer seasson;

	public ProgramAndSeason() {
		super();
	}

	public ProgramAndSeason(Programa programa, Integer seasson) {
		super();
		this.programa = programa;
		this.seasson = seasson;
	}

	public Programa getPrograma() {
		return programa;
	}

	public void setPrograma(Programa programa) {
		this.programa = programa;
	}

	public Integer getSeasson() {
		return seasson;
	}

	public void setSeasson(Integer seasson) {
		this.seasson = seasson;
	}
	

	}
