package com.tvradio.categoria.dto;

import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.UsuarioOnDemand;

public class ProgramasDTO {

	private UsuarioOnDemand usuario;
	private Categoria categoria;

	public ProgramasDTO() {
		super();
	}

	public ProgramasDTO(UsuarioOnDemand usuario, Categoria categoria) {
		super();
		this.usuario = usuario;
		this.categoria = categoria;
	}

	public UsuarioOnDemand getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioOnDemand usuario) {
		this.usuario = usuario;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

}
