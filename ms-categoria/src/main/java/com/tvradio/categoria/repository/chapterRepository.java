package com.tvradio.categoria.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tvradio.general.entities.Capitulos;
@Transactional
public interface chapterRepository extends CrudRepository<Capitulos,Long>{
	
	@Modifying
	@Query(value="UPDATE capitulos  SET status=false WHERE programa =:ss",nativeQuery = true)
	public void deleteAllChapters(@Param("ss") Long ss);

}

