package com.tvradio.categoria.repository;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Programa;

@Transactional
public interface ProgramasRepository extends CrudRepository<Programa, Long> {

	@Modifying
	@Query(value = "UPDATE programa  SET status=false WHERE categoria =:ss", nativeQuery = true)
	public void deleteAllPrograms(@Param("ss") Long ss);

	public List<Programa> findAllByCategory(Categoria cat);

	public Programa findByName(String name);

	public Programa findByNameAndStatus(String name, Boolean status);
	

}
