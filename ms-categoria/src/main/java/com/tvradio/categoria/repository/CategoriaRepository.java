package com.tvradio.categoria.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Programa;



public interface CategoriaRepository extends CrudRepository<Categoria, Long>{
	
	@Query(value="SELECT c.* FROM categoria c WHERE c.status!=0 ORDER BY c.name ASC", nativeQuery = true)
	@Transactional()
	public List<Categoria> getallCategories();
	
	@Transactional(readOnly = true)
	public Categoria  findByCategoryIdAndStatus(Long categoryId,Boolean status);
		
	public Categoria findByName(String name);

	public Categoria findByNameAndStatus(String name, Boolean status);
	

	
	
	
}
