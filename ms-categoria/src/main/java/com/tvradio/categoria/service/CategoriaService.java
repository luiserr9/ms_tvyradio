package com.tvradio.categoria.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale.Category;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvradio.categoria.dto.LogViewDto;
import com.tvradio.categoria.dto.LogViewsCategoryResp;
import com.tvradio.categoria.repository.CategoriaRepository;
import com.tvradio.categoria.repository.LogViewsCategoryRepository;
import com.tvradio.categoria.repository.ProgramasRepository;
import com.tvradio.categoria.repository.chapterRepository;

import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.LogViewCategory;

import com.tvradio.general.entities.Programa;
import com.tvradio.general.utilities.Comparador;
import com.tvradio.general.utilities.Response;

@Service
public class CategoriaService {
	@Autowired
	private CategoriaRepository categoria;

	@Autowired
	private chapterRepository chapter;

	@Autowired
	private ProgramasRepository program;

	@Autowired
	private LogViewsCategoryRepository log;

	public Response insertViewCategory(LogViewDto dto) {
		try {
			LogViewCategory obj = new LogViewCategory(dto.getCategory(), dto.getUsuario(), new Date());
			this.log.save(obj);
			return Response.created("OK");
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getCategoryMostWhatchedByIntervalDate(Date date1, Date date2) {
		try {
			List<LogViewsCategoryResp> resp = new ArrayList<LogViewsCategoryResp>();
			List<LogViewsCategoryResp> respTotal = new ArrayList<LogViewsCategoryResp>();
			List<Categoria> ss = this.categoria.getallCategories();
			Double vistasTotales = 0.0;
			for (int i = 0; i < ss.size(); i++) {
				LogViewsCategoryResp obj = new LogViewsCategoryResp(ss.get(i).getName(),
						this.log.getViewsByCategoryByInterval(ss.get(i), date1, date2));
				vistasTotales = vistasTotales + obj.getViews();
				resp.add(obj);
			}
			vistasTotales = (1 / vistasTotales) * 100;

			for (int i = 0; i < ss.size(); i++) {
				LogViewsCategoryResp obj = new LogViewsCategoryResp(ss.get(i).getName(),
						this.log.getViewsByCategoryByInterval(ss.get(i), date1, date2) * vistasTotales);
				if (this.log.getViewsByCategoryByInterval(ss.get(i), date1, date2) != 0) {
					respTotal.add(obj);
				}
			}
			respTotal.sort(new Comparador());
			return Response.ok(respTotal);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getCategoryMostWhatched() {
		try {
			List<LogViewsCategoryResp> resp = new ArrayList<LogViewsCategoryResp>();
			List<LogViewsCategoryResp> respTotal = new ArrayList<LogViewsCategoryResp>();
			List<Categoria> ss = this.categoria.getallCategories();
			Double vistasTotales = 0.0;
			for (int i = 0; i < ss.size(); i++) {
				LogViewsCategoryResp obj = new LogViewsCategoryResp(ss.get(i).getName(),
						this.log.getViewsByCategory(ss.get(i)));
				vistasTotales = vistasTotales + obj.getViews();
				resp.add(obj);
			}
			vistasTotales = (1 / vistasTotales) * 100;
			for (int i = 0; i < ss.size(); i++) {
				LogViewsCategoryResp obj = new LogViewsCategoryResp(ss.get(i).getName(),
						this.log.getViewsByCategory(ss.get(i)) * vistasTotales);
				if (this.log.getViewsByCategory(ss.get(i)) != 0) {
					respTotal.add(obj);
				}
			}
			respTotal.sort(new Comparador());
			return Response.ok(respTotal);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	/// #####
	public int getNumberOfPorogramsByIdCategory(Long idCategory) {
		return program.findAllByCategory(categoria.findById(idCategory).get()).size();
	}

	public Response getallCategories() {
		try {
			List<Categoria> ss = categoria.getallCategories();
			for (int i = 0; i < ss.size(); i++) {
				ss.get(i).setNumberOfPrograms(this.getNumberOfPorogramsByIdCategory(ss.get(i).getCategoryId()));
			}
			return Response.success(categoria.getallCategories());
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response insertCategoory(Categoria category) {
		try {
			if(category.getDescription().length()>150) {
				return Response.badRequest("Tu descripción no debe ser mayor a 150 caracteres");
			}
			if (categoria.findByNameAndStatus(category.getName(), true) == null) {
				if (categoria.findByName(category.getName()) == null) {
					category.setDateOfCreation(new Date());
					category.setStatus(true);
					categoria.save(category);
					return Response.created("OK");
				} else {
					category.setCategoryId(this.categoria.findByName(category.getName()).getCategoryId());
					category.setStatus(true);
					this.categoria.save(category);
					return Response.accepted("OK");
				}
			} else {
				return Response.badRequest("Categoria ya registrada");
			}
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response getCategoryById(Long id) {
		try {
			// getNumberOfPorogramsByIdCategory idlong
			if (categoria.findByCategoryIdAndStatus(id, true) != null) {
				Categoria ss = categoria.findByCategoryIdAndStatus(id, true);
				ss.setNumberOfPrograms(this.getNumberOfPorogramsByIdCategory(id));
				return Response.success(ss);
			} else {
				return Response.notFound(null);
			}
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response deleteCategoryById(Long id) {
		try {
			if (this.categoria.findById(id).get().getStatus() == true) {
				// update the category
				Optional<Categoria> ss = this.categoria.findById(id);
				ss.get().setStatus(false);
				categoria.save(ss.get());
				// update the program
				program.deleteAllPrograms(id);

				// update the chapters
				List<Programa> sss = this.program.findAllByCategory(this.categoria.findById(id).get());
				for (int i = 0; i < sss.size(); i++) {
					this.chapter.deleteAllChapters(sss.get(i).getProgramId());
				}
				return Response.accepted(id);
			} else {
				return Response.notFound("No existe Categoria");
			}

		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response updateCategory(Categoria category) {
		try {
			if(category.getDescription().length()>150) {
				return Response.badRequest("Tu descripción no debe ser mayor a 150 caracteres");
			}
			if (categoria.findByCategoryIdAndStatus(category.getCategoryId(), true) != null) {
				Categoria ss = categoria.findByCategoryIdAndStatus(category.getCategoryId(), true);

				if (category.getPictureMiniature() == null) {
					category.setPictureMiniature(ss.getPictureMiniature());
				}
				if (category.getName() == null) {
					category.setName(ss.getName());
				}
				if (category.getDescription() == null) {
					category.setDescription(ss.getDescription());
				}
				if (category.getDateOfCreation() == null) {
					category.setDateOfCreation(ss.getDateOfCreation());
				}
				if (category.getCoverImage() == null) {
					category.setCoverImage(ss.getCoverImage());
				}
				if (category.getStatus() == null) {
					category.setStatus(ss.getStatus());
				}
				categoria.save(category);
				return Response.accepted("Registro actualizado");

			} else {
				return Response.notFound("No existe la categoria a actualizar");
			}
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

}