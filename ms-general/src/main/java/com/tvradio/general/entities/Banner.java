package com.tvradio.general.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "banner_campania")
public class Banner {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_baner")
	private Long idbanner;
	
	@Column(name = "name_ad")
	private String namead;
	
	@Column(name = "ruta_banner")
	private String rutabanner;
	
	@Column(name = "sex")
	private String sex;
	
	@Column(name = "age")
	private Integer age;
	
	@Column(name = "s_noticieros")
	private Boolean snoticieros;
	
	@Column(name = "s_opinion")
	private Boolean sopinion;
	
	@Column(name = "s_identidad")
	private Boolean sidentidad;
	
	@Column(name = "s_infantil")
	private Boolean sinfantil;
	
	@Column(name = "s_jovenes")
	private Boolean sjovenes;
	
	@Column(name = "s_especiales")
	private Boolean sespeciales;
	
	@Column(name = "s_entretenimiento")
	private Boolean sentretenimiento;
	
	@Column(name = "s_deportes")
	private Boolean sdeportes;
	
	@Column(name = "s_institucionales")
	private Boolean sinstitucionales;
	
	@Column(name = "s_mujeres")
	private Boolean smujeres;
	
	@Column(name = "s_capsulas")
	private Boolean scapsulas;
	

	@Column(name = "id_campania")
	private Long idcampania;

	public Banner() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdbanner() {
		return idbanner;
	}

	public void setIdbanner(Long idbanner) {
		this.idbanner = idbanner;
	}

	public String getNamead() {
		return namead;
	}

	public void setNamead(String namead) {
		this.namead = namead;
	}

	public String getRutabanner() {
		return rutabanner;
	}

	public void setRutabanner(String rutabanner) {
		this.rutabanner = rutabanner;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Boolean getSnoticieros() {
		return snoticieros;
	}

	public void setSnoticieros(Boolean snoticieros) {
		this.snoticieros = snoticieros;
	}

	public Boolean getSopinion() {
		return sopinion;
	}

	public void setSopinion(Boolean sopinion) {
		this.sopinion = sopinion;
	}

	public Boolean getSidentidad() {
		return sidentidad;
	}

	public void setSidentidad(Boolean sidentidad) {
		this.sidentidad = sidentidad;
	}

	public Boolean getSinfantil() {
		return sinfantil;
	}

	public void setSinfantil(Boolean sinfantil) {
		this.sinfantil = sinfantil;
	}

	public Boolean getSjovenes() {
		return sjovenes;
	}

	public void setSjovenes(Boolean sjovenes) {
		this.sjovenes = sjovenes;
	}

	public Boolean getSespeciales() {
		return sespeciales;
	}

	public void setSespeciales(Boolean sespeciales) {
		this.sespeciales = sespeciales;
	}

	public Boolean getSentretenimiento() {
		return sentretenimiento;
	}

	public void setSentretenimiento(Boolean sentretenimiento) {
		this.sentretenimiento = sentretenimiento;
	}

	public Boolean getSdeportes() {
		return sdeportes;
	}

	public void setSdeportes(Boolean sdeportes) {
		this.sdeportes = sdeportes;
	}

	public Boolean getSinstitucionales() {
		return sinstitucionales;
	}

	public void setSinstitucionales(Boolean sinstitucionales) {
		this.sinstitucionales = sinstitucionales;
	}

	public Boolean getSmujeres() {
		return smujeres;
	}

	public void setSmujeres(Boolean smujeres) {
		this.smujeres = smujeres;
	}

	public Boolean getScapsulas() {
		return scapsulas;
	}

	public void setScapsulas(Boolean scapsulas) {
		this.scapsulas = scapsulas;
	}

	public Long getIdcampania() {
		return idcampania;
	}

	public void setIdcampania(Long idcampania) {
		this.idcampania = idcampania;
	}

	

}
