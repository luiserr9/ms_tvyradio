package com.tvradio.general.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity(name = "tv_vivo_ondemand")
public class TvVivoOndemand {
	
	@Id
	@NotNull
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_tv")
	private Long idtv;
	
	@Column(name = "url_streaming")
	private String  urlstreaming;
	
	@Column(name = "title")
	private String titulo;
	
	@Column(name = "rute_img")
	private String rutaimg;

	public TvVivoOndemand() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdtv() {
		return idtv;
	}

	public void setIdtv(Long idtv) {
		this.idtv = idtv;
	}

	public String getUrlstreaming() {
		return urlstreaming;
	}

	public void setUrlstreaming(String urlstreaming) {
		this.urlstreaming = urlstreaming;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getRutaimg() {
		return rutaimg;
	}

	public void setRutaimg(String rutaimg) {
		this.rutaimg = rutaimg;
	}
	
	
	
}
