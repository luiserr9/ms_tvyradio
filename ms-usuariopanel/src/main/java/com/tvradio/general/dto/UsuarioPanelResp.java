package com.tvradio.general.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UsuarioPanelResp {

	private DateFormat dataformat = new SimpleDateFormat("dd-MM-yyyy");
	private Long  userpanelid;
	private String name; 
    private Integer profile; 
    private String email; 
    private String fechCreation ; 
    private String pass;
    
	public UsuarioPanelResp() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UsuarioPanelResp( Long userpanelid , String name, Integer profile, String email, Date fechCreation,
			String pass) {
		super();
		this.userpanelid = userpanelid;
		this.name = name;
		this.profile = profile;
		this.email = email;
		this.fechCreation = dataformat.format(fechCreation);
		this.pass = pass;
	}

	public Long getUserpanelid() {
		return userpanelid;
	}

	public void setUserpanelid(Long userpanelid) {
		this.userpanelid = userpanelid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getProfile() {
		return profile;
	}

	public void setProfile(Integer profile) {
		this.profile = profile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFechCreation() {
		return fechCreation;
	}

	public void setFechCreation(String fechCreation) {
		this.fechCreation = fechCreation;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	
    
}

