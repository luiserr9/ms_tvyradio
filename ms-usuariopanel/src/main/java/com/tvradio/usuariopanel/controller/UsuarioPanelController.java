package com.tvradio.usuariopanel.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tvradio.general.entities.Token;
import com.tvradio.general.entities.UsuarioPanel;
import com.tvradio.general.utilities.Response;
import com.tvradio.usuariopanel.service.UsuarioPanelService;


@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/usuariopanel")
public class UsuarioPanelController {
	
	@Autowired
	private UsuarioPanelService usuarioPanelService;
	
	@GetMapping("/obteneruaurios")
	public Response obteneruaurios() {
		return usuarioPanelService.getAllUser();
	}
	
	@PostMapping("/loginUsuario")
	public Response loginUsuario(@RequestBody UsuarioPanel usuarioPanel) {
		return usuarioPanelService.loginUser(usuarioPanel);
		
	}
	
	@PostMapping("/actualizarUsuario")
	public Response actualizarUsuario(@RequestBody UsuarioPanel usuarioPanel) {
		return usuarioPanelService.updateUser(usuarioPanel);
		
	}
	
	@PostMapping("/recuperarContra")
	public Response recuperarContra(@RequestBody UsuarioPanel CambioPass) {
		return usuarioPanelService.changePassUserPass(CambioPass);
	}
	
	@PostMapping("/recuperarContraDash")
	public Response recuperarContraDash(UsuarioPanel CambioPass) {
		return usuarioPanelService.changePassUserPass(CambioPass);
	}
	
	@PostMapping("/userValidate")
	public Response userValidate(@RequestBody UsuarioPanel usuarioPanel) {
		return usuarioPanelService.userValidate(usuarioPanel);
	}
	
	
	@PostMapping("/obtenerUsuarioId")
	public Response obtenerUsuarioId(@RequestBody UsuarioPanel usuarioPanel) {
		return usuarioPanelService.getByid(usuarioPanel.getUserpanelid());
	}
	
	@PostMapping("/registrarUsuario")
	public Response registrarUsuario (@RequestBody UsuarioPanel usuarioPanel) {
		return usuarioPanelService.InsertUserPanel(usuarioPanel);
	}
	
	
	@DeleteMapping("/eliminarUsuarioid")
	public Response eliminarUsuarioid(@RequestBody UsuarioPanel usuarioPanel) {
		return usuarioPanelService.deleteByid(usuarioPanel.getUserpanelid());
	}
	@GetMapping("/token")
	public Response token() {
	return Response.success( UUID.randomUUID().toString().toUpperCase() 
           );
	}
	
}
