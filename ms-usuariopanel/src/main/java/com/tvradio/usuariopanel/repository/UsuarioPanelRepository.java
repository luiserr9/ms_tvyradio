package com.tvradio.usuariopanel.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.dto.UsuarioPanelResp;
import com.tvradio.general.entities.UsuarioOnDemand;
import com.tvradio.general.entities.UsuarioPanel;
@Transactional
public interface UsuarioPanelRepository extends CrudRepository<UsuarioPanel, Long> {
	
	@Query("SELECT new com.tvradio.general.dto.UsuarioPanelResp(u.userpanelid, u.name, u.profile, u.email, u.fechCreation, u.pass )" 
	+"FROM usuario_panel u WHERE u.status = 1 ORDER BY u.userpanelid ASC ")
	List<UsuarioPanelResp>ConsultGeneralUsuarioPanelResp();
	
	@Modifying
	@Query(value = "UPDATE usuario_panel SET status=0 WHERE userpanelid =:userid ")
	void eliminarUsuarioPanel( @Param("userid") Long userid );
	
	@Query("SELECT u FROM usuario_panel u WHERE u.status = 1 AND u.email=:email AND u.pass=:pass")
	UsuarioPanel consulalogin (@Param("email") String string, @Param("pass") String pass );
	
	
	@Modifying
	@Query(value = "UPDATE usuario_panel SET pass=:pass WHERE email =:email ")
	void changePassword(@Param("pass") String pass,  @Param("email") String email);
	
	public UsuarioPanel findByEmail(String email);
	
	public UsuarioPanel   findByUserpanelidAndStatus(Long userpanelid,Boolean status);

	public UsuarioPanel findByEmailAndStatus(String email,Boolean status);
	
	

}
