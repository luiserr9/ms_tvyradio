package com.tvradio.usuariopanel.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.tvradio.general.utilities.Response;
import com.tvradio.general.dto.UsuarioPanelResp;
import com.tvradio.general.entities.Token;
import com.tvradio.general.entities.UsuarioPanel;
import com.tvradio.usuariopanel.repository.TokenRepository;
import com.tvradio.usuariopanel.repository.UsuarioPanelRepository;

import net.bytebuddy.asm.Advice.This;

@Service
public class UsuarioPanelService {

	@Autowired
	private UsuarioPanelRepository usuarioPanelRepository;

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private JavaMailSender mailSender;

	public Response updateUser(UsuarioPanel usuarioPanel) {
		try {
			if (usuarioPanelRepository.findByUserpanelidAndStatus(usuarioPanel.getUserpanelid(), true) != null) {

				if (usuarioPanel.getProfile() == null) {
					usuarioPanel.setProfile(
							usuarioPanelRepository.findById(usuarioPanel.getUserpanelid()).get().getProfile());
				}
				if (usuarioPanel.getPass() == null) {
					usuarioPanel
							.setPass(usuarioPanelRepository.findById(usuarioPanel.getUserpanelid()).get().getPass());
				}
				if (usuarioPanel.getName() == null) {
					usuarioPanel
							.setName(usuarioPanelRepository.findById(usuarioPanel.getUserpanelid()).get().getName());
				}
				if (usuarioPanel.getFechCreation() == null) {
					usuarioPanel.setFechCreation(
							usuarioPanelRepository.findById(usuarioPanel.getUserpanelid()).get().getFechCreation());
				}
				if (usuarioPanel.getEmail() == null) {
					usuarioPanel
							.setEmail(usuarioPanelRepository.findById(usuarioPanel.getUserpanelid()).get().getEmail());

				}
				if (usuarioPanel.isStatus() == false) {
					usuarioPanel
							.setStatus(usuarioPanelRepository.findById(usuarioPanel.getUserpanelid()).get().isStatus());
				}

				usuarioPanelRepository.save(usuarioPanel);

				return Response.success("OK");
			} else {
				return Response.error("El usuario no existe o está inabilitado");
			}

		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}

	}

	public Response getAllUser() {
		List<UsuarioPanelResp> usuarioPanel = new ArrayList<UsuarioPanelResp>();
		usuarioPanelRepository.ConsultGeneralUsuarioPanelResp().forEach(usuarioPanel::add);
		return Response.success(usuarioPanel);

	}

	public Response getByid(Long id) {

		if (usuarioPanelRepository.existsById(id)) {
			if (usuarioPanelRepository.findById(id).get().isStatus()) {
				return Response.success(usuarioPanelRepository.findById(id));
			} else {
				return Response.error("usuario esta inabilitado");
			}

		} else {
			return Response.error("No exite un registro");
		}

	}

	public Response updateUserPanel(UsuarioPanel usuarioPanel) {

		if (usuarioPanelRepository.save(usuarioPanel) != null) {
			return Response.success("Se actualizo con exito el usuario");
		} else {
			return Response.error("Ocurrio un error al actualizar el usuario");
		}
	}

	public Response InsertUserPanel(UsuarioPanel usuarioPanel) {
		try {
			if (usuarioPanelRepository.findByEmailAndStatus(usuarioPanel.getEmail(), true) == null) {
				if (usuarioPanelRepository.findByEmail(usuarioPanel.getEmail()) == null) {
					usuarioPanel.setFechCreation(new Date());
					usuarioPanel.setStatus(true);
					usuarioPanelRepository.save(usuarioPanel);
					return Response.created("ok");
				} else {
					usuarioPanel.setUserpanelid(
							usuarioPanelRepository.findByEmail(usuarioPanel.getEmail()).getUserpanelid());
					usuarioPanel.setStatus(true);
					usuarioPanelRepository.save(usuarioPanel);
					return Response.accepted("ok");
				}

			} else {
				return Response.error("El usuario ya existe");
			}

		} catch (Exception e) {
			return Response.error("Error al registrar un usuario" + e.getMessage());
		}

	}

	public Response loginUser(UsuarioPanel usuarioPanel) {

		if (usuarioPanelRepository.consulalogin(usuarioPanel.getEmail(), usuarioPanel.getPass()) != null) {
			return Response.success(usuarioPanelRepository.findByEmail(usuarioPanel.getEmail()));

		} else {
			return Response.error("usuario o contraseña son incorrectos");
		}

	}

	public void sendEmail(String to, String subject, String content) {

		SimpleMailMessage email = new SimpleMailMessage();

		email.setTo(to);
		email.setSubject(subject);
		email.setText(content);

		mailSender.send(email);
	}

	public Response changePassUserPass(UsuarioPanel CambioPass) {
		try {
			if (usuarioPanelRepository.findByEmail(CambioPass.getEmail()) != null) {

				if (usuarioPanelRepository.findByEmailAndStatus(CambioPass.getEmail(), true) != null) {
					usuarioPanelRepository.changePassword(CambioPass.getPass(), CambioPass.getEmail());

					// tokenRepository.eliminarToken(tok.getToken());
					return Response.success("Se cambio la contraseña");
				} else {

					return Response.notFound("El usuario está inabilitado");
				}
			} else {
				return Response.notFound("El usuario no existe");
			}

		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}

	}

	public String token() {
		return UUID.randomUUID().toString().toUpperCase();

	}

	public Response userValidate(UsuarioPanel usuarioPanel) {

		try {
			if (usuarioPanelRepository.findByEmailAndStatus(usuarioPanel.getEmail(), true) != null) {
				
				return Response.success("Ok");
			} else {
				return Response.error("El correo no esta registrado");
			}

		} catch (Exception e) {
			return Response.error("Error:" + e.getMessage());
		}

	}

	public Response deleteByid(Long userid) {

		if (usuarioPanelRepository.existsById(userid)) {

			if (usuarioPanelRepository.findById(userid).get().isStatus()) {
				try {
					usuarioPanelRepository.eliminarUsuarioPanel(userid);
					return Response.success("Se elimino con exito el pedido");

				} catch (Exception e) {
					return Response.error(e.getMessage());
				}
			} else {
				return Response.error("Registro inabilitado");
			}

		} else {

			return Response.error("No exite un registro");
		}

	}
}
