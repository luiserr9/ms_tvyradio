package com.tvradio.home.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tvradio.general.entities.Home;
import com.tvradio.general.utilities.Response;
import com.tvradio.home.service.HomeService;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("/home")
public class HomeController {
	@Autowired

	private HomeService service;

	@GetMapping("/getHome")
	public Response getHome() {
		return this.service.getHome();
	}
	
	@PostMapping("/updateHome")
	public Response updateHome(@RequestBody Home home) {
		return this.service.updateHome(home);
		
	}
	


}