package com.tvradio.home.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.tvradio.general.entities.Home;
@Transactional
public interface homeRepository extends CrudRepository<Home,Long>{
	
		

}

