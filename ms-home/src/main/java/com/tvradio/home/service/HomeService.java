package com.tvradio.home.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale.Category;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvradio.general.entities.Home;
import com.tvradio.general.utilities.Response;
import com.tvradio.home.repository.homeRepository;

@Service
public class HomeService {

	@Autowired
	private homeRepository repo;

	public Response getHome() {
		try {
			return Response.ok(this.repo.findAll());
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response updateHome(Home home) {
		try {
			if (this.repo.findById(home.getId()) != null) {
				Home aux = this.repo.findById(home.getId()).get();
				if (home.getPictureTree() == null) {
					home.setPictureTree(aux.getPictureTree());
				}
				if (home.getPictureTwo() == null) {
					home.setPictureTwo(aux.getPictureTwo());
				}
				if (home.getPictureOne() == null) {
					home.setPictureOne(aux.getPictureOne());
				}
				if (home.getPictureLogo() == null) {
					home.setPictureLogo(aux.getPictureLogo());
				}
				if (home.getDateCreation() == null) {
					home.setDateCreation(aux.getDateCreation());
				}
				this.repo.save(home);
				return Response.accepted("Actualizado");
			} else {
				return Response.notFound("No existe");
			}

		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

}