package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "home")
public class Home {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@Column(name = "picture_logo",  nullable = false)
	private String pictureLogo;
	
	@Column(name = "picture_one", nullable = false)
	private String pictureOne;
	
	@Column(name = "picture_two", nullable = false)
	private String pictureTwo;
	
	@Column(name = "picture_three",  nullable = false)
	private String pictureTree;

	
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_creation", nullable = false, updatable = false)
	public Date dateCreation;


	public Home() {
		super();
	}


	public Home(String pictureLogo, String pictureOne, String pictureTwo, String pictureTree, Date dateCreation) {
		super();
		this.pictureLogo = pictureLogo;
		this.pictureOne = pictureOne;
		this.pictureTwo = pictureTwo;
		this.pictureTree = pictureTree;
		this.dateCreation = dateCreation;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getPictureLogo() {
		return pictureLogo;
	}


	public void setPictureLogo(String pictureLogo) {
		this.pictureLogo = pictureLogo;
	}


	public String getPictureOne() {
		return pictureOne;
	}


	public void setPictureOne(String pictureOne) {
		this.pictureOne = pictureOne;
	}


	public String getPictureTwo() {
		return pictureTwo;
	}


	public void setPictureTwo(String pictureTwo) {
		this.pictureTwo = pictureTwo;
	}


	public String getPictureTree() {
		return pictureTree;
	}


	public void setPictureTree(String pictureTree) {
		this.pictureTree = pictureTree;
	}


	public Date getDateCreation() {
		return dateCreation;
	}


	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	

}
