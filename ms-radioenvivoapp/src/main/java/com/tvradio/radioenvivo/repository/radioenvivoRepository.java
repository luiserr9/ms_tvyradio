package com.tvradio.radioenvivo.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.tvradio.general.entities.RadioEnVivoApp;


@Transactional
public interface radioenvivoRepository extends CrudRepository<RadioEnVivoApp, Long>{

	public List<RadioEnVivoApp> findAllByStatus(Boolean status);
	
	
}
