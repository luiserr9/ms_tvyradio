package com.tvradio.radioenvivo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tvradio.general.entities.RadioEnVivoApp;
import com.tvradio.general.utilities.Response;
import com.tvradio.radioenvivo.service.radioenvivoService;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/liveradioapp")
public class radioenvivoController {

	@Autowired
	private radioenvivoService service;

	@GetMapping("/getAllStations")
	public Response getAllStations() {
		return service.getAllStations();
	}
	
	@PostMapping("/getStationById")
	public Response getStationById(@RequestBody RadioEnVivoApp radio) {
		return this.service.getStationById(radio.getId());
	}
	
	@PostMapping("/deleteStation")
	public Response deleteStation(@RequestBody RadioEnVivoApp radio) {
		return this.service.deleteStation(radio.getId());
	}
	@PostMapping("/insertStation")
	public Response insertStation(@RequestBody RadioEnVivoApp radio) {
		return this.service.insertStation(radio);
	}
	
	@PostMapping("/updateStation")
	public Response updateStation(@RequestBody RadioEnVivoApp radio) {
		return this.service.updateStation(radio);
	}
	

}