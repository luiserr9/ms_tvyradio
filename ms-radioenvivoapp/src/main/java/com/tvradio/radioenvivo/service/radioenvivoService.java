package com.tvradio.radioenvivo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale.Category;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import com.tvradio.general.entities.RadioEnVivoApp;
import com.tvradio.general.utilities.Response;
import com.tvradio.radioenvivo.repository.radioenvivoRepository;

@Service
public class radioenvivoService {
	@Autowired
	private radioenvivoRepository repository;

	public Response getAllStations() {
		try {
			return Response.ok(repository.findAllByStatus(true));
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response getStationById(Long id) {
		try {
			if (this.repository.findById(id).get().getStatus() == true) {
				return Response.ok(this.repository.findById(id));
			} else {
				return Response.notFound(null);
			}
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response deleteStation(Long id) {
		try {
			if (this.repository.findById(id).get().getStatus() == true) {
				RadioEnVivoApp ss = this.repository.findById(id).get();
				ss.setStatus(false);
				this.repository.save(ss);
				return Response.accepted(true);
			} else {
				return Response.notFound(false);
			}
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response insertStation(RadioEnVivoApp radioapp) {
		try {
			radioapp.setStatus(true);
			radioapp.setDate(new Date());
			this.repository.save(radioapp);
			return Response.created(true);
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response updateStation(RadioEnVivoApp radioapp) {
		try {
			if (this.repository.findById(radioapp.getId()) != null) {
				RadioEnVivoApp ss = this.repository.findById(radioapp.getId()).get();
				if (ss.getStatus() == true) {
					//
					if (radioapp.getStatus() == null) {
						radioapp.setStatus(ss.getStatus());
					}
					if (radioapp.getDate() == null) {
						radioapp.setDate(ss.getDate());
					}
					if (radioapp.getFrecuencia() == null) {
						radioapp.setFrecuencia(ss.getFrecuencia());
					}
					if (radioapp.getName() == null) {
						radioapp.setName(ss.getName());
					}
					if (radioapp.getPicture() == null) {
						radioapp.setPicture(ss.getPicture());
					}
					if (radioapp.getUrl() == null) {
						radioapp.setUrl(ss.getUrl());
					}
					this.repository.save(radioapp);
					return Response.accepted(true);

				} else {
					return Response.notFound("Deshabilitado");
				}
			} else {
				return Response.notFound(false);
			}
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

}