package com.tvradio.general.utilities;

import org.springframework.http.HttpStatus;

//TODO: Auto-generated Javadoc
/**
* The Class ErrorException.
*/
public class ErrorException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The developer message. */
	private String developerMessage;

	/** The status. */
	private HttpStatus status;

	/** The code. */
	private Integer code;

	/**
	 * Instantiates a new error exception.
	 *
	 * @param developerMessage the developer message
	 * @param status           the status
	 * @param code             the code
	 * @param message          the message
	 */
	public ErrorException(String developerMessage, HttpStatus status, Integer code, String message) {
		super(message);
		this.developerMessage = developerMessage;
		this.status = status;
		this.code = code;
	}

	/**
	 * Gets the developer message.
	 *
	 * @return the developer message
	 */
	public String getDeveloperMessage() {
		return developerMessage;
	}

	/**
	 * Sets the developer message.
	 *
	 * @param developerMessage the new developer message
	 */
	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public HttpStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

}