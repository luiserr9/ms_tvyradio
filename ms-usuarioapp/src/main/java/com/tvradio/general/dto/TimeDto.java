package com.tvradio.general.dto;

import com.tvradio.general.entities.UsuarioApp;

public class TimeDto {
	
	private UsuarioApp user;
	private Integer time;
	
	
	
	public TimeDto() {
		super();
	}
	public TimeDto(UsuarioApp user, Integer time) {
		super();
		this.user = user;
		this.time = time;
	}
	public UsuarioApp getUser() {
		return user;
	}
	public void setUser(UsuarioApp user) {
		this.user = user;
	}
	public Integer getTime() {
		return time;
	}
	public void setTime(Integer time) {
		this.time = time;
	}
	
	
	
}
