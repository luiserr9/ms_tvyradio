package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name = "time_app")

public class TimeApp {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user", nullable = false)
	private UsuarioApp user;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_creation")
	private Date dateOfCreation;

	@Column(name = "minutes")
	private Integer minutes;

	public TimeApp() {
		super();
	}

	public TimeApp( UsuarioApp user, Date dateOfCreation, Integer minutes) {
		super();
		
		this.user = user;
		this.dateOfCreation = dateOfCreation;
		this.minutes = minutes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UsuarioApp getUser() {
		return user;
	}

	public void setUser(UsuarioApp user) {
		this.user = user;
	}

	public Date getDateOfCreation() {
		return dateOfCreation;
	}

	public void setDateOfCreation(Date dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Integer getMinutes() {
		return minutes;
	}

	public void setMinutes(Integer minutes) {
		this.minutes = minutes;
	}

	
}
