package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity(name = "token")
@Table(name="token")
public class Token {

	@Id
	@NotNull
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id",updatable = false,nullable = false)
	private Long idToken;
	
	
	@Column(name = "token")
	private  String token;
	
	@Column(name = "activo")
	private Integer activo;
	
	@Column(name = "id_user")
	private Long iduser;
	
	@Column (name="identified")
	private Character identified;
	
	@Column(name = "fecha_registro")
	private Date fecharegistro;
	
	@Column(name = "new")
	private String newpass;
	
	
	public Token() {
		super();
	}
	

	public Character getIdentified() {
		return identified;
	}


	public void setIdentified(Character identified) {
		this.identified = identified;
	}


	public Long getIdToken() {
		return idToken;
	}


	public void setIdToken(Long idToken) {
		this.idToken = idToken;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public Integer getActivo() {
		return activo;
	}


	public void setActivo(Integer activo) {
		this.activo = activo;
	}


	public Long getIduser() {
		return iduser;
	}


	public void setIduser(Long iduser) {
		this.iduser = iduser;
	}


	public Date getFecharegistro() {
		return fecharegistro;
	}


	public void setFecharegistro(Date fecharegistro) {
		this.fecharegistro = fecharegistro;
	}


	public String getNewpass() {
		return newpass;
	}


	public void setNewpass(String newpass) {
		this.newpass = newpass;
	}
	
	
	
	
	
}
