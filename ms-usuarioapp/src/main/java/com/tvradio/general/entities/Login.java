package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "login")
public class Login {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user", nullable = false)
	private UsuarioApp user;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_creation")
	private Date dateOfCreation;

	@Column(name = "identified")
	private Character identified;
	
	
	
	

	public Login() {
		super();
	}

	public Login( UsuarioApp user, Date dateOfCreation, Character identified) {
		super();
		this.user = user;
		this.dateOfCreation = dateOfCreation;
		this.identified = identified;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UsuarioApp getUser() {
		return user;
	}

	public void setUser(UsuarioApp user) {
		this.user = user;
	}

	public Date getDateOfCreation() {
		return dateOfCreation;
	}

	public void setDateOfCreation(Date dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Character getIdentified() {
		return identified;
	}

	public void setIdentified(Character identified) {
		this.identified = identified;
	}

}
