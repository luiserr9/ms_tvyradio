package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name = "usuario_app")
public class UsuarioApp {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_app_id", nullable = false, unique = true)
	private Long userAppId;

	@Column(name = "name", nullable = false, updatable = true)
	private String name;

	@Column(name = "email", unique = true, nullable = false, updatable = true)
	private String email;

	@Column(name = "password", updatable = true)
	private String password;

	@Column(name = "location")
	private String location;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_creation", nullable = false, updatable = false)
	private Date dateCreation;

	@Column(name = "staus", nullable = false)
	private Boolean status;

	@Column(name = "session", nullable = false)
	private Boolean session;

	@JsonFormat(timezone = "UTC", pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "session_date", nullable = false)
	private Date sessionDate;
	
	@Column(name="token_device")
	private String tokenDevice;
	
	@Column(name="push_active", nullable = false)
	private Boolean pushActive;
	
	
	

	public String getTokenDevice() {
		return tokenDevice;
	}

	public void setTokenDevice(String tokenDevice) {
		this.tokenDevice = tokenDevice;
	}

	public Boolean getSession() {
		return session;
	}

	public void setSession(Boolean session) {
		this.session = session;
	}

	public Date getSessionDate() {
		return sessionDate;
	}

	public void setSessionDate(Date sessionDate) {
		this.sessionDate = sessionDate;
	}

	public Long getUserAppId() {
		return userAppId;
	}

	public void setUserAppId(Long userAppId) {
		this.userAppId = userAppId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

	public Boolean getPushActive() {
		return pushActive;
	}

	public void setPushActive(Boolean pushActive) {
		this.pushActive = pushActive;
	}

	public UsuarioApp() {
		super();
	}

}
