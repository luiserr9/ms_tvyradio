package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity(name = "categoria")
@Table(name = "categoria")
public class Categoria {

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "category_id", nullable = false, unique = true)
	private Long categoryId;

	@Column(name = "name", unique = true, nullable = false)
	private String name;

	@Column(name = "nom_program")
	Integer numberOfPrograms;

	@JsonFormat(timezone = "UTC",pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "fech_creation", updatable = false, nullable = false)
	private Date dateOfCreation;

	@Column(name = "cover_image", nullable = false)
	private String coverImage;

	@Column(name = "picture_miniature", nullable = false)
	private String pictureMiniature;

	@Column(name = "status", nullable = false)
	private Boolean status;

	@Column(name = "description", nullable = false)
	private String description;
	
	

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumberOfPrograms() {
		return numberOfPrograms;
	}

	public void setNumberOfPrograms(Integer numberOfPrograms) {
		this.numberOfPrograms = numberOfPrograms;
	}

	public Date getDateOfCreation() {
		return dateOfCreation;
	}

	public void setDateOfCreation(Date dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public String getCoverImage() {
		return coverImage;
	}

	public void setCoverImage(String coverImage) {
		this.coverImage = coverImage;
	}

	public String getPictureMiniature() {
		return pictureMiniature;
	}

	public void setPictureMiniature(String pictureMiniature) {
		this.pictureMiniature = pictureMiniature;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
