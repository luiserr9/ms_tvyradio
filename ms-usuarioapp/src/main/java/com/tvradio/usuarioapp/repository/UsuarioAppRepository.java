package com.tvradio.usuarioapp.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.UsuarioApp;

@Transactional
public interface UsuarioAppRepository extends CrudRepository<UsuarioApp, Long> {

	public List<UsuarioApp> findAllByStatus(Boolean status);

	public UsuarioApp findByEmailAndStatus(String email, Boolean status);

	public UsuarioApp findByEmail(String email);

	public UsuarioApp findByEmailAndPassword(String email, String password);
	
	public UsuarioApp findByUserAppIdAndStatus(Long userAppId,Boolean status);
	
	public List<UsuarioApp> findAllByLocationAndStatusAndPushActive(String location,Boolean status,Boolean pushActives);
	

	@Modifying
	@Query(value = "UPDATE usuario_app set password=:pass WHERE email =:email  ")
	void changePassword(@Param("pass") String pass, @Param("email") String email);

	@Query(value = "SELECT COUNT(u) FROM usuario_app u WHERE u.status=true AND u.dateCreation BETWEEN :date1 AND :date2 ")
	public Double getUserByDateInterval(@Param("date1") Date date1, @Param("date2") Date date2);

	@Query(value = "SELECT COUNT(u) FROM usuario_app u WHERE u.status=true ")
	public Double getNumberOfUsers();

	@Query(value = "SELECT count(u) FROM usuario_app u WHERE MONTH(u.dateCreation)=:mes AND YEAR(u.dateCreation)=:anio AND  u.status=true")
	public Double getUsersByActualMonthAndYear(@Param("mes") Integer mes, @Param("anio") Integer anio);

	@Query(value = "SELECT COUNT(u) FROM usuario_app u WHERE u.status=true AND u.session=true")
	public Double getAllUniqueUsers();

	@Query(value = "SELECT COUNT(u) FROM usuario_app u WHERE u.status=true AND u.session=true AND u.sessionDate BETWEEN :date1 AND :date2")
	public Double getUniqueUseresByIntervalDate(@Param("date1") Date date1, @Param("date2") Date date2);

	@Query(value = "SELECT count(u) FROM usuario_app u WHERE MONTH(u.sessionDate)=:mes AND YEAR(u.sessionDate)=:anio AND  u.status=true AND u.session=true")
	public Double getUniqueUsersByActualMonthAndYear(@Param("mes") Integer mes, @Param("anio") Integer anio);

}
