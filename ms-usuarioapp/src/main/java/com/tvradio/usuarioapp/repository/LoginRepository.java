package com.tvradio.usuarioapp.repository;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.Login;
import com.tvradio.general.entities.Token;

public interface LoginRepository extends CrudRepository<Login, Long> {

	@Query(value = "SELECT COUNT(l) FROM login l WHERE l.identified=:identified AND l.dateOfCreation BETWEEN :date1 AND :date2 ")
	public Double getLoginsByIntervalDate(@Param("date1") Date date1, @Param("date2") Date date2,
			@Param("identified") Character identified);

	@Query(value = "SELECT count(l) FROM login l WHERE MONTH(l.dateOfCreation)=:mes AND YEAR(l.dateOfCreation)=:anio and l.identified=:identificador")
	public Double getLoginsByActualMonthAndYear(@Param("mes") Integer mes, @Param("anio") Integer anio,@Param("identificador") Character identificador);

	@Query(value = "SELECT COUNT(l) FROM login l WHERE l.identified=:identified")
	public Double getAllLogins(@Param("identified") Character identified);

}
