package com.tvradio.usuarioapp.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.TimeApp;


public interface TimeRepository extends CrudRepository<TimeApp, Long> {

	@Query(value="SELECT t FROM time_app t")
	public List<TimeApp> getAllRegistries();
	
	@Query(value="SELECT t FROM time_app t WHERE t.dateOfCreation BETWEEN  :date1 AND  :date2")
	public List<TimeApp> getAllRegistriesByInterval(@Param("date1") Date date1, @Param("date2") Date date2);
	
	@Query(value = "SELECT t FROM time_app t WHERE MONTH(t.dateOfCreation)=:mes AND YEAR(t.dateOfCreation)=:anio ")
	public List<TimeApp> getHoursByCurrentMothAndYear(@Param("mes") Integer mes, @Param("anio") Integer anio);
	

	
}
