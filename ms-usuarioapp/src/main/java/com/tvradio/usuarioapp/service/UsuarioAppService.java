package com.tvradio.usuarioapp.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.tvradio.general.utilities.Response;
import com.tvradio.usuarioapp.repository.LoginRepository;
import com.tvradio.usuarioapp.repository.TimeRepository;
import com.tvradio.usuarioapp.repository.TokenRepository;
import com.tvradio.usuarioapp.repository.UsuarioAppRepository;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;

import com.tvradio.general.entities.Login;
import com.tvradio.general.entities.TimeApp;
import com.tvradio.general.entities.Token;
import com.tvradio.general.entities.UsuarioApp;

import net.bytebuddy.asm.Advice.This;

@Service
public class UsuarioAppService {

	@Autowired
	private UsuarioAppRepository user;
	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private TokenRepository token;

	@Autowired
	private LoginRepository log;

	@Autowired

	private TimeRepository time;

	public Response getAllUsersByLocation(String location) {
		try {
			return Response.ok(this.user.findAllByLocationAndStatusAndPushActive(location, true, true));

		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response middleLogin(String correo) {
		try {
			if (this.user.findByEmail(correo) != null) {
				if (this.user.findByEmail(correo).getStatus() == true) {
					// insertar en la tabala login
					UsuarioApp ss = this.user.findByEmail(correo);
					this.log.save(new Login(ss, new Date(), 'A'));
					ss.setSessionDate(new Date());
					ss.setSession(true);
					this.user.save(ss);
					return Response.ok(ss);

				} else {
					return Response.notFound("Usuario Inabilitado");
				}
			} else {
				return Response.notFound("El usuario no existe");
			}
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getAllHours() {
		try {
			Double minutes = 0.0;
			List<TimeApp> ss = this.time.getAllRegistries();
			for (TimeApp timeApp : ss) {
				minutes = minutes + timeApp.getMinutes();
			}
			return Response.ok(minutes / 60);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getPorcentoOfHours(Date date1, Date date2) {
		try {
			Double numeroAnterior = 0.0;
			List<TimeApp> ss = this.time.getAllRegistriesByInterval(date1, date2);
			for (TimeApp timeApp : ss) {
				numeroAnterior = numeroAnterior + timeApp.getMinutes();
			}
			numeroAnterior = numeroAnterior / 60;
			Double numeroActual = 0.0;
			List<TimeApp> si = this.time.getHoursByCurrentMothAndYear(this.getCurrentMonth(), this.getCurrentYear());
			for (TimeApp timeApp : si) {
				numeroActual = numeroActual + timeApp.getMinutes();
			}
			numeroActual = numeroActual / 60;
			if (numeroAnterior == 0 && numeroActual == 0) {
				return Response.ok(0 + "%");
			}
			if (numeroActual == 0) {
				return Response.ok("-" + numeroAnterior * 100 + "%");
			}
			if (numeroAnterior == 0) {
				return Response.ok(numeroActual * 100 + "%");
			}

			Double diferencia = numeroActual - numeroAnterior;
			if (diferencia == 0) {
				return Response.ok("0%");
			}
			Double tasa = 1 / numeroActual * 100;
			Double relacion = numeroAnterior * tasa;
			return Response.ok((100 - relacion) + "%");
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getAllHoursByIntervalDate(Date date1, Date date2) {
		try {
			Double minutes = 0.0;
			List<TimeApp> ss = this.time.getAllRegistriesByInterval(date1, date2);
			for (TimeApp timeApp : ss) {
				minutes = minutes + timeApp.getMinutes();
			}
			return Response.ok(minutes / 60);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response addTimeSession(Integer tiempo, UsuarioApp user) {
		try {
			TimeApp aux = new TimeApp(user, new Date(), tiempo);
			this.time.save(aux);
			return Response.created("OK");
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getPorcentOfUniqueUsers(Date date1, Date date2) {
		Double numeroAnterior = this.user.getUniqueUseresByIntervalDate(date1, date2);
		Double numeroActual = this.user.getUniqueUsersByActualMonthAndYear(this.getCurrentMonth(),
				this.getCurrentYear());
		if (numeroAnterior == 0 && numeroActual == 0) {
			return Response.ok(0 + "%");
		}
		if (numeroActual == 0) {
			return Response.ok("-" + numeroAnterior * 100 + "%");
		}
		if (numeroAnterior == 0) {
			return Response.ok(numeroActual * 100 + "%");
		}

		Double diferencia = numeroActual - numeroAnterior;
		if (diferencia == 0) {
			return Response.ok("0%");
		}
		Double tasa = 1 / numeroActual * 100;
		Double relacion = numeroAnterior * tasa;
		return Response.ok((100 - relacion) + "%");

	}

	public Response getUniqueUseresByIntervalDate(Date date1, Date date2) {
		try {
			return Response.ok(this.user.getUniqueUseresByIntervalDate(date1, date2));

		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response getAllUniqueUsers() {
		try {
			return Response.ok(this.user.getAllUniqueUsers());

		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response getPorcentOfRegistries(Date date1, Date date2) {
		Double numeroAnterior = this.user.getUserByDateInterval(date1, date2);
		Double numeroActual = this.user.getUsersByActualMonthAndYear(this.getCurrentMonth(), this.getCurrentYear());
		if (numeroAnterior == 0 && numeroActual == 0) {
			return Response.ok(0 + "%");
		}
		if (numeroActual == 0) {
			return Response.ok("-" + numeroAnterior * 100 + "%");
		}
		if (numeroAnterior == 0) {
			return Response.ok(numeroActual * 100 + "%");
		}

		Double diferencia = numeroActual - numeroAnterior;
		if (diferencia == 0) {
			return Response.ok("0%");
		}
		Double tasa = 1 / numeroActual * 100;
		Double relacion = numeroAnterior * tasa;
		return Response.ok((100 - relacion) + "%");

	}

	public Response getAllRegistries() {
		try {
			return Response.ok(this.user.getNumberOfUsers());
		} catch (Exception e) {
			return Response.error("Error insesperado: " + e.getMessage());
		}

	}

	public Response getAllRegistriesByIntervalDates(Date date1, Date date2) {
		try {
			return Response.ok(this.user.getUserByDateInterval(date1, date2));
		} catch (Exception e) {
			return Response.error("Error insesperado: " + e.getMessage());
		}

	}

	public Integer getCurrentMonth() {
		Calendar cal = Calendar.getInstance();
		// obntiene del 0 al 11
		return cal.get(Calendar.MONTH) + 1;
	}

	public Integer getCurrentYear() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.YEAR);
	}

	public Response getPorcentOfLogins(Date date1, Date date2) {
		Double numeroAnterior = this.log.getLoginsByIntervalDate(date1, date2, 'A');
		Double numeroActual = this.log.getLoginsByActualMonthAndYear(getCurrentMonth(), getCurrentYear(), 'A');
		if (numeroAnterior == 0 && numeroActual == 0) {
			return Response.ok(0 + "%");
		}
		if (numeroActual == 0) {
			return Response.ok("-" + numeroAnterior * 100 + "%");
		}
		if (numeroAnterior == 0) {
			return Response.ok(numeroActual * 100 + "%");
		}

		Double diferencia = numeroActual - numeroAnterior;
		if (diferencia == 0) {
			return Response.ok("0%");
		}
		Double tasa = 1 / numeroActual * 100;
		Double relacion = numeroAnterior * tasa;
		return Response.ok((100 - relacion) + "%");

	}

	public Response getLoginsByIntervalDate(Date date1, Date date2) {
		try {
			return Response.ok(log.getLoginsByIntervalDate(date1, date2, 'A'));
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response getAllLogins() {
		try {
			return Response.ok(log.getAllLogins('A'));
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public void sendEmail(String to, String subject, String content) {
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(to);
		email.setSubject(subject);
		email.setText(content);
		mailSender.send(email);
	}

	public String generateToken() {
		return UUID.randomUUID().toString().toUpperCase();
	}

	public Response changePassUserPass(UsuarioApp CambiosPass) {
		try {
			if (user.findByEmail(CambiosPass.getEmail()) != null) {
				if (user.findByEmailAndStatus(CambiosPass.getEmail(), true) != null) {
					user.changePassword(CambiosPass.getPassword(), CambiosPass.getEmail());
					//

					return Response.success("Se cambio la contraseña");
				} else {
					return Response.notFound("El usuario está inabilitado");
				}
			} else {

				return Response.notFound("El usuario no existe");
			}
			//

		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response sendEmailToken(UsuarioApp usuario) {
		try {
			if (this.user.findByEmail(usuario.getEmail()) != null) {
				if (this.user.findByEmail(usuario.getEmail()).getStatus() == true) {
					// insertando token
					String to = this.generateToken();
					this.token.insertToken(to, 1, user.findByEmail(usuario.getEmail()).getUserAppId(), new Date(), 'A');
					// enviando email
					sendEmail(usuario.getEmail(), "Tu token", to);
					return Response.accepted("Se envio un correo con el codigo de verificacion");
				} else {
					return Response.notFound("No existe inabilitado");
				}
			} else {
				return Response.notFound("No existe usuario");
			}
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response getAllUsers() {
		try {
			return Response.ok(user.findAllByStatus(true));
		} catch (Exception e) {
			return Response.error("Error insesperado: " + e.getMessage());
		}
	}

	public Response getUserById(Long id) {
		try {
			if (user.findById(id).get().getStatus() == true) {
				return Response.ok(user.findById(id));
			} else {
				return Response.notFound("No existe");
			}
		} catch (Exception e) {
			return Response.error("Error insesperado: " + e.getMessage());
		}
	}

	public Response closeSession(UsuarioApp user) {
		try {
			if (this.user.findByEmailAndStatus(user.getEmail(), true) != null) {
				UsuarioApp app = this.user.findByEmailAndStatus(user.getEmail(), true);
				app.setSessionDate(new Date());
				app.setSession(false);
				return Response.success("OK");
			} else {
				return Response.badRequest("El usuario no existe");
			}
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response login(UsuarioApp user) {
		try {
			if (this.user.findByEmail(user.getEmail()) != null) {
				if (this.user.findByEmail(user.getEmail()).getStatus() == true) {
					if (this.user.findByEmailAndPassword(user.getEmail(), user.getPassword()) != null) {
						// insertar en la tabala login
						UsuarioApp ss = this.user.findByEmail(user.getEmail());
						this.log.save(new Login(ss, new Date(), 'A'));
						ss.setSessionDate(new Date());
						ss.setSession(true);
						this.user.save(ss);
						return Response.ok(ss);
					} else {
						return Response.badRequest("pass incorrecto");
					}
				} else {
					return Response.notFound("Usuario Inabilitado");
				}
			} else {
				return Response.notFound("El usuario no existe");
			}
		} catch (Exception e) {
			return Response.error("Error insesperado: " + e.getMessage());
		}
	}

	public Response updateUser(UsuarioApp usuario) {
		try {
			if (this.user.findById(usuario.getUserAppId()).get().getStatus() == true) {
				UsuarioApp ss = this.user.findById(usuario.getUserAppId()).get();
				if (usuario.getEmail() != null) {
					ss.setEmail(usuario.getEmail());
				}
				if (usuario.getPassword() != null) {
					ss.setPassword(usuario.getPassword());
				}
				if (usuario.getName() != null) {
					ss.setName(usuario.getName());
				}
				if (usuario.getLocation() != null) {
					ss.setLocation(usuario.getLocation());
				}
				if (usuario.getTokenDevice() != null) {
					ss.setTokenDevice(usuario.getTokenDevice());
				}
				if (usuario.getPushActive() != null) {
					ss.setPushActive(usuario.getPushActive());
				}
				this.user.save(ss);
				return Response.accepted("Actualizado correctamente");

			} else {
				return Response.notFound("No existe");
			}
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response insertUser(UsuarioApp usuario) {
		try {
			if (this.user.findByEmailAndStatus(usuario.getEmail(), true) == null) {
				if (this.user.findByEmail(usuario.getEmail()) == null) {
					usuario.setDateCreation(new Date());
					usuario.setStatus(true);
					usuario.setSession(true);
					usuario.setPushActive(true);
					usuario.setSessionDate(new Date());
					this.user.save(usuario);
					return Response.created(usuario);
				} else {
					usuario.setUserAppId(this.user.findByEmail(usuario.getEmail()).getUserAppId());
					usuario.setStatus(true);
					usuario.setSession(true);
					usuario.setDateCreation(new Date());
					usuario.setSessionDate(new Date());
					usuario.setPushActive(true);
					this.user.save(usuario);
					return Response.accepted("Usuario creado");
				}
			} else {
				return Response.badRequest("El usuario ya existe");
			}
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

}
