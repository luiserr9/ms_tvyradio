package com.tvradio.usuarioapp.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tvradio.general.dto.IntervalDate;
import com.tvradio.general.dto.TimeDto;
import com.tvradio.general.entities.Token;
import com.tvradio.general.entities.UsuarioApp;

import com.tvradio.general.utilities.Response;
import com.tvradio.usuarioapp.service.UsuarioAppService;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("/usuarioApp")
public class UsuarioAppController {
	@Autowired
	private UsuarioAppService service;

	
	
	@PostMapping("/getAllUsersByLocation")
	public Response getAllUsersByLocation(@RequestBody UsuarioApp user) {
		return this.service.getAllUsersByLocation(user.getLocation());
	}
	
	@PostMapping("/middleLogin")
	public Response middleLogin(@RequestBody UsuarioApp user) {
		return this.service.middleLogin(user.getEmail());
	}

	@PostMapping("/closeSession")
	public Response closeSession(@RequestBody UsuarioApp user) {
		return this.service.closeSession(user);
	}

	@PostMapping("/addTimeSession")
	public Response addTimeSession(@RequestBody TimeDto dto) {
		return this.service.addTimeSession(dto.getTime(), dto.getUser());
	}

	@GetMapping("/getAllHours")
	public Response getAllHours() {
		return service.getAllHours();
	}

	@PostMapping("/getPorcentoOfHours")
	public Response getPorcentoOfHours(@RequestBody IntervalDate dates) {
		return this.service.getPorcentoOfHours(dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/getAllHoursByIntervalDate")
	public Response getAllHoursByIntervalDate(@RequestBody IntervalDate dates) {
		return this.service.getAllHoursByIntervalDate(dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/getPorcentOfUniqueUsers")
	public Response getPorcentOfUniqueUsers(@RequestBody IntervalDate dates) {
		return service.getPorcentOfUniqueUsers(dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/getUniqueUseresByIntervalDate")
	public Response getUniqueUseresByIntervalDate(@RequestBody IntervalDate dates) {
		return service.getUniqueUseresByIntervalDate(dates.getDate1(), dates.getDate2());
	}

	@GetMapping("/getAllUniqueUsers")
	public Response getAllUniqueUsers() {
		return service.getAllUniqueUsers();
	}

	@PostMapping("/getPorcentOfRegistries")
	public Response getPorcentOfRegistries(@RequestBody IntervalDate dates) {
		return service.getPorcentOfRegistries(dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/getAllRegistriesByIntervalDates")
	public Response getAllRegistriesByIntervalDates(@RequestBody IntervalDate dates) {
		return service.getAllRegistriesByIntervalDates(dates.getDate1(), dates.getDate2());
	}

	@GetMapping("/getAllRegistries")
	public Response getAllRegistries() {
		return service.getAllRegistries();
	}

	@GetMapping("/getAllUsers")
	public Response getAllUsers() {
		return service.getAllUsers();
	}

	@PostMapping("/getUserById")
	public Response getUserById(@RequestBody UsuarioApp user) {
		return service.getUserById(user.getUserAppId());
	}

	@PostMapping("/login")
	public Response login(@RequestBody UsuarioApp user) {
		return service.login(user);
	}

	@PostMapping("/insertUser")
	public Response insertUser(@RequestBody UsuarioApp user) {
		return service.insertUser(user);
	}

	@PostMapping("/updateUser")
	public Response updateUser(@RequestBody UsuarioApp user) {
		return service.updateUser(user);
	}

	@PostMapping("/sendEmailToken")
	public Response sendEmailToken(@RequestBody UsuarioApp user) {
		return service.sendEmailToken(user);
	}

	@PostMapping("/changePassUserPass")
	public Response changePassUserPass(@RequestBody UsuarioApp CambiosPass) {
		return service.changePassUserPass(CambiosPass);
	}

	@GetMapping("/getAllLogins")
	public Response getAllLogins() {
		return this.service.getAllLogins();
	}

	@PostMapping("/getLoginsByIntervalDate")
	public Response getLoginsByIntervalDate(@RequestBody IntervalDate dates) {
		return service.getLoginsByIntervalDate(dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/getPorcentOfLogins")
	public Response getPorcentOfLogins(@RequestBody IntervalDate dates) {
		return this.service.getPorcentOfLogins(dates.getDate1(), dates.getDate2());
	}

}
