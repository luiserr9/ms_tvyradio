package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name = "radio_vivo_ondemand")
public class Radiovivoondemand {
	
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	@Column(name = "id_radio",unique = true, nullable = false)
	private Long idradio; 
	
	@Column(name = "name")
	private String nombre;
	
	@JsonFormat(pattern="yyyy-MM-dd", timezone = "UTC")
	@Column(name = "date_create")
	private Date fechcrea;
	
	@Column(name = "frecuencia")
	private String frecuencia;
	
	@Column(name = "url")
	private String url;
	
	@Column(name = "description")
	private String descripcion;
	
	@Column(name = "img", nullable = false)
	private String img;
	@Column(name = "logo", nullable = false)
	private String logo;
	@Column(name = "cover", nullable = false)
	private String cover;
	
	@Column(name = "status")
	private Boolean status;

	public Radiovivoondemand() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdradio() {
		return idradio;
	}

	public void setIdradio(Long idradio) {
		this.idradio = idradio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechcrea() {
		return fechcrea;
	}

	public void setFechcrea(Date fechcrea) {
		this.fechcrea = fechcrea;
	}

	public String getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}
	
	
	
	
	
}
