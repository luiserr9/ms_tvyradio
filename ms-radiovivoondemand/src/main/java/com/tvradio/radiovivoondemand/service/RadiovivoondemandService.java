package com.tvradio.radiovivoondemand.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvradio.general.entities.Imgradioondemand;
import com.tvradio.general.entities.Radiovivoondemand;
import com.tvradio.general.utilities.Response;
import com.tvradio.radiovivoondemand.repository.ImgbtnRepository;
import com.tvradio.radiovivoondemand.repository.RadiovivoondemandRepository;

@Service
public class RadiovivoondemandService {

	@Autowired
	private RadiovivoondemandRepository radiovivoondemandRepository;
	
	@Autowired
	private ImgbtnRepository imgbtnRepository;

	public Response insertEstacion(Radiovivoondemand radiovivoondemand) {

		try {
			radiovivoondemandRepository.save(radiovivoondemand);
			return Response.success("estacion registrada con exito");
		} catch (Exception e) {
			return Response.error("Error al insertat estacion: " + e.getMessage());
		}

	}

	public Response getEstaciones() {

		return Response.success(radiovivoondemandRepository.findByStatus(true));

	}

	public Response getEstacionesbyid(Radiovivoondemand radiovivoondemand) {

		if (radiovivoondemandRepository.existsById(radiovivoondemand.getIdradio())) {
			return Response.success(radiovivoondemandRepository.findById(radiovivoondemand.getIdradio()));
		} else {
			return Response.error("Error no se encontro ningun registro");
		}
	}

	public Response deleteestacionesByid(Long id) {

		if (radiovivoondemandRepository.existsById(id)) {
			try {
				radiovivoondemandRepository.deleteEstacionbyid(id);
				return Response.success("ok");
			} catch (Exception e) {
				return Response.badRequest("Error al eliminar la estacion: " + e.getMessage());
			}
		} else {
			return Response.badRequest("Error");
		}

	}

	public Response updateestacion(Radiovivoondemand radiovivoondemand) {
		try {
			radiovivoondemandRepository.save(radiovivoondemand);
			return Response.success("estacion actualizada con exito");
		} catch (Exception e) {
			return Response.error("Error al actualizar estacion: " + e.getMessage());
		}
	}
	
	public Response getimgbtn() {
		
		return Response.success(imgbtnRepository.findById((long) 1));
		
	}
	
	public Response updateBtn(Imgradioondemand imgradioondemand) {
		try {
			imgbtnRepository.save(imgradioondemand);
			return Response.success("imagen actualizada");
		} catch (Exception e) {
			return Response.error("Error al actualizar imagen: " + e.getMessage());
		}
	}
	
	
	

}
