package com.tvradio.radiovivoondemand.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.Radiovivoondemand;


public interface RadiovivoondemandRepository extends CrudRepository<Radiovivoondemand, Long> {
	
	public List<Radiovivoondemand> findByStatus(Boolean status);

	@Modifying
	@Query(value = "UPDATE radio_vivo_ondemand SET status=0 WHERE idradio =:idradio ")
	@Transactional
	void deleteEstacionbyid( @Param("idradio") Long idradio );
	
}

