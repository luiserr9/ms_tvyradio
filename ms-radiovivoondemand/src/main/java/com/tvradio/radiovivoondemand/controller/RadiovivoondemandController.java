package com.tvradio.radiovivoondemand.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tvradio.general.entities.Imgradioondemand;
import com.tvradio.general.entities.Radiovivoondemand;

import com.tvradio.general.utilities.Response;
import com.tvradio.radiovivoondemand.service.RadiovivoondemandService;



@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/RadioEnVivoOndemand")
public class RadiovivoondemandController {
	
	@Autowired
	private RadiovivoondemandService radiovivoondemandService;
	
	
	@GetMapping("/getallseasons")
	private Response getallseasons() {
		return radiovivoondemandService.getEstaciones();
	}
	
	@GetMapping("/getImgbtn")
	private Response getImgbtn() {
		return radiovivoondemandService.getimgbtn();
	}
	
	@PostMapping("/insertseasons")
	private Response insertseasons(@RequestBody Radiovivoondemand radiovivoondemand) {
		return radiovivoondemandService.insertEstacion(radiovivoondemand);
	}
	
	
	@PostMapping("/getEstacionesbyid")
	private Response getEstacionesbyid (@RequestBody Radiovivoondemand radiovivoondemand) {
		return radiovivoondemandService.getEstacionesbyid(radiovivoondemand);
	}
	
	@PostMapping("/deleteEstacionesbyid")
	private Response deleteEstacionesbyid (@RequestBody Radiovivoondemand radiovivoondemand) {
		return radiovivoondemandService.deleteestacionesByid(radiovivoondemand.getIdradio());
	}
	
	
	@PostMapping("/updateestacion")
	private Response updateestacion (@RequestBody Radiovivoondemand radiovivoondemand) {
		return radiovivoondemandService.updateestacion(radiovivoondemand);
	}
	
	@PostMapping("/updateimgbtn")
	private Response updateimgbtn (@RequestBody Imgradioondemand imgradioondemand) {
		return radiovivoondemandService.updateBtn(imgradioondemand);
	}
	
}
