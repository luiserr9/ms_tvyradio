package com.tvradio.campania.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tvradio.campania.service.CampaniaService;
import com.tvradio.general.dto.IntervalDate;
import com.tvradio.general.entities.Banner;
import com.tvradio.general.entities.Campanias;
import com.tvradio.general.entities.UsuarioOnDemand;
import com.tvradio.general.entities.AffinitySegmentation;
import com.tvradio.general.utilities.Response;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("/campanias")
public class CampaniaController {

	@Autowired
	private CampaniaService campaniaService;

	@PostMapping("/analiticaPrint")
	public Response analiticaPrint(@RequestBody Campanias campa) {
		return this.campaniaService.analiticPrints(campa.getIdcampania());
	}

	@PostMapping("/addPrint")
	public Response addPrint(@RequestBody Banner banner) {
		return this.campaniaService.addPrint(banner.getIdbanner());
	}

	@PostMapping("/deleteCampaing")
	private Response deleteCampaing(@RequestBody Campanias camp) {
		return this.campaniaService.deleteCampaing(camp);
	}

	@GetMapping("/obtenercampanias")
	private Response obtenercampanias() {

		return campaniaService.getCampaings();

	}

	@PostMapping("/actualizarCampanias")
	private Response actualizarCampanias(@RequestBody Campanias campania) {
		return this.campaniaService.updateCampaing(campania);
	}

	@PostMapping("/obtenercampaniaid")
	private Response obtenercampaniaid(@RequestBody Campanias campanias) {
		return campaniaService.getCampaingbyid(campanias.getIdcampania());
	}

	@PostMapping("/obtenerbanners")
	private Response obtenerbanners(@RequestBody Campanias campanias) {
		return campaniaService.getBannerCampaing(campanias);

	}

	@GetMapping("/obtenerBannerSegmento/{segmeto}")
	private Response obtenerBannerSegmento(@PathVariable("segmeto") int segmeto) {
		return campaniaService.getBannerBySegmentation(segmeto);
	}

	@PostMapping("/insertarCampania")
	private Response insertarCampania(@RequestBody Campanias campanias) {
		return campaniaService.insertCampaing(campanias);
	}

	@PostMapping("/insertarBanner")
	private Response insertarBanner(@RequestBody Banner banner) {
		return campaniaService.insertBanner(banner);
	}

	@PostMapping("/getAllCampaingDateInterval")
	private Response getAllCampaingDateInterval(@RequestBody IntervalDate dates) {
		return campaniaService.getAllCampaingDateInterval(dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/getCampaingDateIntervalById/{idcampania}")
	private Response getCampaingDateIntervalById(@RequestBody IntervalDate dates,
			@PathVariable("idcampania") Long banner) {
		return campaniaService.getCampaingDateIntervalById(banner, dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/addClickBannerCampaing")
	public Response addClickBannerCampaing(@RequestBody Banner banner) {
		return this.campaniaService.addClickBannerCampaing(banner.getIdbanner());
	}

	@PostMapping("/analiticaClicks")
	public Response analiticaClicks(@RequestBody Campanias campa) {
		return this.campaniaService.analiticaClicks(campa.getIdcampania());
	}

	@PostMapping("/getClickDateIntervalById/{idcampania}")
	private Response getClickDateIntervalById(@RequestBody IntervalDate dates, @PathVariable("idcampania") Long campa) {
		return campaniaService.getClickDateIntervalById(campa, dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/getClickCampaingAllDay")
	public Response getClickCampaingAllDay(@RequestBody Campanias campa) {
		return this.campaniaService.getClickCampaingAllDay(campa.getIdcampania());
	}

	@PostMapping("/getClickCampaingDay/{idcampania}")
	private Response getClickCampaingDay(@RequestBody IntervalDate dates, @PathVariable("idcampania") Long banner) {
		return campaniaService.getClickCampaingDay(banner, dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/getPrintsCampaingAllDay")
	public Response getPrintsCampaingAllDay(@RequestBody Campanias campa) {
		return this.campaniaService.getPrintsCampaingAllDay(campa.getIdcampania());
	}

	@PostMapping("/getPrintsCampaingDay/{idcampania}")
	private Response getPrintsCampaingDay(@RequestBody IntervalDate dates, @PathVariable("idcampania") Long banner) {
		return campaniaService.getPrintsCampaingDay(banner, dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/obtenerbannersporusuario")
	private Response actualizarCampanias(@RequestBody UsuarioOnDemand user) {
		return this.campaniaService.getbannerofuser(user);
	}
	
	@PostMapping("/addAffinitySegmentation")
	public Response addAffinitySegmentation(@RequestBody AffinitySegmentation banner) {
		return this.campaniaService.addAffinitySegmentation(banner);
	}
	
	@PostMapping("/getAffinitySegmentationAll")
	private Response getAffinitySegmentationAll(@RequestBody Campanias campa) {
		return campaniaService.getAffinitySegmentationAll(campa.getIdcampania());
	}
	@PostMapping("/actualizarbanners")
	private Response actualizarbanners(@RequestBody Banner banner) {
	return this.campaniaService.updatebanner(banner);
	}
	@PostMapping("/getAffinitySegmentationAllByInterval/{idcampania}")
	private Response getAffinitySegmentationAllByInterval(@RequestBody IntervalDate dates, @PathVariable("idcampania") Long banner) {
		return campaniaService.getAffinitySegmentationAllByInterval(banner, dates.getDate1(), dates.getDate2());
	}
}
