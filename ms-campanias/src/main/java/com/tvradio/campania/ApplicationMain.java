package com.tvradio.campania;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages= {"com.tvradio.campania.repository"})
@ComponentScan(basePackages = {"com.tvradio.general","com.tvradio.campania"})
@EntityScan(basePackages = {"com.tvradio.general.entities", "com.tvradio.general.entities.utilities"})
public class ApplicationMain {
	public static void main(String[] args) {
		SpringApplication.run(ApplicationMain.class, args);
	}
}
