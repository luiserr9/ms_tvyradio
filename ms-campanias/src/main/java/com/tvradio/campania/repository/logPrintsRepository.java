package com.tvradio.campania.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.Campanias;
import com.tvradio.general.entities.logPrints;

public interface logPrintsRepository extends CrudRepository<logPrints,Long>{
	
 public logPrints findByIdBanner(Long idBanner);
 public List<logPrints> findAll();
 
 	@Query(value="SELECT COUNT(c) FROM logPrints c WHERE c.idBanner=:banner AND c.date BETWEEN :date1 AND :date2")
 	public Long getAllCampaingDateById(@Param("banner") Long banner, @Param("date1") Date date1, @Param("date2") Date date2);
 	
 	@Query(value="SELECT DATE_FORMAT(c.date,'%W') FROM logPrints c WHERE c.idBanner=:banner")
 	public List<String> getPrintsCampaingAllDay(@Param("banner") Long campa);
 	
 	@Query(value="SELECT DATE_FORMAT(date,'%W')FROM logPrints c WHERE c.idBanner=:banner AND c.date BETWEEN :date1 AND :date2")
 	public List<String> getPrintsCampaingDay(@Param("banner") Long campa, @Param("date1") Date date1, @Param("date2") Date date2);
}
