package com.tvradio.campania.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.LogViewCategory;

@Transactional
public interface LogViewsCategoryRepository extends CrudRepository<LogViewCategory, Long> {

	@Query(value = "SELECT  COUNT(l)  FROM log_views_category l WHERE l.category=:category ")
	public Double getViewsByCategory(@Param("category") Categoria category);

	@Query(value = "SELECT  COUNT(l)  FROM log_views_category l WHERE l.category=:category AND l.dateOfCreation BETWEEN :date1 AND :date2 ")
	public Double getViewsByCategoryByInterval(@Param("category") Categoria category, @Param("date1") Date date1,
			@Param("date2") Date date2);
	
	@Query(value = "SELECT l.category FROM log_views_category l WHERE l.user=:userid GROUP BY l.category HAVING COUNT(l.category)>1", nativeQuery = true)
	List<Long> getcategoriaid(@Param("userid") Long userid );
}