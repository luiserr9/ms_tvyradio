package com.tvradio.campania.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.Campanias;


public interface CampaniaRepository extends CrudRepository<Campanias, Long> {
	
	public List<Campanias> findByStatus(Boolean status);
	
	
	public  Campanias findByNameCampanig(String nameCampanig);
	public  Campanias findByNameCampanigAndStatus(String nameCampanig,Integer status);
		
	@Query(value="SELECT c FROM publicity_campaing c WHERE c.fechcreate BETWEEN :date1 AND :date2 ")
	List<Campanias> getAllCampaingDate(@Param("date1") Date date1, @Param("date2") Date date2);
	
	@Query(value="SELECT n.* FROM publicity_campaing n ORDER BY n.id_campaign DESC LIMIT 1 ", nativeQuery = true)
	public Campanias getbylast();

	
}
