package com.tvradio.campania.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.tvradio.general.entities.Banner;

public interface BannerReposiory extends CrudRepository<Banner, Long> {
	
	
	public List<Banner> findByIdcampania(Long idcampania);
	public List<Banner> findBySnoticieros(Boolean snoticieros);
	public List<Banner> findBySopinion(Boolean sopinion);
	public List<Banner> findBySidentidad(Boolean sidentidad);
	public List<Banner> findBySinfantil(Boolean sinfantil);
	public List<Banner> findBySjovenes(Boolean sjovenes);
	public List<Banner> findBySespeciales(Boolean sespeciales);
	public List<Banner> findBySentretenimiento(Boolean sentretenimiento);
	public List<Banner> findBySdeportes(Boolean sdeportes);
	public List<Banner> findBySinstitucionales(Boolean sinstitucionales);
	public List<Banner> findBySmujeres(Boolean smujeres);
	public List<Banner> findByScapsulas(Boolean scapsulas);
	public List<Banner>  findAllByIdcampania(Long idcampania);
	


}
