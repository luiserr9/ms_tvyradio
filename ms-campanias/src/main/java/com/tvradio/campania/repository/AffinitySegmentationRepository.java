package com.tvradio.campania.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.AffinitySegmentation;
import com.tvradio.general.entities.Categoria;

public interface AffinitySegmentationRepository extends CrudRepository<AffinitySegmentation,Long>{

	
	public List<AffinitySegmentation> findAll();
	
	@Query(value = "SELECT l FROM AffinitySegmentation l WHERE l.idBanner=:banner ")
	public List<String> getAffinitySegmentationAllById(@Param("banner") Long campa);
	
	@Query(value = "SELECT l FROM AffinitySegmentation l WHERE l.idBanner=:banner AND l.date BETWEEN :date1 AND :date2")
	public List<AffinitySegmentation> getAffinitySegmentationAllByInterval(@Param("banner") Long campa, @Param("date1") Date date1, @Param("date2") Date date2);
}
