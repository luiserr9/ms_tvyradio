package com.tvradio.campania.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.tvradio.general.entities.ClickBannerCampaing;



public interface ClickBannerCampaingRepository extends CrudRepository<ClickBannerCampaing,Long>{
	

	public List<ClickBannerCampaing> findAll();
 
 	@Query(value="SELECT COUNT(c) FROM click_banner_campaing c WHERE c.idBanner=:banner AND c.date BETWEEN :date1 AND :date2")
 	public Long getClickCampaingDateById(@Param("banner") Long campa, @Param("date1") Date date1, @Param("date2") Date date2);
 	
 	public ClickBannerCampaing findByIdBanner(Long idBanner);
 	
 	@Query(value="SELECT DATE_FORMAT(date,'%W')FROM click_banner_campaing c WHERE c.idBanner=:banner AND c.date BETWEEN :date1 AND :date2")
 	public List<String> getClickCampaingDay(@Param("banner") Long campa, @Param("date1") Date date1, @Param("date2") Date date2);
 	
 	@Query(value="SELECT DATE_FORMAT(c.date,'%W') FROM click_banner_campaing c WHERE c.idBanner=:banner")
 	public List<String> getClickCampaingAllDay(@Param("banner") Long campa);
}
