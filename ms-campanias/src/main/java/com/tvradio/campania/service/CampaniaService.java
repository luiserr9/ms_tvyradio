package com.tvradio.campania.service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sun.xml.fastinfoset.util.StringArray;
import com.tvradio.campania.repository.AffinitySegmentationRepository;
import com.tvradio.campania.repository.BannerReposiory;
import com.tvradio.campania.repository.CampaniaRepository;
import com.tvradio.campania.repository.CategoriaRepository;
import com.tvradio.campania.repository.ClickBannerCampaingRepository;
import com.tvradio.campania.repository.LogViewsCategoryRepository;
import com.tvradio.campania.repository.logPrintsRepository;
import com.tvradio.general.dto.BannerResp;
import com.tvradio.general.dto.SegmentacionContador;
import com.tvradio.general.entities.AffinitySegmentation;
import com.tvradio.general.entities.Banner;
import com.tvradio.general.entities.Campanias;
import com.tvradio.general.entities.ClickBannerCampaing;
import com.tvradio.general.entities.UsuarioOnDemand;
import com.tvradio.general.entities.logPrints;
import com.tvradio.general.utilities.Response;

@Service
public class CampaniaService {

	@Autowired
	private CampaniaRepository campaniaRepository;

	@Autowired
	private BannerReposiory bannerReposiory;

	@Autowired
	private logPrintsRepository log;

	@Autowired
	private ClickBannerCampaingRepository click;

	@Autowired
	private LogViewsCategoryRepository logViewsCategoryRepository;

	@Autowired
	private CategoriaRepository categoriaRepository;

	@Autowired
	private AffinitySegmentationRepository segmentacion;

	public Response analiticPrints(Long campa) {
		try {
			// List<logPrints> ss=(List<logPrints>) this.log.findAll();
			Integer cont = 0;
			List<logPrints> ss = this.log.findAll();

			for (int i = 0; i < ss.size(); i++) {

				if (this.bannerReposiory.existsById(ss.get(i).getIdBanner()) == true) {

					Banner aux = this.bannerReposiory.findById(ss.get(i).getIdBanner()).get();

					if (aux.getIdcampania() == campa) {
						cont++;
					}
				}
			}

			return Response.created(cont);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}

	}

	public Response addPrint(Long idBanner) {
		try {
			this.log.save(new logPrints(idBanner, new Date()));
			return Response.created("OK");
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response deleteCampaing(Campanias camp) {
		try {
			if (this.campaniaRepository.findById(camp.getIdcampania()) != null) {
				// borrando todos los banners de campania
				List<Banner> ss = this.bannerReposiory.findAllByIdcampania(camp.getIdcampania());
				for (int i = 0; i < ss.size(); i++) {
					this.bannerReposiory.delete(ss.get(i));
				}
				// elimina la campania
				this.campaniaRepository.delete(this.campaniaRepository.findById(camp.getIdcampania()).get());
				return Response.success("Eliminado");
			} else {
				return Response.notFound("No se encuentra");
			}
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getCampaings() {

		return Response.success(campaniaRepository.findAll());
	}

	public Response getBannerCampaing(Campanias campanias) {

		return Response.success(bannerReposiory.findByIdcampania(campanias.getIdcampania()));

	}

	public Response getCampaingbyid(Long id) {

		if (campaniaRepository.existsById(id)) {
			return Response.success(campaniaRepository.findById(id));
		} else {
			return Response.badRequest("no se encontro campaña");
		}

	}

	public Response getBannerBySegmentation(int segmento) {

		switch (segmento) {
		case 1:
			return Response.success(bannerReposiory.findBySnoticieros(true));
		case 2:
			return Response.success(bannerReposiory.findBySopinion(true));
		case 3:
			return Response.success(bannerReposiory.findBySidentidad(true));
		case 4:
			return Response.success(bannerReposiory.findBySinfantil(true));
		case 5:
			return Response.success(bannerReposiory.findBySjovenes(true));
		case 6:
			return Response.success(bannerReposiory.findBySespeciales(true));
		case 7:
			return Response.success(bannerReposiory.findBySentretenimiento(true));
		case 8:
			return Response.success(bannerReposiory.findBySdeportes(true));
		case 9:
			return Response.success(bannerReposiory.findBySinstitucionales(true));
		case 10:
			return Response.success(bannerReposiory.findBySmujeres(true));
		case 11:
			return Response.success(bannerReposiory.findByScapsulas(true));
		default:
			return Response.notFound("El segmento no se encontro");

		}
	}

	public Response insertCampaing(Campanias campanias) {
		try {
			if (this.campaniaRepository.findByNameCampanigAndStatus(campanias.getNameCampanig(), 1) == null) {
				if (this.campaniaRepository.findByNameCampanig(campanias.getNameCampanig()) == null) {
					campanias.setStatus(1);
					campanias.setFechcreate(new Date());
					campaniaRepository.save(campanias);
					return Response.created(campaniaRepository.getbylast().getIdcampania());
				} else {
					campanias.setStatus(1);
					campaniaRepository.save(campanias);
					return Response.accepted("Se inserto la campaña correctamente");
				}
			} else {
				return Response.badRequest("Ya existe");
			}
		} catch (Exception e) {
			return Response.error("Error an insertar la campania");
		}

	}

	public Response updateCampaing(Campanias campanas) {
		try {
			if (this.campaniaRepository.existsById(campanas.getIdcampania()) == true) {
				Campanias ss = this.campaniaRepository.findById(campanas.getIdcampania()).get();
				if (campanas.getFechcreate() == null) {
					campanas.setFechcreate(ss.getFechcreate());
				}
				if (campanas.getFechfin() == null) {
					campanas.setFechfin(ss.getFechfin());
				}

				if (campanas.getFechini() == null) {
					campanas.setFechini(ss.getFechini());
				}

				if (campanas.getNameCampanig() == null) {
					campanas.setNameCampanig(ss.getNameCampanig());
				}
				if (campanas.getStatus() == null) {
					campanas.setStatus(ss.getStatus());
				}

				if (campanas.getInversion() == null) {
					campanas.setInversion(ss.getInversion());
				}
				this.campaniaRepository.save(campanas);
				return Response.ok("OK");
			} else {
				return Response.notFound("No existe");
			}

		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response insertBanner(Banner banner) {
		try {
			bannerReposiory.save(banner);
			return Response.success("ok");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

	public Response getAllCampaingDateInterval(Date date1, Date date2) {
		try {
			return Response.ok(this.campaniaRepository.getAllCampaingDate(date1, date2));
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}

	}

	public Response getCampaingDateIntervalById(Long campa, Date date1, Date date2) {
		try {
			// List<logPrints> ss=(List<logPrints>) this.log.findAll();
			Integer cont = 0;

			List<Banner> ss = this.bannerReposiory.findByIdcampania(campa);
			for (int i = 0; i < ss.size(); i++) {
				long idbaner = ss.get(i).getIdbanner();

				cont = (int) (cont + this.log.getAllCampaingDateById(idbaner, date1, date2));
			}

			return Response.created(cont);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response addClickBannerCampaing(Long idBanner) {
		try {
			this.click.save(new ClickBannerCampaing(idBanner, new Date()));
			return Response.created("OK");
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response analiticaClicks(Long campa) {
		try {
			// List<logPrints> ss=(List<logPrints>) this.log.findAll();
			Integer cont = 0;
			List<ClickBannerCampaing> ss = this.click.findAll();

			for (int i = 0; i < ss.size(); i++) {

				if (this.bannerReposiory.existsById(ss.get(i).getIdBanner()) == true) {

					Banner aux = this.bannerReposiory.findById(ss.get(i).getIdBanner()).get();
					System.out.println(aux);

					if (aux.getIdcampania() == campa) {

						cont++;

					}
				}
			}

			return Response.created(cont);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}

	}



	public Response getClickDateIntervalById(Long campa, Date date1, Date date2) {
		try {
			// List<logPrints> ss=(List<logPrints>) this.log.findAll();
			Integer cont = 0;

			List<Banner> ss = this.bannerReposiory.findByIdcampania(campa);
			for (int i = 0; i < ss.size(); i++) {
				long idbaner = ss.get(i).getIdbanner();

				cont = (int) (cont + this.click.getClickCampaingDateById(ss.get(i).getIdbanner(), date1, date2));
			}

			return Response.created(cont);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getClickCampaingAllDay(Long campa) {

		try {
			// List<logPrints> ss=(List<logPrints>) this.log.findAll();

			Integer lunes = 0;
			Integer martes = 0;
			Integer miercoles = 0;
			Integer jueves = 0;
			Integer viernes = 0;
			Integer sabado = 0;
			Integer domingo = 0;

			List<Banner> arraybanners = bannerReposiory.findAllByIdcampania(campa);

			for (int i = 0; i < arraybanners.size(); i++) {

				// System.out.println(arraybanners.get(i).getIdbanner());
				List<String> date = this.click.getClickCampaingAllDay(arraybanners.get(i).getIdbanner());
				// System.out.println(date.get(i));

				for (int j = 0; j < date.size(); j++) {
					if (date.get(j).equals("Monday")) {
						lunes++;
					} else if (date.get(j).equals("Tuesday")) {
						martes++;
					} else if (date.get(j).equals("Wednesday")) {
						miercoles++;
					} else if (date.get(j).equals("Thursday")) {
						jueves++;

					} else if (date.get(j).equals("Friday")) {
						viernes++;
					} else if (date.get(j).equals("Saturday")) {
						sabado++;
					} else if (date.get(j).equals("Sunday")) {
						domingo++;
					}
				}

			}
			ArrayList<Integer> ContSemana = new ArrayList<Integer>();
			ContSemana.add(domingo);
			ContSemana.add(lunes);
			ContSemana.add(martes);
			ContSemana.add(miercoles);
			ContSemana.add(jueves);
			ContSemana.add(viernes);
			ContSemana.add(sabado);
			return Response.created(ContSemana);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}

	}

	public Response getClickCampaingDay(Long campa, Date date1, Date date2) {

		try {
			// List<logPrints> ss=(List<logPrints>) this.log.findAll();

			Integer lunes = 0;
			Integer martes = 0;
			Integer miercoles = 0;
			Integer jueves = 0;
			Integer viernes = 0;
			Integer sabado = 0;
			Integer domingo = 0;

			List<Banner> arraybanners = bannerReposiory.findAllByIdcampania(campa);

			for (int i = 0; i < arraybanners.size(); i++) {

				// System.out.println(arraybanners.get(i).getIdbanner());
				List<String> date = this.click.getClickCampaingDay(arraybanners.get(i).getIdbanner(), date1, date2);
				for (int j = 0; j < date.size(); j++) {
					if (date.get(j).equals("Monday")) {
						lunes++;
					} else if (date.get(j).equals("Tuesday")) {
						martes++;
					} else if (date.get(j).equals("Wednesday")) {
						miercoles++;
					} else if (date.get(j).equals("Thursday")) {
						jueves++;

					} else if (date.get(j).equals("Friday")) {
						viernes++;
					} else if (date.get(j).equals("Saturday")) {
						sabado++;
					} else if (date.get(j).equals("Sunday")) {
						domingo++;
					}
				}

			}
			ArrayList<Integer> ContSemana = new ArrayList<Integer>();
			ContSemana.add(domingo);
			ContSemana.add(lunes);
			ContSemana.add(martes);
			ContSemana.add(miercoles);
			ContSemana.add(jueves);
			ContSemana.add(viernes);
			ContSemana.add(sabado);
			return Response.created(ContSemana);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}

	}

	public Response getPrintsCampaingAllDay(Long campa) {

		try {
			// List<logPrints> ss=(List<logPrints>) this.log.findAll();

			Integer lunes = 0;
			Integer martes = 0;
			Integer miercoles = 0;
			Integer jueves = 0;
			Integer viernes = 0;
			Integer sabado = 0;
			Integer domingo = 0;

			List<Banner> arraybanners = bannerReposiory.findAllByIdcampania(campa);

			for (int i = 0; i < arraybanners.size(); i++) {

				// System.out.println(arraybanners.get(i).getIdbanner());
				List<String> date = this.log.getPrintsCampaingAllDay(arraybanners.get(i).getIdbanner());
				// System.out.println(date.get(i));

				for (int j = 0; j < date.size(); j++) {
					if (date.get(j).equals("Monday")) {
						lunes++;
					} else if (date.get(j).equals("Tuesday")) {
						martes++;
					} else if (date.get(j).equals("Wednesday")) {
						miercoles++;
					} else if (date.get(j).equals("Thursday")) {
						jueves++;

					} else if (date.get(j).equals("Friday")) {
						viernes++;
					} else if (date.get(j).equals("Saturday")) {
						sabado++;
					} else if (date.get(j).equals("Sunday")) {
						domingo++;
					}
				}

			}
			ArrayList<Integer> ContSemana = new ArrayList<Integer>();
			ContSemana.add(domingo);
			ContSemana.add(lunes);
			ContSemana.add(martes);
			ContSemana.add(miercoles);
			ContSemana.add(jueves);
			ContSemana.add(viernes);
			ContSemana.add(sabado);
			return Response.created(ContSemana);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}

	}

	public Response getPrintsCampaingDay(Long campa, Date date1, Date date2) {

		try {
			// List<logPrints> ss=(List<logPrints>) this.log.findAll();

			Integer lunes = 0;
			Integer martes = 0;
			Integer miercoles = 0;
			Integer jueves = 0;
			Integer viernes = 0;
			Integer sabado = 0;
			Integer domingo = 0;

			List<Banner> arraybanners = bannerReposiory.findAllByIdcampania(campa);

			for (int i = 0; i < arraybanners.size(); i++) {

				// System.out.println(arraybanners.get(i).getIdbanner());
				List<String> date = this.log.getPrintsCampaingDay(arraybanners.get(i).getIdbanner(), date1, date2);
				for (int j = 0; j < date.size(); j++) {
					if (date.get(j).equals("Monday")) {
						lunes++;
					} else if (date.get(j).equals("Tuesday")) {
						martes++;
					} else if (date.get(j).equals("Wednesday")) {
						miercoles++;
					} else if (date.get(j).equals("Thursday")) {
						jueves++;

					} else if (date.get(j).equals("Friday")) {
						viernes++;
					} else if (date.get(j).equals("Saturday")) {
						sabado++;
					} else if (date.get(j).equals("Sunday")) {
						domingo++;
					}
				}

			}
			ArrayList<Integer> ContSemana = new ArrayList<Integer>();
			ContSemana.add(domingo);
			ContSemana.add(lunes);
			ContSemana.add(martes);
			ContSemana.add(miercoles);
			ContSemana.add(jueves);
			ContSemana.add(viernes);
			ContSemana.add(sabado);
			return Response.created(ContSemana);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}

	}

	public Response getbannerofuser(UsuarioOnDemand user) {

		List<Long> ss = logViewsCategoryRepository.getcategoriaid(user.getUserOnDemandId());

		ArrayList banner = new ArrayList();

		for (int i = 0; i < ss.size(); i++) {

			String namecat = categoriaRepository.findById(ss.get(i)).get().getName();

			if (namecat.equals("Noticieros")) {

				List<Banner> getbanersegmetacion = bannerReposiory.findBySnoticieros(true);
				for (int j = 0; j < getbanersegmetacion.size(); j++) {

					banner.add(getbanersegmetacion.get(j));
				}
			}
			if (namecat.equals("Opinion")) {
				List<Banner> getbanersegmetacion = bannerReposiory.findBySopinion(true);
				for (int j = 0; j < getbanersegmetacion.size(); j++) {

					banner.add(getbanersegmetacion.get(j));
				}
			}
			if (namecat.equals("Identidad")) {
				List<Banner> getbanersegmetacion = bannerReposiory.findBySidentidad(true);
				for (int j = 0; j < getbanersegmetacion.size(); j++) {

					banner.add(getbanersegmetacion.get(j));
				}
			}
			if (namecat.equals("Infantil")) {

				List<Banner> getbanersegmetacion = bannerReposiory.findBySinfantil(true);
				for (int j = 0; j < getbanersegmetacion.size(); j++) {

					banner.add(getbanersegmetacion.get(j));
				}
			}
			if (namecat.equals("Jovenes")) {
				List<Banner> getbanersegmetacion = bannerReposiory.findBySjovenes(true);
				for (int j = 0; j < getbanersegmetacion.size(); j++) {

					banner.add(getbanersegmetacion.get(j));
				}
			}
			if (namecat.equals("Especiales")) {
				List<Banner> getbanersegmetacion = bannerReposiory.findBySespeciales(true);
				for (int j = 0; j < getbanersegmetacion.size(); j++) {

					banner.add(getbanersegmetacion.get(j));
				}
			}
			if (namecat.equals("Etretenimiento")) {
				List<Banner> getbanersegmetacion = bannerReposiory.findBySentretenimiento(true);
				for (int j = 0; j < getbanersegmetacion.size(); j++) {

					banner.add(getbanersegmetacion.get(j));
				}
			}
			if (namecat.equals("Deportes")) {
				List<Banner> getbanersegmetacion = bannerReposiory.findBySdeportes(true);
				for (int j = 0; j < getbanersegmetacion.size(); j++) {

					banner.add(getbanersegmetacion.get(j));
				}
			}
			if (namecat.equals("Institucionales")) {
				List<Banner> getbanersegmetacion = bannerReposiory.findBySinstitucionales(true);
				for (int j = 0; j < getbanersegmetacion.size(); j++) {

					banner.add(getbanersegmetacion.get(j));
				}
			}
			if (namecat.equals("Mujeres")) {
				List<Banner> getbanersegmetacion = bannerReposiory.findBySmujeres(true);
				for (int j = 0; j < getbanersegmetacion.size(); j++) {

					banner.add(getbanersegmetacion.get(j));
				}
			}

			if (namecat.equals("Capsulas")) {
				List<Banner> getbanersegmetacion = bannerReposiory.findByScapsulas(true);
				for (int j = 0; j < getbanersegmetacion.size(); j++) {

					banner.add(getbanersegmetacion.get(j));
				}
			}

		}
		return Response.success(banner);
	}

	public Response updatebanner(Banner banner) {

		try {

			bannerReposiory.save(banner);
			return Response.accepted("ok");
		} catch (Exception e) {
			return Response.badRequest("error: " + e.getMessage());
		}

	}

	public Response addAffinitySegmentation(AffinitySegmentation banner) {
		try {

			banner.setDate(new Date());
			this.segmentacion.save(banner);

			return Response.created("ok");
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}
	public Response getAffinitySegmentationAll(Long campa) {
		try {
			// List<logPrints> ss=(List<logPrints>) this.log.findAll();
			Long Noticieros = (long) 0;
			Long Opinion = (long) 0;
			Long Identidad = (long) 0;
			Long Infantil = (long) 0;
			Long Jovenes = (long) 0;
			Long Especiales = (long) 0;
			Long Entretenimiento = (long) 0;
			Long Deportes = (long) 0;
			Long Institucionales = (long) 0;
			Long Mujeres = (long) 0;
			Long Capsulas = (long) 0;
			Long sexmas = (long) 0;
			Long sexfem = (long) 0;

			ArrayList<SegmentacionContador> segmentacionConta = new ArrayList();
			/// List<ClickBannerCampaing> ss = this.click.findAll();
			List<AffinitySegmentation> seg = this.segmentacion.findAll();
			for (int i = 0; i < seg.size(); i++) {
				// System.out.println(seg.get(i).getIdBanner());
				if (this.bannerReposiory.existsById(seg.get(i).getIdBanner()) == true) {

					Banner aux = this.bannerReposiory.findById(seg.get(i).getIdBanner()).get();
					String segmentacion = seg.get(i).getSegmentacion();
					String segmentacioSexo = seg.get(i).getSexo();
					if (aux.getIdcampania() == campa) {
						if (segmentacion.equals("Noticieros")) {
							Noticieros++;
						} else if (segmentacion.equals("Opinion")) {
							Opinion++;
						} else if (segmentacion.equals("Identidad")) {
							Identidad++;
						} else if (segmentacion.equals("Infantil")) {
							Infantil++;
						} else if (segmentacion.equals("Jovenes")) {
							Jovenes++;
						} else if (segmentacion.equals("Especiales")) {
							Especiales++;
						} else if (segmentacion.equals("Entretenimiento")) {
							Entretenimiento++;
						} else if (segmentacion.equals("Deportes")) {
							Deportes++;
						} else if (segmentacion.equals("Institucionales")) {
							Institucionales++;
						} else if (segmentacion.equals("Mujeres")) {
							Mujeres++;
						} else if (segmentacion.equals("Capsulas")) {
							Capsulas++;
						}
						if (segmentacioSexo.equals("Femenino")) {
							sexfem++;
						} else if (segmentacioSexo.equals("Masculino")) {
							sexmas++;
						}

					}

				}

			}
			SegmentacionContador SegmentoCont = new SegmentacionContador(Noticieros, Opinion, Identidad, Infantil,
					Jovenes, Especiales, Entretenimiento, Deportes, Institucionales, Mujeres, Capsulas, sexmas, sexfem);
			segmentacionConta.add(SegmentoCont);

			return Response.created(segmentacionConta);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}
	public Response getAffinitySegmentationAllByInterval(Long campa, Date date1, Date date2) {
		try {
			// List<logPrints> ss=(List<logPrints>) this.log.findAll();
			Long Noticieros = (long) 0;
			Long Opinion = (long) 0;
			Long Identidad = (long) 0;
			Long Infantil = (long) 0;
			Long Jovenes = (long) 0;
			Long Especiales = (long) 0;
			Long Entretenimiento = (long) 0;
			Long Deportes = (long) 0;
			Long Institucionales = (long) 0;
			Long Mujeres = (long) 0;
			Long Capsulas = (long) 0;
			Long sexmas = (long) 0;
			Long sexfem = (long) 0;

			ArrayList<SegmentacionContador> segmentacionConta = new ArrayList();
			/// List<ClickBannerCampaing> ss = this.click.findAll();
			List<Banner> arraybanners = bannerReposiory.findAllByIdcampania(campa);

			for (int j = 0; j < arraybanners.size(); j++) {
			List<AffinitySegmentation> seg = this.segmentacion.getAffinitySegmentationAllByInterval(arraybanners.get(j).getIdbanner(), date1, date2);
			//System.out.println(seg.get(0));
			for (int i = 0; i < seg.size(); i++) {
				 System.out.println(seg.get(i).getIdBanner());
				if (this.bannerReposiory.existsById(seg.get(i).getIdBanner()) == true) {

					Banner aux = this.bannerReposiory.findById(seg.get(i).getIdBanner()).get();
					String segmentacion = seg.get(i).getSegmentacion();
					String segmentacioSexo = seg.get(i).getSexo();
					if (aux.getIdcampania() == campa) {
						if (segmentacion.equals("Noticieros")) {
							Noticieros++;
						} else if (segmentacion.equals("Opinion")) {
							Opinion++;
						} else if (segmentacion.equals("Identidad")) {
							Identidad++;
						} else if (segmentacion.equals("Infantil")) {
							Infantil++;
						} else if (segmentacion.equals("Jovenes")) {
							Jovenes++;
						} else if (segmentacion.equals("Especiales")) {
							Especiales++;
						} else if (segmentacion.equals("Entretenimiento")) {
							Entretenimiento++;
						} else if (segmentacion.equals("Deportes")) {
							Deportes++;
						} else if (segmentacion.equals("Institucionales")) {
							Institucionales++;
						} else if (segmentacion.equals("Mujeres")) {
							Mujeres++;
						} else if (segmentacion.equals("Capsulas")) {
							Capsulas++;
						}
						if (segmentacioSexo.equals("Femenino")) {
							sexfem++;
						} else if (segmentacioSexo.equals("Masculino")) {
							sexmas++;
						}

					}

				}

			}
			}
			SegmentacionContador SegmentoCont = new SegmentacionContador(Noticieros, Opinion, Identidad, Infantil,
					Jovenes, Especiales, Entretenimiento, Deportes, Institucionales, Mujeres, Capsulas, sexmas, sexfem);
			segmentacionConta.add(SegmentoCont);

			return Response.created(segmentacionConta);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

}
