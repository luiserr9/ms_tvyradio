package com.tvradio.general.dto;

public class BannerResp {

	private Long idbanner;
	private String namead;
	private String rutabanner;
	private String sex;
	private String segmentoinvocacion;

	public BannerResp() {
		super();

	}

	public BannerResp(Long idbanner, String namead, String rutabanner, String sex, String segmentoinvocacion) {
		super();
		this.idbanner = idbanner;
		this.namead = namead;
		this.rutabanner = rutabanner;
		this.sex = sex;
		this.segmentoinvocacion = segmentoinvocacion;

	}

	public Long getIdbanner() {
		return idbanner;
	}

	public void setIdbanner(Long idbanner) {
		this.idbanner = idbanner;
	}

	public String getNamead() {
		return namead;
	}

	public void setNamead(String namead) {
		this.namead = namead;
	}

	public String getRutabanner() {
		return rutabanner;
	}

	public void setRutabanner(String rutabanner) {
		this.rutabanner = rutabanner;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSegmentoinvocacion() {
		return segmentoinvocacion;
	}

	public void setSegmentoinvocacion(String segmentoinvocacion) {
		this.segmentoinvocacion = segmentoinvocacion;
	}
}