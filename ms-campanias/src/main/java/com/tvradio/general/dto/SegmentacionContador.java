package com.tvradio.general.dto;


public class SegmentacionContador {
	
	private Long Noticieros;
	private Long Opinion;
	private Long Identidad;
	private Long Infantil;
	private Long Jovenes;
	private Long Especiales;
	private Long Entretenimiento;
	private Long Deportes;
	private Long Institucionales;
	private Long Mujeres;
	private Long Capsulas;
	private Long sexmas;
	private Long sexfem;
	
	public SegmentacionContador() {
		super();

	}
	public SegmentacionContador( Long Noticieros, Long Opinion, Long Identidad, Long Infantil, Long Jovenes, Long Especiales, Long Entretenimiento, Long Deportes, Long Institucionales, Long Mujeres, Long Capsulas,
		Long sexmas,Long sexfem) {
		super();
		this.Noticieros=Noticieros;
		this.Opinion=Opinion;
		this.Identidad=Identidad;
		this.Infantil=Infantil;
		this.Jovenes=Jovenes;
		this.Especiales=Especiales;
		this.Entretenimiento=Entretenimiento;
		this.Deportes=Deportes;
		this.Institucionales=Institucionales;
		this.Mujeres=Mujeres;
		this.Capsulas=Capsulas;
		this.sexmas = sexmas;
		this.sexfem =sexfem;
	}
	
	
	public Long getNoticieros() {
		return Noticieros;
	}
	public void setNoticieros(Long noticieros) {
		Noticieros = noticieros;
	}
	public Long getOpinion() {
		return Opinion;
	}
	public void setOpinion(Long opinion) {
		Opinion = opinion;
	}
	public Long getIdentidad() {
		return Identidad;
	}
	public void setIdentidad(Long identidad) {
		Identidad = identidad;
	}
	public Long getInfantil() {
		return Infantil;
	}
	public void setInfantil(Long infantil) {
		Infantil = infantil;
	}
	public Long getJovenes() {
		return Jovenes;
	}
	public void setJovenes(Long jovenes) {
		Jovenes = jovenes;
	}
	public Long getEspeciales() {
		return Especiales;
	}
	public void setEspeciales(Long especiales) {
		Especiales = especiales;
	}
	public Long getDeportes() {
		return Deportes;
	}
	public void setDeportes(Long deportes) {
		Deportes = deportes;
	}
	public Long getInstitucionales() {
		return Institucionales;
	}
	public void setInstitucionales(Long institucionales) {
		Institucionales = institucionales;
	}
	public Long getMujeres() {
		return Mujeres;
	}
	public void setMujeres(Long mujeres) {
		Mujeres = mujeres;
	}
	public Long getCapsulas() {
		return Capsulas;
	}
	public void setCapsulas(Long capsulas) {
		Capsulas = capsulas;
	}
	public Long getEntretenimiento() {
		return Entretenimiento;
	}
	public void setEntretenimiento(Long entretenimiento) {
		Entretenimiento = entretenimiento;
	}
	public Long getSexmas() {
		return sexmas;
	}
	public void setSexmas(Long sexmas) {
		this.sexmas = sexmas;
	}
	public Long getSexfem() {
		return sexfem;
	}
	public void setSexfem(Long sexfem) {
		this.sexfem = sexfem;
	}
	

}
