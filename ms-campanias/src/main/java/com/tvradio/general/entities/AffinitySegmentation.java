package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name = "AffinitySegmentation")
public class AffinitySegmentation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@Column(name = "banner", nullable = false)
	private Long idBanner;

	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
	@Column(name = "date", nullable = false, updatable = false)
	private Date date;

	@Column(name = "segmentacion", nullable = false)
	private String segmentacion;
	
	@Column(name = "sexo", nullable = false)
	private String sexo;

	public AffinitySegmentation() {
		super();
	}

	public AffinitySegmentation(Long idBanner, Date date, String segmentacion, String sexo) {
		super();
		this.idBanner = idBanner;
		this.date = date;
		this.segmentacion = segmentacion;
		this.sexo= sexo;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getSegmentacion() {
		return segmentacion;
	}

	public void setSegmentacion(String segmentacion) {
		this.segmentacion = segmentacion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdBanner() {
		return idBanner;
	}

	public void setIdBanner(Long idBanner) {
		this.idBanner = idBanner;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
