package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name = "logPrints")
@Table(name = "log_prints")
public class logPrints {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;
	
	
	@Column(name="banner", nullable = false)
	private Long idBanner;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
	@Column(name = "date", nullable = false,updatable = false)
	private Date date;
	
	

	public logPrints() {
		super();
	}
	
	

	public logPrints(Long idBanner, Date date) {
		super();
		this.idBanner = idBanner;
		this.date = date;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdBanner() {
		return idBanner;
	}

	public void setIdBanner(Long idBanner) {
		this.idBanner = idBanner;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
