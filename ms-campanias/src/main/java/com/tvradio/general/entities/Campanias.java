package com.tvradio.general.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;



@Entity(name = "publicity_campaing")
public class Campanias {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_campaign", nullable=false,unique = true)
	private Long idcampania;
	
	@Column(name = "name_campaign",nullable = false,unique = true)
	private String nameCampanig;
	
	@JsonFormat(pattern="yyyy-MM-dd", timezone = "UTC")
	@Column(name = "fech_create")
	private Date fechcreate;

	@JsonFormat(pattern="yyyy-MM-dd", timezone = "UTC")
	@Column(name = "fech_ini")
	private Date fechini;
	
	@JsonFormat(pattern="yyyy-MM-dd", timezone = "UTC")
	@Column(name = "fech_fin")
	private Date fechfin;
	
	@Column(name="inversion",nullable = false)
	private Double inversion;
	
		
	@Column(name = "status")
	private Integer status;
	
	
	//@Column(name="banners")
	//List<Banner> banners;
	
	

	public Campanias() {
		super();
	}

	public Long getIdcampania() {
		return idcampania;
	}

	public void setIdcampania(Long idcampania) {
		this.idcampania = idcampania;
	}

	public String getNameCampanig() {
		return nameCampanig;
	}

	public void setNameCampanig(String nameCampanig) {
		this.nameCampanig = nameCampanig;
	}

	public Date getFechcreate() {
		return fechcreate;
	}

	public void setFechcreate(Date fechcreate) {
		this.fechcreate = fechcreate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getFechini() {
		return fechini;
	}

	public void setFechini(Date fechini) {
		this.fechini = fechini;
	}

	public Date getFechfin() {
		return fechfin;
	}

	public void setFechfin(Date fechfin) {
		this.fechfin = fechfin;
	}

	public Double getInversion() {
		return inversion;
	}

	public void setInversion(Double inversion) {
		this.inversion = inversion;
	}
	
	
	
	

	
	
	
	
}
