package com.tvradio.capitulos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tvradio.general.entities.Capitulos;
import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Programa;


@Transactional
public interface CapituloRepository extends CrudRepository<Capitulos, Long> {
	public List<Capitulos> findAllByStatus(Boolean status);

	public Capitulos findByIdChapterAndStatus(Long idChapter, Boolean status);

	@Query(value = "SELECT * FROM capitulos  WHERE programa=:programa ORDER BY id_chapter DESC limit 10", nativeQuery = true)
	public List<Capitulos> getLastTenChaptersByProgram(@Param("programa") Programa programa);

	@Modifying
	@Query(value = "UPDATE capitulos c SET c.status=true WHERE c.program=Program")
	public void deleteAllChapters(@Param("Program") Long Program);

	public List<Capitulos> findAllByProgramAndStatus(Programa program, Boolean status);

	public Capitulos findByName(String name);

	public Capitulos findByNameAndStatus(String name, Boolean status);

	public List<Capitulos> findAllByStatusAndSeason(Boolean status, Integer season);

	public List<Capitulos> findByProgramAndStatusAndSeason(Programa program, Boolean status, Integer season);

}