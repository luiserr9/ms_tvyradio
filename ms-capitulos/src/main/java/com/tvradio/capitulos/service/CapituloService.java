package com.tvradio.capitulos.service;

import java.util.Date;
import java.util.Locale.Category;
import java.util.Optional;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvradio.capitulos.repository.CapituloRepository;
import com.tvradio.general.dto.CapitulosResp;
import com.tvradio.general.dto.ProgramaCapituloResp;
import com.tvradio.general.entities.Capitulos;
import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Programa;
import com.tvradio.general.utilities.Response;

@Service
public class CapituloService {
	@Autowired
	private CapituloRepository chapter;

	public Response getLastTenChaptersByProgram(Programa program) {
		try {
			return Response.ok(this.chapter.getLastTenChaptersByProgram(program));
		} catch (Exception e) {
			return Response.error("Error Ineserado: " + e.getMessage());
		}
	}

	public Response getAllChaptersByProgram(Programa program) {
		try {
			ArrayList newresprogram = new ArrayList();
			List<Capitulos> capitulos = this.chapter.findAllByProgramAndStatus(program, true);
			int aux = 0;
			for (int i = 1; i <= capitulos.get(0).getProgram().getSeasons(); i++) {
				if (aux != capitulos.get(i).getSeason()) {
					aux = capitulos.get(i).getSeason();
					List<Capitulos> cap = this.chapter.findByProgramAndStatusAndSeason(capitulos.get(i).getProgram(),true,i);
					ArrayList respcap = new ArrayList();
					for (int j = 0; j < cap.size(); j++) {
						respcap.add(new CapitulosResp(cap.get(j).getIdChapter(), cap.get(j).getName(),
								cap.get(j).getUrl(), cap.get(j).getDescription(), cap.get(j).getPricturePortada(),
								cap.get(j).getSeason()));
					}
					newresprogram.add(respcap);
				}
			}

			ProgramaCapituloResp newresp = new ProgramaCapituloResp(
					this.chapter.findAllByProgramAndStatus(program, true).get(0).getProgram().getProgramId(),
					this.chapter.findAllByProgramAndStatus(program, true).get(0).getProgram().getName(),
					this.chapter.findAllByProgramAndStatus(program, true).get(0).getProgram().getCategory()
							.getCategoryId(),
					this.chapter.findAllByProgramAndStatus(program, true).get(0).getProgram().getPictureCover(),
					newresprogram);
			return Response.ok(newresp);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}

	}

	public Response getAllChaptersByProgramAndSeasson(Programa programa, Integer seasson) {
		try {
			return Response.ok(this.chapter.findByProgramAndStatusAndSeason(programa, true, seasson));
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response getChapterById(Long id) {
		try {
			return Response.ok(chapter.findByIdChapterAndStatus(id, true));
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response getAllChapters() {
		try {
			return Response.ok(this.chapter.findAllByStatus(true));
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response getAllChaptersBySeason(Integer season) {
		try {
			return Response.ok(this.chapter.findAllByStatusAndSeason(true, season));
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response deleteChapterById(Long id) {
		try {
			if (this.chapter.findByIdChapterAndStatus(id, true) != null) {
				Capitulos chapt = chapter.findById(id).get();
				chapt.setStatus(false);
				chapter.save(chapt);
				return Response.accepted("OK");
			} else {
				return Response.notFound("No se encuentra capitulo a eliminar");
			}
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	/*
	 * public Response insertChapter(Capitulos cap) { try { if
	 * (cap.getDateCreation() == null) { cap.setDateCreation(new Date()); } if
	 * (cap.getStatus() == null) { cap.setStatus(true); } chapter.save(cap); return
	 * Response.accepted("OK"); } catch (Exception e) { return
	 * Response.error("Error: " + e.getMessage()); } }
	 */

	public Response insertChapter(Capitulos cap) {
		try {
			if(cap.getDescription().length()>150) {
				return Response.badRequest("Tu descripcion no debe ser mayor a 150 caracteres");
			}
			if (this.chapter.findByNameAndStatus(cap.getName(), true) == null) {
				if (this.chapter.findByName(cap.getName()) == null) {
					//System.out.println((cap.getDescription()).size());
					cap.setStatus(true);
					cap.setDateCreation(new Date());
					chapter.save(cap);
					return Response.created("OK");
				} else {
					cap.setIdChapter(chapter.findByName(cap.getName()).getIdChapter());
					cap.setStatus(true);
					this.chapter.save(cap);
					return Response.accepted("OK");
				}
			} else {
				return Response.badRequest("Ya existe");
			}

		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response updateChapter(Capitulos cap) {
		try {
			if(cap.getDescription().length()>150) {

				return Response.badRequest("Tu descripcion no debe ser mayor a 150 caracteres");
			}
			if (chapter.findByIdChapterAndStatus(cap.getIdChapter(), true) != null) {
				Optional<Capitulos> chap = this.chapter.findById(cap.getIdChapter());
				if (cap.getUrl() == null) {
					cap.setUrl(chap.get().getUrl());
				}

				if (cap.getSeason() == null) {
					cap.setSeason(chap.get().getSeason());
				}

				if (cap.getProgram() == null) {
					cap.setProgram(chap.get().getProgram());
				}

				if (cap.getPricturePortada() == null) {
					cap.setPricturePortada(chap.get().getPricturePortada());
				}

				if (cap.getName() == null) {
					cap.setName(chap.get().getName());
				}

				if (cap.getDescription() == null) {
					cap.setDescription(chap.get().getDescription());
				}

				if (cap.getDateCreation() == null) {
					cap.setDateCreation(chap.get().getDateCreation());
				}
				if (cap.getStatus() == null) {
					cap.setStatus(chap.get().getStatus());
				}
				this.chapter.save(cap);
				return Response.accepted("OK");
			} else {
				return Response.notFound("No existe capitulo a editar");
			}

		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

}