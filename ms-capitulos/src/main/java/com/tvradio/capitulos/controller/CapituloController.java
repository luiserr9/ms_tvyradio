package com.tvradio.capitulos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tvradio.capitulos.service.CapituloService;
import com.tvradio.general.dto.ProgramAndSeason;
import com.tvradio.general.entities.Capitulos;
import com.tvradio.general.entities.Categoria;
import com.tvradio.general.entities.Programa;
import com.tvradio.general.utilities.Response;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("/chapter")
public class CapituloController {

	@Autowired
	private CapituloService service;

	@PostMapping("/getLastTenChaptersByProgram")
	public Response getLastTenChaptersByProgram(@RequestBody Programa program) {
		return this.service.getLastTenChaptersByProgram(program);
	}
	
	@PostMapping("/getAllChaptersByProgram")
	public Response getAllChaptersByProgram(@RequestBody Programa program) {
		return this.service.getAllChaptersByProgram(program);
	}

	@PostMapping("/getAllChaptersByProgramAndSeasson")
	public Response getAllChaptersByProgramAndSeasson(@RequestBody ProgramAndSeason dto) {
		return this.service.getAllChaptersByProgramAndSeasson(dto.getPrograma(), dto.getSeasson());
	}

	@PostMapping("/getChapterById")
	public Response getChapterById(@RequestBody Capitulos capitulo) {
		return service.getChapterById(capitulo.getIdChapter());
	}

	@PostMapping("/deleteChapterById")
	public Response deleteChapterById(@RequestBody Capitulos chapter) {
		return service.deleteChapterById(chapter.getIdChapter());
	}

	@PostMapping("/insertChapter")
	public Response insertChapter(@RequestBody Capitulos chapter) {
		return service.insertChapter(chapter);
	}

	@PostMapping("/updateChapter")
	public Response updateChapter(@RequestBody Capitulos chapter) {
		return service.updateChapter(chapter);
	}

	@GetMapping("/getAllChapters")
	public Response getAllChapters() {
		return this.service.getAllChapters();
	}

	@PostMapping("/getAllBySeason")
	public Response getAllBySeason(@RequestBody Capitulos chapter) {
		return this.service.getAllChaptersBySeason(chapter.getSeason());
	}

}
