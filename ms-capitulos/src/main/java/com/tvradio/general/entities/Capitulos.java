package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "capitulos")
public class Capitulos {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_chapter", nullable = false, unique = true)
	private Long idChapter;

	@Column(name = "name", unique = true, nullable = false)
	private String name;

	@Column(name = "url", nullable = false)
	private String url;

	@ManyToOne
	// @JsonIgnore
	@JoinColumn(name = "programa", nullable = false)
	private Programa program;

	@Column(name = "description", nullable = false)
	private String description;

	@Column(name = "picture_portada")
	private String pricturePortada;

	@Column(name = "season", nullable = false)
	public Integer season;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
	@Temporal(TemporalType.DATE)
	@Column(name = "date_creation", nullable = false, updatable = false)
	public Date dateCreation;

	@Column(name = "status", nullable = false)
	private Boolean status;

	public Capitulos() {
		super();
	}

	public Long getIdChapter() {
		return idChapter;
	}

	public void setIdChapter(Long idChapter) {
		this.idChapter = idChapter;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Programa getProgram() {
		return program;
	}

	public void setProgram(Programa program) {
		this.program = program;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPricturePortada() {
		return pricturePortada;
	}

	public void setPricturePortada(String pricturePortada) {
		this.pricturePortada = pricturePortada;
	}

	public Integer getSeason() {
		return season;
	}

	public void setSeason(Integer season) {
		this.season = season;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

}
