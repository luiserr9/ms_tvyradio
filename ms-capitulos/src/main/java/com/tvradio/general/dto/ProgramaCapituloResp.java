package com.tvradio.general.dto;


import java.math.BigInteger;
import java.util.ArrayList;

public class ProgramaCapituloResp {

	private long programId=0; 	
	private String name;
    private Long category; 
    private String pictureCover;
    private ArrayList seasons;
    
    
    
	public ProgramaCapituloResp() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProgramaCapituloResp(Long programId, String name, Long category, String pictureCover, ArrayList seasons) {
		super();
		this.programId = programId;
		this.name = name;
		this.category = category;
		this.pictureCover =pictureCover;
		this.seasons = seasons;
	}

	public long getProgramId() {
		return programId;
	}

	public void setProgramId(long programId) {
		this.programId = programId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCategory() {
		return category;
	}

	public void setCategory(Long category) {
		this.category = category;
	}

	public String getPictureCover() {
		return pictureCover;
	}

	public void setPictureCover(String pictureCover) {
		this.pictureCover = pictureCover;
	}

	public ArrayList getSeasons() {
		return seasons;
	}

	public void setSeasons(ArrayList seasons) {
		this.seasons = seasons;
	}
	 
	


}

