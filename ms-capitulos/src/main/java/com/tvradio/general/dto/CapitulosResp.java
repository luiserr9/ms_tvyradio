package com.tvradio.general.dto;

public class CapitulosResp {
	
	private Long idChapter;
	private String name;
	private String url;
	private String description;
	private String pricturePortada;
	private int season;
	
	
	
	public CapitulosResp() {
		super();
	}
	
	public CapitulosResp(Long  idChapter,String name,String url,String description,String pricturePortada, int season) {
		this.idChapter = idChapter;
		this.name = name;
		this.url = url;
		this.description = description;
		this.pricturePortada = pricturePortada;
		this.season = season;
	}

	public Long getIdChapter() {
		return idChapter;
	}

	public void setIdChapter(Long idChapter) {
		this.idChapter = idChapter;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPricturePortada() {
		return pricturePortada;
	}

	public void setPricturePortada(String pricturePortada) {
		this.pricturePortada = pricturePortada;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}
	
	
	
	
	
	
	

}
