package com.tvradio.tvenvivoapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tvradio.general.entities.TvEnVivoApp;
import com.tvradio.general.utilities.Response;
import com.tvradio.tvenvivoapp.service.TvEnVivoAppService;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/livetvapp")
public class TvEnVivoAppController {
	@Autowired
	private TvEnVivoAppService tv;

	@GetMapping("/getStreamingTvApp")
	public Response getStreamingTvApp() {
		return this.tv.getStreamingApp();
	}

	@PostMapping("/updateStreamingApp")
	public Response updateStreamingApp(@RequestBody TvEnVivoApp streaming) {
		return this.tv.updateStreamingApp(streaming);
	}

}