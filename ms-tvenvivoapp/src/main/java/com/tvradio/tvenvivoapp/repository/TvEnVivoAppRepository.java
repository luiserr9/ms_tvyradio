package com.tvradio.tvenvivoapp.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.tvradio.general.entities.TvEnVivoApp;



public interface TvEnVivoAppRepository extends CrudRepository<TvEnVivoApp, Long>{
	
	
	
	
}
