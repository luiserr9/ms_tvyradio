package com.tvradio.tvenvivoapp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale.Category;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tvradio.general.entities.TvEnVivoApp;
import com.tvradio.general.utilities.Response;
import com.tvradio.tvenvivoapp.repository.TvEnVivoAppRepository;

@Service
public class TvEnVivoAppService {
	@Autowired
	private TvEnVivoAppRepository tv;

	public Response getStreamingApp() {
		try {
			return Response.ok(tv.findAll());
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response updateStreamingApp(TvEnVivoApp tvenvivo) {
		try {
			this.tv.save(tvenvivo);
			return Response.accepted(true);
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}

	}

}