package com.tvradio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TvradioApplication {

	public static void main(String[] args) {
		SpringApplication.run(TvradioApplication.class, args);
	}

}
