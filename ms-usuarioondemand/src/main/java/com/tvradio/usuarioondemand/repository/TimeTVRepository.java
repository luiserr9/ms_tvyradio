package com.tvradio.usuarioondemand.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.TimeTV;


public interface TimeTVRepository extends CrudRepository<TimeTV, Long> {

	@Query(value="SELECT t FROM time_tv t")
	public List<TimeTV> getAllRegistries();
	
	@Query(value="SELECT t FROM time_tv t WHERE t.dateOfCreation BETWEEN  :date1 AND  :date2")
	public List<TimeTV> getAllRegistriesByInterval(@Param("date1") Date date1, @Param("date2") Date date2);
	
	@Query(value = "SELECT t FROM time_tv t WHERE MONTH(t.dateOfCreation)=:mes AND YEAR(t.dateOfCreation)=:anio ")
	public List<TimeTV> getHoursByCurrentMothAndYear(@Param("mes") Integer mes, @Param("anio") Integer anio);
	

	
}
