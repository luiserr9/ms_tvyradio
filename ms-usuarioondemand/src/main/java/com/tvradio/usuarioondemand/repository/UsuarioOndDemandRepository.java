package com.tvradio.usuarioondemand.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.dto.UsuarioPanelResp;
import com.tvradio.general.entities.UsuarioOnDemand;
import com.tvradio.general.entities.UsuarioPanel;

@Transactional
public interface UsuarioOndDemandRepository extends CrudRepository<UsuarioOnDemand, Long> {

	public UsuarioOnDemand findByUserOnDemandIdAndStatus(Long userOnDemandId, Boolean status);

	public List<UsuarioOnDemand> findAllByStatus(Boolean status);

	public UsuarioOnDemand findByEmailAndPasswordAndStatus(String email, String password, Boolean status);

	public UsuarioOnDemand findByEmail(String email);

	public UsuarioOnDemand findByEmailAndStatus(String email, Boolean status);

	@Query(value = "SELECT COUNT(u) FROM usuario_on_demand u WHERE u.status=true AND u.dateOfCreation BETWEEN :date1 AND :date2 ")
	public Double getUserByDateInterval(@Param("date1") Date date1, @Param("date2") Date date2);

	@Query(value = "SELECT COUNT(u) FROM usuario_on_demand u WHERE u.status=true")
	public Double getAllUsers();

	@Query(value = "SELECT count(u) FROM usuario_on_demand u WHERE MONTH(u.dateOfCreation)=:mes AND YEAR(u.dateOfCreation)=:anio AND  u.status=true ")
	public Double getCurrenRegistries(@Param("mes") Integer mes, @Param("anio") Integer anio);

	@Modifying
	@Query(value = "UPDATE usuario_on_demand SET status=0 WHERE userOnDemandId =:userid ")
	void deleteUserOnDemandId(@Param("userid") Long userid);

	@Modifying
	@Query(value = "UPDATE usuario_on_demand set password=:pass WHERE email =:email  ")
	void changePassword(@Param("pass") String pass, @Param("email") String email);
	
	
	
	@Query(value = "SELECT COUNT(u) FROM usuario_on_demand u WHERE u.status=true AND u.session=true")
	public Double getAllUniqueUsers();

	@Query(value = "SELECT COUNT(u) FROM usuario_on_demand u WHERE u.status=true AND u.session=true AND u.sessionDate BETWEEN :date1 AND :date2")
	public Double getUniqueUseresByIntervalDate(@Param("date1") Date date1, @Param("date2") Date date2);

	@Query(value = "SELECT count(u) FROM usuario_on_demand u WHERE MONTH(u.sessionDate)=:mes AND YEAR(u.sessionDate)=:anio AND  u.status=true AND u.session=true")
	public Double getUniqueUsersByActualMonthAndYear(@Param("mes") Integer mes, @Param("anio") Integer anio);


}
