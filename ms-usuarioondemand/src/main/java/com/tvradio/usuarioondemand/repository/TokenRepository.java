package com.tvradio.usuarioondemand.repository;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.Token;

public interface TokenRepository extends CrudRepository<Token, Long>{

	
	@Modifying
	@Query(value = "INSERT INTO token (token, activo, id_user,fecha_registro,identified ) values (:token, :activo, :iduser, :fecharegistro,:identified )",nativeQuery = true)
	@Transactional
	void insertToken(@Param("token") String token, @Param("activo") Integer activo, @Param("iduser") Long iduser, @Param("fecharegistro") Date fecharegistro,@Param("identified") Character identified);
	
	@Modifying
	@Query(value = "UPDATE token SET activo=0 WHERE token =:token ")
	@Transactional
	void eliminarToken( @Param("token") String userid );
	
	public Token  findByTokenAndActivo(String token,Boolean activo);
	
	public Token findByTokenAndIdentified(String tokken , Character identified);
	
}
