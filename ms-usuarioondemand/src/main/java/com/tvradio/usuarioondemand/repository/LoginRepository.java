package com.tvradio.usuarioondemand.repository;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tvradio.general.entities.LoginOnDemand;
import com.tvradio.general.entities.Token;

public interface LoginRepository extends CrudRepository<LoginOnDemand, Long> {

	@Query(value = "SELECT COUNT(l) FROM login_on_demand l WHERE  l.dateOfCreation BETWEEN :date1 AND :date2 ")
	public Double getLoginsByIntervalDate(@Param("date1") Date date1, @Param("date2") Date date2);

	@Query(value = "SELECT count(l) FROM login_on_demand l WHERE MONTH(l.dateOfCreation)=:mes AND YEAR(l.dateOfCreation)=:anio ")
	public Double getLoginsByActualMonthAndYear(@Param("mes") Integer mes, @Param("anio") Integer anio);

	@Query(value = "SELECT COUNT(l) FROM login_on_demand l")
	public Double getAllLogins();

	// **************
	// SELECT count(*) AS primer_intervalo FROM login_on_demand WHERE
	// date_of_creation='2019-12-02' AND hour_of_creation BETWEEN '00:00:00' AND
	// '05:59:59'

	//by intervals of today
	@Query(value = "SELECT count(l) FROM login_on_demand l WHERE l.dateOfCreation=:today AND  l.hourOfCreation BETWEEN '00:00:01' AND '05:59:59'")
	public Double getLoginsByFisrtInterval(@Param("today") Date today);
	
	@Query(value = "SELECT count(l) FROM login_on_demand l WHERE l.dateOfCreation=:today AND  l.hourOfCreation BETWEEN '06:00:00' AND '11:59:59'")
	public Double getLoginsBySecondInterval(@Param("today") Date today);
	
	@Query(value = "SELECT count(l) FROM login_on_demand l WHERE l.dateOfCreation=:today AND  l.hourOfCreation BETWEEN '12:00:00' AND '17:59:59'")
	public Double getLoginsByThirtInterval(@Param("today") Date today);
	
	@Query(value = "SELECT count(l) FROM login_on_demand l WHERE l.dateOfCreation=:today AND  l.hourOfCreation BETWEEN '18:00:00' AND '23:59:59'")
	public Double getLoginsByFourthInterval(@Param("today") Date today);
	
	//by interval of intevrals dates
	
	@Query(value = "SELECT count(l) FROM login_on_demand l WHERE   l.dateOfCreation BETWEEN :date1 AND :date2     AND  l.hourOfCreation BETWEEN '00:00:01' AND '05:59:59'")
	public Double getLoginsByFisrtIntervalAndIntevralDate(@Param("date1") Date date1,@Param("date2") Date date2);
	
	@Query(value = "SELECT count(l) FROM login_on_demand l WHERE   l.dateOfCreation BETWEEN :date1 AND :date2     AND  l.hourOfCreation BETWEEN '06:00:00' AND '11:59:59'")
	public Double getLoginsBySecondIntervalAndIntevralDate(@Param("date1") Date date1,@Param("date2") Date date2);
	
	@Query(value = "SELECT count(l) FROM login_on_demand l WHERE   l.dateOfCreation BETWEEN :date1 AND :date2     AND  l.hourOfCreation BETWEEN '12:00:00' AND '17:59:59'")
	public Double getLoginsByThirdIntervalAndIntevralDate(@Param("date1") Date date1,@Param("date2") Date date2);
	
	@Query(value = "SELECT count(l) FROM login_on_demand l WHERE   l.dateOfCreation BETWEEN :date1 AND :date2     AND  l.hourOfCreation BETWEEN '18:00:00' AND '23:59:59'")
	public Double getLoginsByFourthIntervalAndIntevralDate(@Param("date1") Date date1,@Param("date2") Date date2);
	

}
