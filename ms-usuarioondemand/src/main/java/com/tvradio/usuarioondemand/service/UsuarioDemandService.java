package com.tvradio.usuarioondemand.service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.tvradio.general.utilities.Response;

import com.tvradio.usuarioondemand.repository.TokenRepository;
import com.tvradio.usuarioondemand.repository.UsuarioOndDemandRepository;
import com.tvradio.general.dto.HoraryLoginsResp;
import com.tvradio.general.dto.UsuarioPanelResp;

import com.tvradio.general.entities.LoginOnDemand;

import com.tvradio.general.entities.TimeTV;
import com.tvradio.general.entities.Token;

import com.tvradio.general.entities.UsuarioOnDemand;
import com.tvradio.general.entities.UsuarioPanel;
import com.tvradio.usuarioondemand.repository.LoginRepository;
import com.tvradio.usuarioondemand.repository.TimeTVRepository;

import net.bytebuddy.asm.Advice.This;

@Service
public class UsuarioDemandService {
	@Autowired
	private UsuarioOndDemandRepository repository;

	@Autowired
	private TokenRepository token;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private LoginRepository log;

	@Autowired

	private TimeTVRepository time;

	public Response getAllHours() {
		try {
			Double minutes = 0.0;
			List<TimeTV> ss = this.time.getAllRegistries();
			for (TimeTV timeApp : ss) {
				minutes = minutes + timeApp.getMinutes();
			}
			return Response.ok(minutes / 60);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getPorcentoOfHours(Date date1, Date date2) {
		try {
			Double numeroAnterior = 0.0;
			List<TimeTV> ss = this.time.getAllRegistriesByInterval(date1, date2);
			for (TimeTV TimeTV : ss) {
				numeroAnterior = numeroAnterior + TimeTV.getMinutes();
			}
			
			numeroAnterior = numeroAnterior / 60;
			Double numeroActual = 0.0;
			List<TimeTV> si = this.time.getHoursByCurrentMothAndYear(this.getCurrentMonth(), this.getCurrentYear());
			for (TimeTV TimeTV : si) {
				numeroActual = numeroActual + TimeTV.getMinutes();
			}
			
			numeroActual = numeroActual / 60;
			if (numeroAnterior == 0 && numeroActual == 0) {
				return Response.ok(0 + "%");
			}
			if (numeroActual == 0) {
				return Response.ok("-" + numeroAnterior * 100 + "%");
			}
			if (numeroAnterior == 0) {
				System.out.println(numeroActual);
				return Response.ok(numeroActual * 100 + "%");
			}

			Double diferencia = numeroActual - numeroAnterior;
			if (diferencia == 0) {
				return Response.ok("0%");
			}
			Double tasa = 1 / numeroActual * 100;
			Double relacion = numeroAnterior * tasa;
			return Response.ok((100 - relacion) + "%");
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getAllHoursByIntervalDate(Date date1, Date date2) {
		try {
			Double minutes = 0.0;
			List<TimeTV> ss = this.time.getAllRegistriesByInterval(date1, date2);
			for (TimeTV timeApp : ss) {
				minutes = minutes + timeApp.getMinutes();
			}
			return Response.ok(minutes / 60);
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response addTimeSession(Integer tiempo, UsuarioOnDemand user) {
		try {
			TimeTV aux = new TimeTV(user, new Date(), tiempo);
			this.time.save(aux);
			return Response.created("OK");
		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	// auxiliary methods
	public Integer getCurrentMonth() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.MONTH) + 1;
	}

	public Integer getCurrentYear() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.YEAR);
	}

	///////////////////////

	public Response getAllIntervalsOfLoginsByInterval(Date date1, Date date2) {
		try {
			return Response.ok(new HoraryLoginsResp(this.log.getLoginsByFisrtIntervalAndIntevralDate(date1, date2),
					this.log.getLoginsBySecondIntervalAndIntevralDate(date1, date2),
					this.log.getLoginsByThirdIntervalAndIntevralDate(date1, date2),
					this.log.getLoginsByFourthIntervalAndIntevralDate(date1, date2)));
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response getAllIntervalsOfLoginsOfToday() {
		try {
			return Response.ok(new HoraryLoginsResp(this.log.getLoginsByFisrtInterval(new Date()),
					this.log.getLoginsBySecondInterval(new Date()), this.log.getLoginsByThirtInterval(new Date()),
					this.log.getLoginsByFourthInterval(new Date())));
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response getPorcentOfUniqueUsers(Date date1, Date date2) {
		Double numeroAnterior = this.repository.getUniqueUseresByIntervalDate(date1, date2);
		Double numeroActual = this.repository.getUniqueUsersByActualMonthAndYear(this.getCurrentMonth(),
				this.getCurrentYear());
		if (numeroAnterior == 0 && numeroActual == 0) {
			return Response.ok(0 + "%");
		}
		if (numeroActual == 0) {
			return Response.ok("-" + numeroAnterior * 100 + "%");
		}
		if (numeroAnterior == 0) {
			return Response.ok(numeroActual * 100 + "%");
		}

		Double diferencia = numeroActual - numeroAnterior;
		if (diferencia == 0) {
			return Response.ok("0%");
		}
		Double tasa = 1 / numeroActual * 100;
		Double relacion = numeroAnterior * tasa;
		return Response.ok((100 - relacion) + "%");

	}

	public Response getUniqueUseresByIntervalDate(Date date1, Date date2) {
		try {
			return Response.ok(this.repository.getUniqueUseresByIntervalDate(date1, date2));

		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response getAllUniqueUsers() {
		try {
			return Response.ok(this.repository.getAllUniqueUsers());

		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response getPorcentOfLogins(Date date1, Date date2) {
		Double numeroAnterior = this.log.getLoginsByIntervalDate(date1, date2);
		Double numeroActual = this.log.getLoginsByActualMonthAndYear(getCurrentMonth(), getCurrentYear());
		if (numeroAnterior == 0 && numeroActual == 0) {
			return Response.ok(0 + "%");
		}
		if (numeroActual == 0) {
			return Response.ok("-" + numeroAnterior * 100 + "%");
		}
		if (numeroAnterior == 0) {
			return Response.ok(numeroActual * 100 + "%");
		}

		Double diferencia = numeroActual - numeroAnterior;
		if (diferencia == 0) {
			return Response.ok("0%");
		}
		Double tasa = 1 / numeroActual * 100;
		Double relacion = numeroAnterior * tasa;
		return Response.ok((100 - relacion) + "%");
	}

	public Response getLoginsByIntervalDate(Date date1, Date date2) {
		try {
			return Response.ok(log.getLoginsByIntervalDate(date1, date2));
		} catch (Exception e) {
			return Response.error("Error ineaperado: " + e.getMessage());
		}
	}

	public Response getAllLogins() {
		try {
			return Response.ok(log.getAllLogins());
		} catch (Exception e) {
			return Response.error("Error ineaperado: " + e.getMessage());
		}
	}
	///

	public Response getProcentOfRegistries(Date date1, Date date2) {
		try {
			Double numeroAnterior = this.repository.getUserByDateInterval(date1, date2);
			Double numeroActual = this.repository.getCurrenRegistries(this.getCurrentMonth(), this.getCurrentYear());
			if (numeroAnterior == 0 && numeroActual == 0) {
				return Response.ok(0 + "%");
			}
			if (numeroActual == 0) {
				return Response.ok("-" + numeroAnterior * 100 + "%");
			}
			if (numeroAnterior == 0) {
				return Response.ok(numeroActual * 100 + "%");
			}

			Double diferencia = numeroActual - numeroAnterior;
			if (diferencia == 0) {
				return Response.ok("0%");
			}
			Double tasa = 1 / numeroActual * 100;
			Double relacion = numeroAnterior * tasa;
			return Response.ok((100 - relacion) + "%");

		} catch (Exception e) {
			return Response.error("Error Inesperado: " + e.getMessage());
		}
	}

	public Response getAllUsers() {
		try {
			return Response.ok(this.repository.getAllUsers());
		} catch (Exception e) {
			return Response.error("Error inesperado: " + e.getMessage());
		}
	}

	public Response getNumberOfUserByDateInterval(Date date1, Date date2) {
		try {
			return Response.ok(this.repository.getUserByDateInterval(date1, date2));
		} catch (Exception e) {
			return Response.error("Error inessperado: " + e.getMessage());
		}

	}

	public Response getUserOnDemandById(Long id) {
		try {
			return Response.success(repository.findByUserOnDemandIdAndStatus(id, true));
		} catch (Exception e) {
			return Response.error("Error: " + e.getLocalizedMessage());
		}
	}

	public Response login(String email, String pass) {
		try {
			if (this.repository.findByEmail(email) != null) {
				if (this.repository.findByEmail(email).getStatus() == true) {
					if (this.repository.findByEmailAndPasswordAndStatus(email, pass, true) != null) {
						UsuarioOnDemand ss = this.repository.findByEmail(email);
						// this.log.save(new LoginOnDemand(ss, new Date()));
						this.log.save(new LoginOnDemand(ss, new Date(), new Date()));
						ss.setSessionDate(new Date());
						ss.setSession(true);
						this.repository.save(ss);
						return Response.ok(ss);
					} else {
						return Response.badRequest("pass incorrecto");
					}
				} else {
					return Response.notFound("Usuario Inabilitado");
				}
			} else {
				return Response.notFound("El usuario no existe");
			}
		} catch (Exception e) {
			return Response.error("Error: " + e.getLocalizedMessage());
		}
	}

	public Response getAllUsersOnDemand() {
		try {
			return Response.success((repository.findAllByStatus(true)));
		} catch (Exception e) {
			return Response.success("Error: " + e.getMessage());
		}
	}

	public Response deleteUserOnDemandById(Long id) {
		try {
			if (repository.findByUserOnDemandIdAndStatus(id, true) != null) {
				repository.deleteUserOnDemandId(id);
				return Response.success("OK");
			} else {
				return Response.error("El usuario a eliminar no existe");
			}
		} catch (Exception e) {
			return Response.success("Error: " + e.getMessage());
		}
	}

	public Response updateUser(UsuarioOnDemand usuario) {
		try {
			if (repository.findByUserOnDemandIdAndStatus(usuario.getUserOnDemandId(), true) != null) {
				UsuarioOnDemand ss = repository.findByUserOnDemandIdAndStatus(usuario.getUserOnDemandId(), true);
				if (usuario.getSex() == null) {
					usuario.setSex(ss.getSex());
				}
				if (usuario.getPassword() == null) {
					usuario.setPassword(ss.getPassword());
				}
				if (usuario.getName() == null) {
					usuario.setName(ss.getName());
				}
				if (usuario.getDateOfCreation() == null) {
					usuario.setDateOfCreation(ss.getDateOfCreation());
				}
				if (usuario.getAge() == null) {
					usuario.setAge(ss.getAge());
				}
				if (usuario.getStatus() == null) {
					usuario.setStatus(ss.getStatus());
				}
				if (usuario.getEmail() == null) {
					usuario.setEmail(ss.getEmail());
				}

				if (usuario.getSession() == null) {
					usuario.setSession(ss.getSession());
				}
				if (usuario.getSessionDate() == null) {
					usuario.setSessionDate(ss.getSessionDate());
				}
				repository.save(usuario);
				return Response.success("OK");

			} else {
				return Response.error("El usuario a actualizar no existe");
			}
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public Response insertUser(UsuarioOnDemand usuario) {
		try {

			if (repository.findByEmailAndStatus(usuario.getEmail(), true) == null) {
				if (repository.findByEmail(usuario.getEmail()) == null) {
					usuario.setDateOfCreation(new Date());
					usuario.setStatus(true);
					usuario.setSession(true);
					usuario.setSessionDate(new Date());
					repository.save(usuario);
					return Response.success("OK");
				} else {
					usuario.setUserOnDemandId(repository.findByEmail(usuario.getEmail()).getUserOnDemandId());
					usuario.setDateOfCreation(new Date());
					usuario.setStatus(true);
					usuario.setSession(true);
					usuario.setSessionDate(new Date());
					repository.save(usuario);
					this.repository.save(usuario);
					return Response.accepted("OK");
				}

			} else {
				return Response.error("El usuario a insertar ya existe");
			}
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public void sendEmail(String to, String subject, String content) {
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(to);
		email.setSubject(subject);
		email.setText(content);
		mailSender.send(email);
	}

	public Response changePassUserPass(UsuarioOnDemand CambiosPass) {
		try {
			System.out.println(CambiosPass.getEmail());
			if (repository.findByEmail(CambiosPass.getEmail())  != null) {
				if (repository.findByEmailAndStatus(CambiosPass.getEmail(), true) != null) {
					repository.changePassword(CambiosPass.getPassword(),CambiosPass.getEmail());							
					return Response.success("Se cambio la contraseña");
				} else {
					return Response.notFound("El usuario está inabilitado");
				}
			} else {
				return Response.notFound("El usuario no existe");
			}
		} catch (Exception e) {
			return Response.error("Error: " + e.getMessage());
		}
	}

	public String token() {
		return UUID.randomUUID().toString().toUpperCase();

	}

	public Response sendEamialToken(UsuarioPanel usuarioPanel) {
		if (repository.findByEmailAndStatus(usuarioPanel.getEmail(), true) != null) {
			String tokenText = this.token();
			try {

				token.insertToken(tokenText, 1,
						repository.findByEmailAndStatus(usuarioPanel.getEmail(), true).getUserOnDemandId(), new Date(),
						'O');
				sendEmail(usuarioPanel.getEmail(), "Tu token", tokenText);
				return Response.success("Se envio un correo con el codigo de verificacion al siginte correo: "
						+ repository.findByEmailAndStatus(usuarioPanel.getEmail(), true).getEmail());
			} catch (Exception e) {
				return Response.error("Error al enviar el correo con el codigo de verificacion: " + e.getMessage());
			}
		} else {
			return Response.error("El correo no registrado");
		}
	}
}
