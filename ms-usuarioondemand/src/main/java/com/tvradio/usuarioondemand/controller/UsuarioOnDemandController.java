package com.tvradio.usuarioondemand.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tvradio.general.dto.IntervalDate;
import com.tvradio.general.dto.TimeDto;
import com.tvradio.general.entities.Token;
import com.tvradio.general.entities.UsuarioOnDemand;
import com.tvradio.general.entities.UsuarioPanel;
import com.tvradio.general.utilities.Response;

import com.tvradio.usuarioondemand.service.UsuarioDemandService;
import org.thymeleaf.context.Context;

@RestController
@CrossOrigin
@RequestMapping("/userOnDemand")
public class UsuarioOnDemandController {

	
	
	@PostMapping("/addTimeSession")
	public Response addTimeSession(@RequestBody TimeDto dto) {
		return this.service.addTimeSession(dto.getTime(), dto.getUser());
	}

	@GetMapping("/getAllHours")
	public Response getAllHours() {
		return service.getAllHours();
	}

	@PostMapping("/getPorcentoOfHours")
	public Response getPorcentoOfHours(@RequestBody IntervalDate dates) {
		return this.service.getPorcentoOfHours(dates.getDate1(), dates.getDate2());
	}
	@PostMapping("/getAllHoursByIntervalDate")
	public Response getAllHoursByIntervalDate(@RequestBody IntervalDate dates) {
		return this.service.getAllHoursByIntervalDate(dates.getDate1(), dates.getDate2());
	}

	
	@Autowired
	private UsuarioDemandService service;

	@PostMapping("/getUserOnDemandById")
	public Response getUserOnDemandById(@RequestBody UsuarioOnDemand user) {
		return service.getUserOnDemandById(user.getUserOnDemandId());
	}

	@GetMapping("/getAllUsersOnDemand")
	public Response getAllUsersOnDemand() {
		return service.getAllUsersOnDemand();
	}

	@PostMapping("/deleteUserOnDemandById")
	public Response deleteUserOnDemandById(@RequestBody UsuarioOnDemand user) {
		return service.deleteUserOnDemandById(user.getUserOnDemandId());
	}

	@PostMapping("/updateUserOnDemand")
	public Response updateUserOnDemand(@RequestBody UsuarioOnDemand user) {
		return service.updateUser(user);
	}

	@PostMapping("/insertUserOnDemand")
	public Response insertUserOnDemand(@RequestBody UsuarioOnDemand user) {
		return service.insertUser(user);
	}

	@PostMapping("/login")
	public Response login(@RequestBody UsuarioOnDemand user) {
		return service.login(user.getEmail(), user.getPassword());
	}

	@PostMapping("/recuperarContra")
	public Response recuperarContra(@RequestBody UsuarioOnDemand CambiosPass) {
		return service.changePassUserPass(CambiosPass);
	}

	@PostMapping("/enviarTokenRepcuContra")
	public Response enviarTokenRepcuContra(@RequestBody UsuarioPanel usuarioPanel) {
		return service.sendEamialToken(usuarioPanel);
	}

//analitics  registries
	@PostMapping("/getNumberOfUserByDateInterval")
	public Response getNumberOfUserByDateInterval(@RequestBody IntervalDate dates) {
		return this.service.getNumberOfUserByDateInterval(dates.getDate1(), dates.getDate2());
	}

	@GetMapping("/getAllUsers")
	public Response getAllUsers() {
		return service.getAllUsers();
	}

	@PostMapping("/getProcentOfRegistries")
	public Response getProcentOfRegistries(@RequestBody IntervalDate dates) {
		return this.service.getProcentOfRegistries(dates.getDate1(), dates.getDate2());
	}

	// analitics logins

	@GetMapping("/getAllLogins")
	public Response getAllLogins() {
		return service.getAllLogins();
	}

	@PostMapping("/getPorcentOfLogins")
	public Response getPorcentOfLogins(@RequestBody IntervalDate dates) {
		return service.getPorcentOfLogins(dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/getLoginsByIntervalDate")
	public Response getLoginsByIntervalDate(@RequestBody IntervalDate dates) {
		return service.getLoginsByIntervalDate(dates.getDate1(), dates.getDate2());
	}

	// analitics unique

	@GetMapping("/getAllUniqueUsers")
	public Response getAllUniqueUsers() {
		return service.getAllUniqueUsers();
	}

	@PostMapping("/getUniqueUseresByIntervalDate")
	public Response getUniqueUseresByIntervalDate(@RequestBody IntervalDate dates) {
		return service.getUniqueUseresByIntervalDate(dates.getDate1(), dates.getDate2());
	}

	@PostMapping("/getPorcentOfUniqueUsers")
	public Response getPorcentOfUniqueUsers(@RequestBody IntervalDate dates) {
		return service.getPorcentOfUniqueUsers(dates.getDate1(), dates.getDate2());
	}

	// getAllIntervalsOfLoginsOfToday

	@GetMapping("/getAllIntervalsOfLoginsOfToday")
	public Response getAllIntervalsOfLoginsOfToday() {
		return service.getAllIntervalsOfLoginsOfToday();
	}
	@PostMapping("/getAllIntervalsOfLoginsByInterval")
	public Response getAllIntervalsOfLoginsOfToday(@RequestBody IntervalDate dates) {
		return service.getAllIntervalsOfLoginsByInterval(dates.getDate1(),dates.getDate2());
	}
	
	

}
