package com.tvradio.general.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="usuario_panel")
public class UsuarioPanel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="userpanel_id")
	private long  userpanelid;
	@Column(name="name")
	private String name;
	@Column(name="profile")
	private Integer profile;
	@Column(name = "email")
	private String email;
	@Column(name = "fech_creation",updatable = false)
	private Date fechCreation;
	@Column(name = "pass")
	private String pass;
	@Column (name = "status")
	private boolean status;
	
	
	
	
	public Long getUserpanelid() {
		return userpanelid;
	}
	public void setUserpanelid(Long userpanelid) {
		this.userpanelid = userpanelid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getProfile() {
		return profile;
	}
	public void setProfile(Integer profile) {
		this.profile = profile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getFechCreation() {
		return fechCreation;
	}
	public void setFechCreation(Date fechCreation) {
		this.fechCreation = fechCreation;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	

}

