package com.tvradio.general.dto;

import com.tvradio.general.entities.UsuarioOnDemand;

public class TimeDto {
	
	private UsuarioOnDemand user;
	private Integer time;
	
	
	
	public TimeDto() {
		super();
	}
	public TimeDto(UsuarioOnDemand user, Integer time) {
		super();
		this.user = user;
		this.time = time;
	}
	public UsuarioOnDemand getUser() {
		return user;
	}
	public void setUser(UsuarioOnDemand user) {
		this.user = user;
	}
	public Integer getTime() {
		return time;
	}
	public void setTime(Integer time) {
		this.time = time;
	}
	
	
	
}
