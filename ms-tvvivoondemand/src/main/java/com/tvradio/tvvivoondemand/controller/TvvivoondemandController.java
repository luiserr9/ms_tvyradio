package com.tvradio.tvvivoondemand.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tvradio.general.entities.TvVivoOndemand;
import com.tvradio.general.utilities.Response;
import com.tvradio.tvvivoondemand.service.TvvivoondemandService;



@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/tvEnVivoOndemand")
public class TvvivoondemandController {
	
	@Autowired
	private TvvivoondemandService tvvivoondemandService;
	
	@GetMapping("/getStreaming")
	private Response getStreaming() {
		return tvvivoondemandService.getStreamingTv();
	}
	
	@PostMapping("/updateStreaming")
	private Response updateStreaming(@RequestBody TvVivoOndemand tvVivoOndemand) {
		return tvvivoondemandService.StreamingTv(tvVivoOndemand);
	}
	
}
