package com.tvradio.tvvivoondemand.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvradio.general.entities.TvVivoOndemand;
import com.tvradio.general.utilities.Response;
import com.tvradio.tvvivoondemand.repository.TvvivoondemandRepository;

@Service
public class TvvivoondemandService {

	@Autowired
	private TvvivoondemandRepository tvvivoondemandRepository;
	
	
	public Response getStreamingTv() {
		
		return Response.success(tvvivoondemandRepository.findAll());
		
	}
	
	
	public Response StreamingTv( TvVivoOndemand tvVivoOndemand ) {
		
		if (tvvivoondemandRepository.save(tvVivoOndemand) != null) {
			
			System.out.println(tvVivoOndemand.getRutaimg());
			return Response.success("ok");			
		}else {
			return Response .error("Ocurrio un error al actualizar");
		}
			
		
		
		
	}
}
