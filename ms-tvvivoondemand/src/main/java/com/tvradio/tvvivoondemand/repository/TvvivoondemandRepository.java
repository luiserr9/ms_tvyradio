package com.tvradio.tvvivoondemand.repository;

import org.springframework.data.repository.CrudRepository;
import com.tvradio.general.entities.TvVivoOndemand;

public interface TvvivoondemandRepository extends CrudRepository<TvVivoOndemand, Long> {
	
}
