package com.tvradio.general.utilities;

import org.springframework.http.HttpStatus;

public class Response {

	private int status;
	private Object data;
	
	

	public Response(int status, Object data) {
		
		this.data = data;
		this.status = status;
	}
	
	public final static Response error(Object data) {
		return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), data);
	}
	
	public final static Response success(Object data) {
		return new Response(HttpStatus.OK.value(), data);
	}
	
	public final static Response badRequest(Object data) {
		return new Response(HttpStatus.BAD_REQUEST.value(), data);
	}
	public final static Response created(Object data) {
		return new Response(HttpStatus.CREATED.value(),data);
	}
	public final static Response notFound(Object data) {
		return new Response(HttpStatus.NOT_FOUND.value(), data);
	}
	
	public final static Response accepted(Object data) {
		return new Response(HttpStatus.ACCEPTED.value(), data);
	}
	public final static Response ok(Object data) {
		return new Response(HttpStatus.OK.value(), data);
	}
	
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
}
